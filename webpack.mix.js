const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');


mix.js('resources/assets/frontend/js/scripts.js', 'public/frontend/js')
    .sass('resources/assets/frontend/scss/style.scss', 'public/frontend/css')
    .sass('resources/assets/frontend/scss/style_old.scss', 'public/frontend/css');

mix.js('resources/assets/frontend/modules/users/register.js', 'public/frontend/modules/users/js/')
.js('resources/assets/frontend/modules/users/login.js', 'public/frontend/modules/users/js/')
.js('resources/assets/frontend/modules/users/changePassword.js', 'public/frontend/modules/users/js/')
.js('resources/assets/frontend/modules/users/users.js', 'public/frontend/modules/users/js/');

mix.copyDirectory('resources/assets/frontend/images', 'public/frontend/images');


mix.copyDirectory('resources/assets/backend/images', 'public/backend/images');

// Modules
mix.js('resources/assets/backend/js/main.js', 'public/backend/assets/js')

    .js('resources/assets/backend/js/dashboard.js', 'public/backend/assets/js')

    .js('resources/assets/backend/modules/users/js/changePassword.js', 'public/backend/modules/users/js/')
    .js('resources/assets/backend/modules/banners/js/banners.js', 'public/backend/modules/banners/js/')
    .js('resources/assets/backend/modules/categories/js/categories.js', 'public/backend/modules/categories/js/')
    .js('resources/assets/backend/modules/blogs/js/categories.js', 'public/backend/modules/blogs/js/')
    .js('resources/assets/backend/modules/blogs/js/blogs.js', 'public/backend/modules/blogs/js/')
    .js('resources/assets/backend/modules/offers/js/offers.js', 'public/backend/modules/offers/js/')
    .js('resources/assets/backend/modules/freegifts/js/freegifts.js', 'public/backend/modules/freegifts/js/')
    .js('resources/assets/backend/modules/products/js/products.js', 'public/backend/modules/products/js/');


// styles
mix.sass('resources/assets/backend/scss/theme.scss', 'public/backend/assets/css');

// mix.styles([
//     'resources/assets/backend/css/_normalize.scss',
//     'resources/assets/backend/css/_cs-skin-elastic.scss',
//     'resources/assets/backend/css/_themify-icons.scss',
//     'resources/assets/backend/assets/css/_flag-icon.min.scss'
// ], 'public/backend/assets/css/all.css');


