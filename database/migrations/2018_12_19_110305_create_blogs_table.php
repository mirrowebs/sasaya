<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('blog_id');
            $table->integer('blog_bc_id')->comment('Comes from blog categories table');
            $table->string('blog_name');
            $table->string('blog_alias')->nullable();
            $table->string('blog_img')->nullable();
            $table->longText('blog_desc')->nullable();
            $table->text('blog_tags')->nullable();
            $table->string('blog_status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
