<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_gifts', function (Blueprint $table) {
            $table->increments('free_gift_id');
            $table->string('free_gift_name');
            $table->float('free_gift_price');
            $table->string('free_gift_cover_image');
            $table->string('free_gift_uniqid')->nullable();
            $table->text('free_gift_description')->nullable();
            $table->string('free_gift_status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_gifts');
    }
}
