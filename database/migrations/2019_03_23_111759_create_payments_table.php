<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('p_id');
            $table->integer('p_order_id');
            $table->string('p_tracking_id');
            $table->string('p_invoice_number')->nullable();
            $table->string('p_bank_ref_no')->nullable();
            $table->string('p_order_status')->nullable();
            $table->string('p_payment_mode')->nullable();
            $table->string('p_card_name')->nullable();
            $table->string('p_status_code')->nullable();
            $table->string('p_status_message')->nullable();
            $table->string('p_currency')->nullable();
            $table->float('p_amount');
            $table->string('p_billing_name')->nullable();
            $table->string('p_billing_address')->nullable();
            $table->string('p_billing_city')->nullable();
            $table->string('p_billing_state')->nullable();
            $table->string('p_billing_zip')->nullable();
            $table->string('p_billing_country')->nullable();
            $table->string('p_billing_tel')->nullable();
            $table->string('p_billing_email')->nullable();
            $table->string('p_delivery_name')->nullable();
            $table->string('p_delivery_address')->nullable();
            $table->string('p_delivery_city')->nullable();
            $table->string('p_delivery_state')->nullable();
            $table->string('p_delivery_zip')->nullable();
            $table->string('p_delivery_country')->nullable();
            $table->string('p_delivery_tel')->nullable();
            $table->string('p_country')->nullable();
            $table->string('p_failure_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
