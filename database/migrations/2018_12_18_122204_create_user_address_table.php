<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_address', function (Blueprint $table) {
            $table->increments('ua_id');
            $table->string('ua_name');
            $table->text('ua_address');
            $table->string('ua_landmark')->nullable();
            $table->string('ua_state');
            $table->string('ua_city');
            $table->string('ua_email')->nullable();
            $table->bigInteger('ua_phone')->nullable();
            $table->integer('ua_defult')->comment('1=default address')->default(0);
            $table->integer('ua_user_id');
            $table->integer('ua_country');
            $table->integer('ua_pincode');
            $table->string('ua_status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_address');
    }
}
