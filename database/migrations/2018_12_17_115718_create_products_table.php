<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('p_id');
            $table->integer('p_category');
            $table->integer('p_sub_category')->nullable();
            $table->string('p_name');
            $table->string('p_alias');
            $table->string('p_sku')->nullable();

            $table->string('p_type')->nullable();
            $table->string('p_weight')->nullable();
            $table->string('p_part_number')->nullable();
            $table->string('p_video_id')->nullable();
            $table->string('p_color')->nullable();

            $table->float('p_mrp_price')->default(0);
            $table->float('p_sel_price')->default(0);

            $table->string('p_capacity')->nullable();
            $table->string('p_package_dimension')->nullable();
            $table->text('p_linked_products')->nullable();

            $table->string('p_availability')->default(0);
            $table->string('p_status');

            $table->text('p_primary_material')->nullable();
            $table->text('p_tech_details')->nullable();

            $table->string('p_meta_description')->nullable();
            $table->string('p_meta_keywords')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
