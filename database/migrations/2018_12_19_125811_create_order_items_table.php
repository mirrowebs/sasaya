<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('oitem_id');
            $table->integer('oitem_order_id');
            $table->string('oitem_product_id');
            $table->integer('oitem_is_freegift')->comment('1=yes 0=No');
            $table->string('oitem_product_name');
            $table->integer('oitem_qty');
            $table->float('oitem_product_price');
            $table->float('oitem_discount_price')->nullable();
            $table->string('oitem_discount_type')->nullable()->comment('% or fixed price');
            $table->float('oitem_sub_total');
            $table->text('oitem_note')->nullable();
            $table->string('oitem_type')->default('order_item')->comment('order item or gift');
            $table->string('oitem_status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
