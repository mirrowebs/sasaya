<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('order_user_id');
            $table->float('order_discount')->nullable();
            $table->string('order_discount_type')->nullable()->comment('% or fixed price');
            $table->float('order_total_price');
            $table->longText('order_delivery_address');
            $table->string('order_traking_number')->nullable();
            $table->string('order_courier_company')->nullable();
            $table->date('order_delivery_expected_date')->nullable();
            $table->string('order_payment_mode')->nullable();
            $table->date('order_payment_date')->nullable();
            $table->string('order_reference_number');
            $table->string('order_payment_transaction_id')->nullable();
            $table->float('order_shipping_price')->nullable();
            $table->float('order_sub_total_price')->nullable();
            $table->string('order_status')->default('pending');
            $table->string('order_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
