/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/frontend/modules/users/users.js":
/*!**********************************************************!*\
  !*** ./resources/assets/frontend/modules/users/users.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

jQuery(document).ready(function () {
  $('.userAddress').each(function () {
    $(this).validate({
      ignore: [],
      errorClass: 'text-danger',
      // You can change the animation class for a different entrance animation - check animations page
      errorElement: 'div',
      errorPlacement: function errorPlacement(error, e) {
        e.parents('.form-group').append(error);
      },
      highlight: function highlight(e) {
        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        $(e).closest('.text-danger').remove();
      },
      success: function success(e) {
        // You can use the following if you would like to highlight with green color the input after successful validation!
        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');

        e.closest('.text-danger').remove();
      },
      rules: {
        ua_name: {
          required: true
        },
        ua_address: {
          required: true
        },
        ua_landmark: {
          required: true
        },
        ua_city: {
          required: true
        },
        ua_state: {
          required: true
        },
        ua_email: {
          required: true
        },
        ua_phone: {
          required: true,
          number: true,
          minlength: 10,
          maxlength: 15
        },
        ua_country: {
          required: true
        },
        ua_pincode: {
          required: true
        }
      },
      messages: {
        ua_name: {
          required: 'Please enter name'
        },
        ua_address: {
          required: 'Please enter address'
        },
        ua_landmark: {
          required: 'Please enter landmark'
        },
        ua_city: {
          required: 'Please enter city'
        },
        ua_state: {
          required: 'Please enter state'
        },
        ua_email: {
          required: 'Please enter email'
        },
        ua_phone: {
          required: 'Please enter phone number',
          number: 'Please enter valid phone number',
          minlength: 'Please enter min 10 numbers',
          maxlength: 'Please enter max 15 numbers'
        },
        ua_country: {
          required: 'Please select country'
        },
        ua_pincode: {
          required: 'Please enter pincode'
        }
      }
    });
  });
  $('.delete_user_image').on('click', function () {
    var image = $(this).attr('id');

    if (image != '') {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: '/user/ajax/deleteuserimage',
        type: 'POST',
        data: {
          'image': image
        },
        success: function success(response) {
          $('.imagediv' + image).remove();
        }
      });
    }
  });
  jQuery('#profileInfo').validate({
    ignore: [],
    errorClass: 'text-danger',
    // You can change the animation class for a different entrance animation - check animations page
    errorElement: 'div',
    errorPlacement: function errorPlacement(error, e) {
      e.parents('.form-group').append(error);
    },
    highlight: function highlight(e) {
      $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
      $(e).closest('.text-danger').remove();
    },
    success: function success(e) {
      // You can use the following if you would like to highlight with green color the input after successful validation!
      e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');

      e.closest('.text-danger').remove();
    },
    rules: {
      name: {
        required: true
      },
      email: {
        required: true
      },
      mobile: {
        required: true
      },
      gender: {
        required: true
      },
      dob: {
        required: true
      }
    },
    messages: {
      name: {
        required: 'Enter enter name'
      },
      email: {
        required: 'Please enter email'
      },
      mobile: {
        required: 'Please enter mobile'
      },
      gender: {
        required: 'Please select gender'
      },
      dob: {
        required: 'Please select DOB'
      }
    }
  });
});

/***/ }),

/***/ 4:
/*!****************************************************************!*\
  !*** multi ./resources/assets/frontend/modules/users/users.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\projects\gonda\sasaya\resources\assets\frontend\modules\users\users.js */"./resources/assets/frontend/modules/users/users.js");


/***/ })

/******/ });