<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CCAvenue configuration file
    |--------------------------------------------------------------------------
    |   gateway = CCAvenue
    |   view    = File
     */

    'gateway' => 'CCAvenue', // Making this option for implementing multiple gateways in future

    'testMode' => false, // True for Testing the Gateway [For production false]

    'ccavenue' => [ // CCAvenue Parameters
        'merchantId' => env('CCAVENUE_MERCHANT_ID', '211075'),
        'accessCode' => env('CCAVENUE_ACCESS_CODE', 'AVQI83GC91AS45IQSA'),
        'workingKey' => env('CCAVENUE_WORKING_KEY', '555C9EEF51A48AECB9A781BC2076DFD3'),

        // Should be route address for url() function
        'redirectUrl' => env('CCAVENUE_REDIRECT_URL', '/payment/response'),
        'cancelUrl' => env('CCAVENUE_CANCEL_URL', '/payment/response'),

        'currency' => env('CCAVENUE_CURRENCY', 'INR'),
        'language' => env('CCAVENUE_LANGUAGE', 'EN'),
    ],

    // Add your response link here. In Laravel 5.* you may use the api middleware instead of this.
    'remove_csrf_check' => [
        'payment/response',
    ],

];
