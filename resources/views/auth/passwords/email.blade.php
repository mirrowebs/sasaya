<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Forgot Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('frontend._partials.stylesheets')

</head>

<body>
<!--main login and register -->
<main>
    <section class="sign">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 leftsign"></div>
                <div class="col-lg-6 align-self-center">
                    <div class="signin mx-auto">
                        <figure class="text-center signlogo"><img src="/frontend/images/logo.png" alt="" title=""></figure>
                        <article class="text-center py-2">
                            <h3>Forgot password?</h3>
                            <h4 class="pt-3 pb-2">Enter your details below.</h4>
                        </article>
                        <form class="formsign" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label>Enter your Registered Email Address</label>
                                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter Your Registered Email" id="email" type="email"
                                       name="email"
                                       value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<p class="text-center">You get Recet password link to your Registered Email Once you you will click on below link</p>--}}
                            {{--</div>--}}

                        <div class="text-center"><input class="btn" type="submit" value="RESET PASSWORD"></div>
                        </form>
                        <p class="text-center pt-4">Back to <span><a href="{{ route('userlogin') }}">Signin?</a></span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--/ main login and register -->
<!-- footer scripts -->
@include('frontend._partials.scripts')
<!--/ footer scripts -->
</body>
</html>