@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'categories'=>'Categories',
               ''=>'Add New'
               ),'Add New Category'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn container">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="categories" action="{{ route('addNewCategories') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="category_id" value="{{ $category_id }}">

                                <div class=" form-group">

                                    <label for="hf-email" class="form-control-label">Category</label>
                                    <div class="input-group">
                                        <select name="category_parent" id="category_parent" class="form-control">
                                            <option value="0">Parent</option>
                                            <?php
                                            $categories = getCategoriesByID($category_id); ?>
                                            @foreach($categories as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('category_parent')) && old('category_parent')==$ks)  ? 'selected' : ((($category) && ($category->category_parent == $ks)) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    @if ($errors->has('category_parent'))
                                        <span class="text-danger help-block">{{ $errors->first('category_parent') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Category Name</label>
                                    <div class="input-group">

                                        <input class="form-control" id="category_name" type="text"
                                               placeholder="Category Name"
                                               name="category_name"
                                               value="{{ !empty(old('category_name')) ? old('category_name') : ((($category) && ($category->category_name)) ? $category->category_name : '') }}">

                                    </div>
                                    @if ($errors->has('category_name'))
                                        <span class="text-danger help-block">{{ $errors->first('category_name') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class="form-control-label">Category Image</label>
                                    <div class="input-group">

                                        <input class="form-control {{ (($category) && ($category->category_image)) ? '' : 'categoryimage'}}"
                                               id="categoryimage" type="file" name="category_image">


                                        @if(($category) && ($category->category_image))
                                            <div class="imagediv{{ $category->category_id }}">
                                                <img width="150"
                                                     src="/uploads/categories/thumbs/{{$category->category_image}}"/>
                                                <a href="#" id="{{$category->category_id}}"
                                                   class="delete_category_image btn btn-danger btn-xs"
                                                   onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                            </div>
                                        @endif


                                    </div>
                                    @if ($errors->has('category_image'))
                                        <span class="text-danger">{{ $errors->first('category_image') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Status</label>
                                    <div class="input-group">
                                        <select name="category_status" id="category_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('category_status')) && old('category_status')==$ks)  ? 'selected' : ((($category) && ($category->category_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    @if ($errors->has('category_status'))
                                        <span class="text-danger help-block">{{ $errors->first('category_status') }}</span>
                                    @endif
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/categories/js/categories.js')}}"></script>

@endsection