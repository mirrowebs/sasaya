@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Users'
              ),'Users'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Users</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">DOB</th>
                                    <th scope="col">gender</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($users)>0)
                                    @foreach($users as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->name }}</td>
                                            <td>
                                                @if(!empty($item->image))

                                                    <img src="{{ '/uploads/users/thumbs/'.$item->image }}"/>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->mobile }}</td>
                                            <td>{{ ($item->dob) ? \Carbon\Carbon::parse($item->dob)->format('d/m/Y') : '-' }}</td>
                                            <td>{{ ($item->gender) ? $item->gender : '-' }}</td>
                                            <td>{{ allStatuses('general',$item->status) }}</td>
                                            <td>
                                                <div class="">
                                                    {{--<a class="btn btn-outline-primary dropdown-toggle" href="#"--}}
                                                       {{--role="button"--}}
                                                       {{--data-toggle="dropdown">--}}
                                                        {{--<i class="fa fa-ellipsis-h"></i>--}}
                                                    {{--</a>--}}
                                                    {{--<div class="dropdown-menu dropdown-menu-right">--}}
                                                        <a class="dropdown-item"
                                                           href="{{ route('userAddress',['id'=>$item->id]) }}"><i
                                                                    class="fa fa-eye"></i> View</a>
                                                       {{--<a class="dropdown-item"--}}
                                                           {{--href="{{ route('userOrders',['id'=>$item->id]) }}"><i--}}
                                                                    {{--class="fa fa-eye"></i> Orders </a>--}}

                                                    {{--</div>--}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $users->firstItem() }}
                        to {{ $users->lastItem() }} of {{ $users->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $users->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection