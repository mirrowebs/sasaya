@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'users'=>'Users',
              ''=>'User Orders'
              ),'User Orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                <div class="col-lg-4">
                    @include('backend.users.userinfo')
                </div>
                <div class="col-lg-8">

                    @include('backend.users.nav')

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">User Orders</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Added At</th>
                                    <th scope="col">View More</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($wishlist)>0)
                                    @foreach($wishlist as $item)

                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->getProduct->product_name }}</td>
                                            <td>{{ $item->getProduct->product_price }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>

                                                <a href="#" data-toggle="modal"
                                                   data-target="#productDetails{{ $item->ua_id }}"
                                                   class="text-uppercase pr-3 btn btn-default btn-sm">
                                                    <i class="far fa-eye"></i> View more
                                                </a>

                                                <!-- Modal -->
                                                <div id="productDetails{{ $item->ua_id }}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <table class="table table-striped">
                                                                        <tr>
                                                                            <td>
                                                                                @if(count($item->getProduct->productImages)>0)
                                                                                    <img src="/uploads/products/thumbs/{{ $item->getProduct->productImages[0]->pi_image_name }}">
                                                                                @endif


                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Product Name
                                                                                : {{ $item->getProduct->product_name }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Type
                                                                                : {{ $item->getProduct->product_type }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Availability
                                                                                : {{ $item->getProduct->product_availability }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Color
                                                                                : {{ $item->getProduct->product_color }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Real Price
                                                                                : {{ $item->getProduct->product_real_price }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Offer Price
                                                                                : {{ $item->getProduct->product_price }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Description
                                                                                : {!! $item->getProduct->product_description !!} </td>
                                                                        </tr>

                                                                    </table>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </td>

                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                 aria-live="polite">Showing {{ $wishlist->firstItem() }}
                                to {{ $wishlist->lastItem() }} of {{ $wishlist->total() }} entries
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                {!! $wishlist->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection