<div class="">
    <table class="table table-hover table-bordered">
        @if($userInfo->image)
            <tr>
                <td colspan="2">
                    <img src="{{ '/uploads/users/thumbs/'.$userInfo->image }}"/>
                </td>
            </tr>
        @endif
        <tr>
            <td>Name</td>
            <td>{{ $userInfo->name }}</td>
        </tr>
        <tr>
            <td>email</td>
            <td>{{ $userInfo->email }}</td>
        </tr>
        <tr>
            <td>mobile</td>
            <td>{{ $userInfo->mobile }}</td>
        </tr>
        <tr>
            <td>gender</td>
            <td>{{ $userInfo->gender }}</td>
        </tr>
        <tr>
            <td>status</td>
            <td>{{ $userInfo->status }}</td>
        </tr>
    </table>
</div>
