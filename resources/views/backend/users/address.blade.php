@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'users'=>'Users',
              ''=>'User Address'
              ),'User Address'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-4">
                    @include('backend.users.userinfo')
                </div>
                <div class="col-lg-8">

                    @include('backend.users.nav')


                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">User Address</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($userAddress)>0)
                                    @foreach($userAddress as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->ua_name }}</td>

                                            <td>{{ $item->ua_email }}</td>
                                            <td>{{ $item->ua_address }}</td>

                                            <td>{{ allStatuses('general',$item->ua_status) }}</td>
                                            <td>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#addressDetails{{ $item->ua_id }}"
                                                   class="text-uppercase pr-3 btn btn-default btn-sm">
                                                    <i class="far fa-eye"></i> View more
                                                </a>

                                                <!-- Modal -->
                                                <div id="addressDetails{{ $item->ua_id }}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <table class="table table-striped">
                                                                        <tr>
                                                                            <td>Name : {{ $item->ua_name }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Address : {{ $item->ua_address }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Landmark : {{ $item->ua_landmark }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>City : {{ $item->ua_city }}</td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>State : {{ $item->ua_state }}</td>

                                                                        </tr>
                                                                        <tr>

                                                                            <td>Email : {{ $item->ua_email }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Phone : {{ $item->ua_phone }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Default
                                                                                : {{ ($item->ua_defult==1) ? 'Yes' : 'No' }}</td>

                                                                        </tr>
                                                                        <tr>

                                                                            <td>Country
                                                                                : {{ $item->getCountry->country_name }}</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td>Status
                                                                                : {{ allStatuses('general',$item->ua_status) }}</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                 aria-live="polite">Showing {{ $userAddress->firstItem() }}
                                to {{ $userAddress->lastItem() }} of {{ $userAddress->total() }} entries
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                {!! $userAddress->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('ua_status')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection