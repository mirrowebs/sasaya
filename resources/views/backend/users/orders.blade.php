@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'users'=>'Users',
              ''=>'User Orders'
              ),'User Orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                <div class="col-lg-4">
                   @include('backend.users.userinfo')
                </div>
                <div class="col-lg-8">

                  @include('backend.users.nav')

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">User Orders</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Reference Number</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($orders)>0)
                                    @foreach($orders as $item)
                                        @php
                                            $address=unserialize($item->order_delivery_address);
                                        @endphp
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->order_reference_number }}</td>
                                            <td>{{ $address['ua_name'] }},{{ $address['ua_address'] }}
                                                ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                                ,{{ $address['ua_country'] }}</td>
                                            <td><i class="fa fa-inr"></i>{{ $item->order_total_price }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>{{ $item->order_status }}</td>
                                            <td>

                                                <a href="#" data-toggle="modal"
                                                   data-target="#orderDetails{{ $item->order_id }}"
                                                   class="text-uppercase pr-3 btn btn-default btn-sm">
                                                    <i class="far fa-eye"></i> Order Details
                                                </a>

                                                <!-- Modal -->
                                                <div id="orderDetails{{ $item->order_id }}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog modal-lg">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                    Order : {{ $item->order_reference_number }}</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    @foreach($item->orderItems as $orits)
                                                                        @if (!empty($orits->getProduct->productImages))
                                                                        <div class="col-lg-2">
                                                                            <figure class=timezone"cartimg">
                                                                                <a href="#">
                                                                                    <img src="/uploads/products/thumbs/{{ $orits->getProduct->productImages[0]->pi_image_name }}"  />
                                                                                </a>
                                                                            </figure>
                                                                        </div>
                                                                        @endif
                                                                        <div class="col-lg-6 align-self-center">
                                                                            <h5 class="fmed h6">{{ $orits->getProduct->product_name }}</h5>
                                                                            <p class="fgray ">{!! $orits->getProduct->product_description !!}</p>

                                                                        </div>
                                                                        <div class="col-lg-2 align-self-center">
                                                                            <h5 class="fmed h6">{{ $orits->oitem_qty }}
                                                                                X {{ $orits->oitem_product_price }}</h5>
                                                                        </div>
                                                                        <div class="col-lg-2 align-self-center">
                                                                            <h5 class="fmed h6"><i
                                                                                        class="fa fa-inr"></i>{{ $orits->oitem_sub_total }}
                                                                            </h5>
                                                                        </div>

                                                                    @endforeach
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-4 align-self-right text-center">
                                                                     Total :
                                                                    </div>
                                                                   <div class="col-lg-4 align-self-right text-center">
                                                                        <h1 class="price h1"><i
                                                                                    class="fa fa-inr"></i>
                                                                            {{ $item->order_total_price }}</h1>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </td>

                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                 aria-live="polite">Showing {{ $orders->firstItem() }}
                                to {{ $orders->lastItem() }} of {{ $orders->total() }} entries
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                {!! $orders->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection