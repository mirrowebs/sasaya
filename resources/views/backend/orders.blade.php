@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Orders'
              ),'Orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Orders</strong>

                        </div>
                        <div class="card-body">


                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Reference Number</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($orders)>0)
                                    @foreach($orders as $item)
                                        @php
                                            $address=unserialize($item->order_delivery_address);
                                        @endphp



                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->order_reference_number }}</td>
                                            <td>{{ $address['ua_name'] }},{{ $address['ua_address'] }}
                                                ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                                ,{{ $address['ua_country'] }}</td>
                                            <td><i class="fa fa-inr"></i>{{ $item->order_total_price }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>{{ $item->order_status }}</td>
                                            <td>


                                                <a href="#" data-toggle="modal"
                                                   data-target="#orderDetails{{ $item->order_id }}"
                                                   class="text-uppercase pr-3 btn btn-default btn-sm">
                                                    <i class="far fa-eye"></i> Order Details
                                                </a>

                                                <!-- Modal -->
                                                <div id="orderDetails{{ $item->order_id }}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                    Order : {{ $item->order_reference_number }}</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">


                                                                    @if(count($item->orderItems)> 0)
                                                                        @foreach($item->orderItems as $orits)



                                                                            @if($orits->oitem_is_freegift==1)


                                                                                <?php

                                                                                if (!empty($orits->getFreeProduct->free_gift_cover_image)) {
                                                                                    $pname = '/uploads/products/thumbs/' . $orits->getFreeProduct->free_gift_cover_image;
                                                                                } else {

                                                                                    $pname = '/frontend/images/data/plant01.png';
                                                                                }
                                                                                ?>


                                                                                <div class="col-lg-2">
                                                                                    <figure class=timezone"cartimg">
                                                                                        <a href="#">
                                                                                            <img src="{{ $pname }}"
                                                                                                 width="150"
                                                                                                 height="150"/>
                                                                                        </a>
                                                                                    </figure>
                                                                                </div>

                                                                                <div class="col-lg-6 align-self-center">
                                                                                    <h5 class="fmed h6">{{ $orits->getFreeProduct->free_gift_name }}</h5>
                                                                                    <p class="fgray">{!! str_limit($orits->getFreeProduct->free_gift_description, 150) !!}</p>

                                                                                </div>
                                                                                <div class="col-lg-2 align-self-center">
                                                                                    <h5 class="fmed h6">{{ $orits->oitem_qty }}
                                                                                        X {{ $orits->oitem_product_price }}</h5>
                                                                                </div>
                                                                                <div class="col-lg-2 align-self-center">
                                                                                    <h5 class="fmed h6">
                                                                                        <i class="fa fa-inr"></i>{{ $orits->oitem_sub_total }}
                                                                                    </h5>
                                                                                </div>

                                                                            @else


                                                                                <?php

                                                                                if (isset($orits->getProduct)) {
                                                                                    if (isset($orits->getProduct->product_cover_image)) {


                                                                                        $pname = '/uploads/products/thumbs/' . $orits->getProduct->product_cover_image;
                                                                                    } else {
                                                                                        dd($orits->getProduct);


                                                                                        $productImages = getProductImages($orits->getProduct->product_id);

                                                                                        dd($productImages);

                                                                                        if (count($productImages) > 0) {
                                                                                            $pname = '/uploads/products/thumbs/' . $productImages[0]->pi_image_name;
                                                                                        } else {
                                                                                            $pname = '/frontend/images/data/plant01.png';

                                                                                        }
                                                                                    }
                                                                                }else {
                                                                                    $pname = '/frontend/images/data/plant01.png';

                                                                                }



                                                                                ?>

                                                                                <div class="col-lg-2">
                                                                                    <figure class=timezone"cartimg">
                                                                                        <a href="#">
                                                                                            <img src="{{ $pname }}"
                                                                                                 width="150"
                                                                                                 height="150"/>
                                                                                        </a>
                                                                                    </figure>
                                                                                </div>

                                                                                <div class="col-lg-6 align-self-center">
                                                                                    {{--                                                                                    <h5 class="fmed h6">{{ $orits->getProduct->product_name }}</h5>--}}
                                                                                    {{--                                                                                    <p class="fgray">{!! str_limit($orits->getProduct->product_description, 150) !!}</p>--}}

                                                                                </div>
                                                                                <div class="col-lg-2 align-self-center">
                                                                                    <h5 class="fmed h6">{{ $orits->oitem_qty }}
                                                                                        X {{ $orits->oitem_product_price }}</h5>
                                                                                </div>
                                                                                <div class="col-lg-2 align-self-center">
                                                                                    <h5 class="fmed h6"><i
                                                                                                class="fa fa-inr"></i>{{ $orits->oitem_sub_total }}
                                                                                    </h5>
                                                                                </div>

                                                                            @endif

                                                                        @endforeach

                                                                    @endif

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-4 align-self-right text-center">
                                                                        Total :
                                                                    </div>
                                                                    <div class="col-lg-4 align-self-right text-center">
                                                                        <h1 class="price h1"><i
                                                                                    class="fa fa-inr"></i>
                                                                            {{ $item->order_total_price }}</h1>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </td>

                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $orders->firstItem() }}
                        to {{ $orders->lastItem() }} of {{ $orders->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $orders->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection