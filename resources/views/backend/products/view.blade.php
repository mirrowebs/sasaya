@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'products'=>'Products',
               ''=>'Add New'
               ),'Add New Product'
            ) !!}

    <ul class="nav nav-tabs">
        @if($product_id)
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('productInfo',['id'=>$product_id]) }}">Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{ route('addNewProducts',['id'=>$product_id]) }}">Update Product</a>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('addNewProducts') }}">Add Product</a>
            </li>
        @endif

    </ul>


    <div class="content mt-3">
        <div class="animated fadeIn">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <div class="row">
                                <div class="col-md-3"> Name :</div>
                                <div class="col-md-6">{{ $product->product_name }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Alias :</div>
                                <div class="col-md-6">{{ $product->product_alias }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Description :</div>
                                <div class="col-md-6">{{ $product->product_description }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Weight :</div>
                                <div class="col-md-6">{{ $product->product_weight }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Color :</div>
                                <div class="col-md-6">{{ $product->product_color }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Package Dimension :</div>
                                <div class="col-md-6">{{ $product->product_package_dimension }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Part number :</div>
                                <div class="col-md-6">{{ $product->product_part_number }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Primary material :</div>
                                <div class="col-md-6">{{ $product->product_primary_material }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Capacity :</div>
                                <div class="col-md-6">{{ $product->product_capacity }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Real Price :</div>
                                <div class="col-md-6">{{ $product->product_real_price }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> Price :</div>
                                <div class="col-md-6">{{ $product->product_price }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"> SKU :</div>
                                <div class="col-md-6">{{ $product->product_sku }}</div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/products/js/products.js')}}"></script>

    <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>

@endsection