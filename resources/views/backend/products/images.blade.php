@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'products'=>'Products',
               ''=>'Add Images to product'
               ),'Add Images to product'
            ) !!}


    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('productInfo',['id'=>$product_id]) }}">Info</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{ route('addNewProducts',['id'=>$product_id]) }}">Edit</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('addProductImages',['id'=>$product_id]) }}">Add Images</a>
        </li>
    </ul>

    <div class="content mt-3">
        <div class="animated fadeIn">


            <div class="row">
                <div class="col-md-12">
                    @if(count($product_images)>0)
                        @foreach($product_images as $item)
                            <div class="col-md-2 imagediv{{ $item->pi_id }}">
                                <img class="img-responsive" src="/uploads/products/thumbs/{{ $item->pi_image_name }}"/>
                                <a href="#" id="{{$item->pi_id}}"
                                   class="delete_product_image btn btn-danger btn-xs"
                                   onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-2">
                            No records found
                        </div>
                    @endif
                </div>
            </div>
            <br/><br/>


            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="productImages"
                                  action="{{ route('addProductImages',['id'=>$product_id]) }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Images</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="productimage" type="file"
                                               name="productimages[]" multiple>
                                        @if ($errors->has('productimages[]'))
                                            <span class="text-danger">{{ $errors->first('productimages[]') }}</span>
                                        @endif
                                        <i>Note: upload images size is 800px/800px</i>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/products/js/products.js')}}"></script>

@endsection