<script>

    $(function () {
        $('#p_category').on('change', function () {
            var category = $('#p_category').val();
            $('#p_sub_category').children('option').remove();
            if (category != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/admin/ajax/getsubcategories',
                    type: 'POST',
                    data: {'category': category},
                    success: function (response) {
                        var resultdata = $.parseJSON(response);
                        var length = Object.keys(resultdata.categories).length;
                        if (length > 0) {
                            $('#p_sub_category').append(new Option('Select', '', false, false));
                            $.each(resultdata.categories, function (k, v) {
                                var subcatId = $('#p_sub_category').data('oldid');

                                // console.log($('#p_sub_category').data('oldid'));

                                if (subcatId == k) {
                                // if ($('.p_sub_category').val() == k) {
                                    $('#p_sub_category').append(new Option(v, k, true, true));
                                } else {
                                    $('#p_sub_category').append(new Option(v, k, false, false));
                                }

                            });
                        } else {
                            $('#p_sub_category').append(new Option('There is no sub categories', '', false, false));
                        }
                    }
                });
            } else {
                $('#p_sub_category').append(new Option('Select category first', '', false, false));
            }
        }).change();

        $('.delete_product_image').on('click', function () {
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('ajax_deleteproductimage') }}',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.imagediv' + image).remove();
                    }
                });
            }
        });
        $('.delete_cover_image').on('click', function () {
            alert('hai');
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/admin/ajax/deleteproductcoverimage',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.coverimagediv' + image).remove();
                        $('#productcoverimage').addClass('productcoverimage');
                        imageRules();
                    }
                });
            }
        });

        $('#productImages').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                'productimages[]': {
                    required: true
                }
            },
            messages: {
                'productimages[]': {
                    required: 'Select any one image'
                }
            },
        });

        $('#products').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {

                p_category: {
                    required: true
                },

                product_name: {
                    required: true
                },
                product_type: {
                    required: true
                },
                product_availability: {
                    required: true
                },
                product_color: {
                    required: true
                },
                product_description: {
                    required: true
                },
                product_price: {
                    required: true,
                    number: true
                },

                product_discount: {
                    required: true,
                    number: true
                },
                product_status: {
                    required: true
                }
            },
            messages: {
                product_name: {
                    required: 'Please enter name'
                },
                product_type: {
                    required: 'Please enter type'
                },
                product_availability: {
                    required: 'Please enter availability'
                },
                product_color: {
                    required: 'Please enter color'
                },
                product_description: {
                    required: 'Please enter description'
                },
                product_price: {
                    required: 'Please enter price'
                },
                product_category: {
                    required: 'Please select category'
                },
                product_discount: {
                    required: 'Please enter discount',
                    number: 'Please enter numbers only'
                },
                product_status: {
                    required: 'Please select status'
                }
            },
        });
        imageRules();
    });

    function imageRules() {
        $(".productcoverimage").rules("add", {
            required: true,
            messages: {
                required: "Upload image to save"
            }
        });
    }


</script>