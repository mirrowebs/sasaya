@extends('backend.layout')
@section('title', $title)

@section('headerStyles')
    <style>

        .ck-editor__editable {
            min-width: 800px;
            min-height: 200px;
        }
    </style>

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'products'=>'Products',
               ''=>'Add New'
               ),'Add New Product'
            ) !!}




    <div class="content mt-3">
        <div class="animated fadeIn ">


            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="products" action="{{ route('addNewProducts') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="p_id" value="{{ $p_id }}">

                                {{--                                @if(isset($product->product_sub_category))--}}
                                {{--                                    <input type="hidden" class="product_sub_category"--}}
                                {{--                                           value="{{$product->product_sub_category}}">--}}
                                {{--                                    @php--}}
                                {{--                                        $optionselected='';--}}
                                {{--                                        $optionselected=$product->product_sub_category;--}}
                                {{--                                    @endphp--}}
                                {{--                                @else--}}
                                {{--                                    @php--}}
                                {{--                                        $optionselected='';--}}
                                {{--                                        $optionselected=old('product_sub_category');--}}
                                {{--                                    @endphp--}}
                                {{--                                @endif--}}

                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="card">
                                            <div class="card-header">
                                                Product Info
                                            </div>
                                            <div class="card-body">

                                                <div class=" form-group">

                                                    <label for="hf-email" class="form-control-label">Category</label>
                                                    <div class="input-group">
                                                        <select name="p_category" id="p_category"
                                                                class="form-control">
                                                            <option value="">Select</option>
                                                            <?php
                                                            $categories = getCategoriesByID(); ?>
                                                            @foreach($categories as $ks=>$s)
                                                                <option value="{{ $ks }}" {{ (!empty(old('p_category')) && old('p_category')==$ks)  ? 'selected' : ((($product) && ($product->p_category == $ks)) ? 'selected' : '') }}>
                                                                    {{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                    </div>

                                                    {!! formValidationError($errors, 'p_category') !!}

                                                </div>


                                                <?php
                                                //                                                dump($product);
                                                ?>

                                                <div class=" form-group">

                                                    <label for="hf-email" class="form-control-label">
                                                        Sub Category
                                                    </label>
                                                    <div class="input-group">

                                                        @if($p_id!='')

                                                            <select class="form-control select2 p_sub_category"
                                                                    name="p_sub_category" id="p_sub_category"
                                                                    data-oldid="{{ $product->p_sub_category }}">
                                                                @foreach (json_decode('{"": "Select category first"}', true) as $optionKey => $optionValue)
                                                                    <option value="{{ $optionKey }}" {{ ($product->p_sub_category == $optionKey) ? 'selected' : ''}}>
                                                                        {{ $optionValue }}
                                                                    </option>
                                                                @endforeach
                                                            </select>

                                                        @else
                                                            <select class="form-control select2 p_sub_category"
                                                                    name="p_sub_category" id="p_sub_category"
                                                                   >

                                                            </select>

                                                        @endif


                                                    </div>
                                                    {!! formValidationError($errors, 'p_sub_category') !!}


                                                </div>

                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        name</label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_name" type="text"
                                                               placeholder="Product name"
                                                               name="p_name"
                                                               value="{{ !empty(old('p_name')) ? old('p_name') : ((($product) && ($product->p_name)) ? $product->p_name : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_name') !!}


                                                </div>

                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        sku</label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_sku" type="text"
                                                               placeholder="Product sku"
                                                               name="p_sku"
                                                               value="{{ !empty(old('p_sku')) ? old('p_sku') : ((($product) && ($product->p_sku)) ? $product->p_sku : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_sku') !!}
                                                </div>

                                            </div>
                                        </div>


                                        <div class="card">
                                            <div class="card-header">
                                                Product Options
                                            </div>
                                            <div class="card-body">


                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        Type</label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_type" type="text"
                                                               placeholder="Product Type"
                                                               name="p_type"
                                                               value="{{ !empty(old('p_type')) ? old('p_type') : ((($product) && ($product->p_type)) ? $product->p_type : '') }}">

                                                    </div>

                                                    {!! formValidationError($errors, 'p_type') !!}


                                                </div>


                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        Weight</label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_weight" type="number"
                                                               placeholder="Product Weight"
                                                               name="p_weight"
                                                               value="{{ !empty(old('p_weight')) ? old('p_weight') : ((($product) && ($product->p_weight)) ? $product->p_weight : '') }}">


                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Kgs
                                                            </span>
                                                        </div>

                                                    </div>

                                                    {!! formValidationError($errors, 'p_weight') !!}

                                                </div>


                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">
                                                        Product Part Number
                                                    </label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_part_number" type="text"
                                                               placeholder="Product part number"
                                                               name="p_part_number"
                                                               value="{{ !empty(old('p_part_number')) ? old('p_part_number') : ((($product) && ($product->p_part_number)) ? $product->p_part_number : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_part_number') !!}
                                                </div>
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">
                                                        Product Video
                                                    </label>
                                                    <div class="input-group">

                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                youtube Video ID
                                                            </span>
                                                        </div>


                                                        <input class="form-control" id="p_video_id" type="text"
                                                               placeholder="Product video"
                                                               name="p_video_id"
                                                               value="{{ !empty(old('p_video_id')) ? old('p_video_id') : ((($product) && ($product->p_video_id)) ? $product->p_video_id : '') }}">


                                                    </div>
                                                    {!! formValidationError($errors, 'p_video_id') !!}
                                                </div>
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        Color</label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_color" type="text"
                                                               placeholder="Product Color"
                                                               name="p_color"
                                                               value="{{ !empty(old('p_color')) ? old('p_color') : ((($product) && ($product->p_color)) ? $product->p_color : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_color') !!}
                                                </div>


                                            </div>
                                        </div>


                                        {{--                                        <div class="card">--}}
                                        {{--                                            <div class="card-header">--}}
                                        {{--                                                Product Info--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="card-body">--}}

                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}


                                    </div>
                                    <div class="col-md-6">


                                        <div class="card">
                                            <div class="card-header">
                                                Gallery Image
                                            </div>
                                            <div class="card-body">

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="card">

                                                            <div class="card-body card-block">


                                                                <div class=" form-group">

                                                                    <label for="hf-email" class="form-control-label">
                                                                        Product Images
                                                                    </label>
                                                                    <div class="input-group">

                                                                        <input class="form-control" id="p_images"
                                                                               type="file"
                                                                               name="p_images[]" multiple>

                                                                    </div>

                                                                    {{--                                                                    {!! formValidationError($errors, 'p_images[]') !!}--}}

                                                                    <i>Note: upload images size is 800px/800px</i>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        @if(count($product_images)>0)
                                                                            @foreach($product_images as $item)
                                                                                <?php

                                                                                $getimagePath = productImageUrl('thumb');


                                                                                ?>

                                                                                <div class="col-md-4 imagediv{{ $item->pi_id }}">
                                                                                    <img class="img-fluid"
                                                                                         src="{{ $getimagePath.$item->pi_image_name }}"/>
                                                                                    <a href="#" id="{{$item->pi_id}}"
                                                                                       class="delete_product_image btn btn-danger btn-xs"
                                                                                       onclick="return confirm('Confirm delete?')">
                                                                                        <i class="fa fa-trash-o"
                                                                                           aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="card">
                                            <div class="card-header">
                                                Product Price Info
                                            </div>
                                            <div class="card-body">


                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product real
                                                        Price</label>
                                                    <div class="input-group">

                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                              Rs.
                                                            </span>
                                                        </div>

                                                        <input class="form-control" id="p_mrp_price" type="number"
                                                               placeholder="Product Price"
                                                               name="p_mrp_price"
                                                               value="{{ !empty(old('p_mrp_price')) ? old('p_mrp_price') : ((($product) && ($product->p_mrp_price)) ? $product->p_mrp_price : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_mrp_price') !!}
                                                </div>
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product Selling
                                                        Price</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                              Rs.
                                                            </span>
                                                        </div>

                                                        <input class="form-control" id="p_sel_price" type="number"
                                                               placeholder="Product Price"
                                                               name="p_sel_price"
                                                               value="{{ !empty(old('p_sel_price')) ? old('p_sel_price') : ((($product) && ($product->p_sel_price)) ? $product->p_sel_price : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_sel_price') !!}
                                                </div>
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        Capacity</label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_capacity" type="text"
                                                               placeholder="Product Capacity"
                                                               name="p_capacity"
                                                               value="{{ !empty(old('p_capacity')) ? old('p_capacity') : ((($product) && ($product->p_capacity)) ? $product->p_capacity : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_capacity') !!}
                                                </div>

                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">
                                                        Product Package dimension
                                                    </label>
                                                    <div class="input-group">

                                                        <input class="form-control" id="p_package_dimension"
                                                               type="text"
                                                               placeholder="Product package dimension"
                                                               name="p_package_dimension"
                                                               value="{{ !empty(old('p_package_dimension')) ? old('p_package_dimension') : ((($product) && ($product->p_package_dimension)) ? $product->p_package_dimension : '') }}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_package_dimension') !!}
                                                </div>


                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">
                                                        Linked
                                                        Products</label>
                                                    <div class="input-group">
                                                        <input name="p_linked_products"
                                                               id="p_linked_products"
                                                               data-role="tagsinput"
                                                               placeholder="Linked products..."
                                                               class="form-control "
                                                               value="{!! !empty(old('p_linked_products')) ? old('p_linked_products') : ((($product) && ($product->p_linked_products)) ? $product->p_linked_products : '') !!}">

                                                    </div>
                                                    {!! formValidationError($errors, 'p_linked_products') !!}
                                                </div>


                                            </div>
                                        </div>


                                    </div>

                                </div>


                                <div class="card">
                                    <div class="card-header">
                                        Product Settings
                                    </div>
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Product
                                                        Availability</label>
                                                    <div class="input-group">
                                                        <select name="p_availability" id="p_availability"
                                                                class="form-control">
                                                            <?php
                                                            $availability = availability(); ?>
                                                            @foreach($availability as $ks=>$s)
                                                                <option value="{{ $ks }}" {{ (!empty(old('p_availability')) && old('p_availability')==$ks)  ? 'selected' : ((($product) && ($product->p_availability == $ks)) ? 'selected' : '') }}
                                                                >
                                                                    {{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                    {!! formValidationError($errors, 'p_availability') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Status</label>
                                                    <div class="input-group">
                                                        <select name="p_status" id="p_status"
                                                                class="form-control">
                                                            <?php
                                                            $status = allStatuses('general'); ?>
                                                            @foreach($status as $ks=>$s)
                                                                <option value="{{ $ks }}" {{ (!empty(old('p_status')) && old('p_status')==$ks)  ? 'selected' : ((($product) && ($product->p_status == $ks)) ? 'selected' : '') }}
                                                                >
                                                                    {{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                    {!! formValidationError($errors, 'p_status') !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="card">
                                    <div class="card-header">
                                        Description
                                    </div>
                                    <div class="card-body">


                                        <div class=" form-group">

                                            <label for="hf-email" class=" form-control-label">Product Tech
                                                Details</label>
                                            <div class="input-group">
                                        <textarea name="p_tech_details" id="p_tech_details"
                                                  rows="15"
                                                  placeholder="Product Tech Detailsl..."
                                                  class="form-control ckeditor">{!! !empty(old('p_tech_details')) ? old('p_tech_details') : ((($product) && ($product->p_tech_details)) ? $product->p_tech_details : '') !!}</textarea>

                                            </div>
                                            {!! formValidationError($errors, 'p_tech_details') !!}
                                        </div>

                                        <div class=" form-group">

                                            <label for="hf-email" class=" form-control-label">Product Primary
                                                Material</label>
                                            <div class="input-group">
                                        <textarea name="p_primary_material" id="p_primary_material"
                                                  rows="15"
                                                  placeholder="Product Primary Material..."
                                                  class="form-control ckeditor ">{!! !empty(old('p_primary_material')) ? old('p_primary_material') : ((($product) && ($product->p_primary_material)) ? $product->p_primary_material : '') !!}</textarea>

                                            </div>
                                            {!! formValidationError($errors, 'p_primary_material') !!}
                                        </div>


                                    </div>
                                </div>


                                <div class="card">
                                    <div class="card-header">
                                        Meta Information
                                    </div>
                                    <div class="card-body">

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Meta
                                                        Description</label>
                                                    <div class="input-group">
                                        <textarea name="p_meta_description" id="p_meta_description" rows="6"
                                                  placeholder="Description..."
                                                  class="form-control ">{!! !empty(old('p_meta_description')) ? old('p_meta_description') : ((($product) && ($product->p_meta_description)) ? $product->p_meta_description : '') !!}</textarea>

                                                    </div>

                                                    {!! formValidationError($errors, 'p_meta_description') !!}

                                                </div>

                                            </div>
                                            <div class="col-md-6">

                                                <div class=" form-group">

                                                    <label for="hf-email" class=" form-control-label">Meta
                                                        Keywords</label>
                                                    <div class="input-group">
                                        <textarea name="p_meta_keywords" id="p_meta_keywords" rows="6"
                                                  placeholder="Keywords..."
                                                  class="form-control ">{!! !empty(old('p_meta_keywords')) ? old('p_meta_keywords') : ((($product) && ($product->p_meta_keywords)) ? $product->p_meta_keywords : '') !!}</textarea>

                                                    </div>
                                                    {!! formValidationError($errors, 'p_meta_keywords') !!}
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    @include('backend.products.scripts')




@endsection