@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Products'
              ),'Products'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                            <a href="{{route('addNewProducts')}}" class="btn btn-primary btn-xs" style="float:right;">Add
                                Products
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Sub Category</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Avbailability</th>
                                    <th scope="col">MRP Price</th>
                                    <th scope="col">Selling Price</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($products)>0)
                                    @foreach($products as $item)
                                        <?php
                                                $imagePath = '';
                                                if(count($item->productImages) > 0 && isset($item->productImages[0])){

                                                        $getimagePath = productImageUrl('thumb');

                                                        $imagePath = $getimagePath.$item->productImages[0]->pi_image_name;

                                                }else{
                                                    $imagePath = '';
                                                }


                                        ?>

                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>
                                                @if($imagePath!='')
                                                    <img width="150" src="{{$imagePath}}" class="img-fluid" />
                                                    @endif


                                            </td>
                                            <td>{{ (isset($item->getCategory))?$item->getCategory->category_name:'' }}</td>
                                            <td>{{ (isset($item->getSubCategory))?$item->getSubCategory->category_name:'' }}</td>
                                            <td>{{ $item->p_name }}</td>

                                            <td>{{ $item->p_type }}</td>
                                            <td>{{ availability($item->p_availability) }}</td>
                                            <td>
                                                {!! getCurency() !!}
                                                {{ $item->p_mrp_price }}
                                            </td>

                                            <td>
                                                {!! getCurency() !!}
                                                {!! $item->p_sel_price !!}
                                            </td>
                                            <td>{{ allStatuses('general',$item->p_status) }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                       role="button"
                                                       data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="{{ route('productInfo',['id'=>$item->p_id]) }}">
                                                            <i class="fa fa-plus"></i>
                                                            Product Info
                                                        </a>

                                                        <a class="dropdown-item" href="{{ route('addNewProducts',['id'=>$item->p_id]) }}">
                                                            <i class="fa fa-plus"></i>
                                                            Product Edit
                                                        </a>


                                                        <form method="POST" id="products"
                                                              action="{{ route('products') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="p_id"
                                                                   value="{{ $item->p_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Product"
                                                                    onclick="return confirm(' Confirm delete ?')">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </button>

                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $products->firstItem() }}
                        to {{ $products->lastItem() }} of {{ $products->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $products->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('p_status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection