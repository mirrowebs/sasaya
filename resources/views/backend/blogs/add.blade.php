@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blogs'=>'Blogs',
               ''=>'Add New'
               ),'Add New Blog'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn container">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blogs" action="{{ route('addNewBlogs') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="blog_id" value="{{ $blog_id }}">


                                <div class=" form-group">

                                    <label for="hf-email" class="form-control-label">Category</label>
                                    <div class="input-group">
                                        <select name="blog_bc_id" id="blog_bc_id" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            $categories = getBlogCategoriesById(); ?>
                                            @foreach($categories as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('blog_bc_id')) && old('blog_bc_id')==$ks)  ? 'selected' : ((($blog) && ($blog->blog_bc_id == $ks)) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    @if ($errors->has('blog_bc_id'))
                                        <span class="text-danger help-block">{{ $errors->first('blog_bc_id') }}</span>
                                    @endif
                                </div>

                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Name</label>
                                    <div class="input-group">

                                        <input class="form-control" id="blog_name" type="text"
                                               placeholder="Blog Name"
                                               name="blog_name"
                                               value="{{ !empty(old('blog_name')) ? old('blog_name') : ((($blog) && ($blog->blog_name)) ? $blog->blog_name : '') }}">

                                    </div>
                                    @if ($errors->has('blog_name'))
                                        <span class="text-danger help-block">{{ $errors->first('blog_name') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class="form-control-label">Blog Image</label>
                                    <div class="input-group">

                                        <input class="form-control {{ (($blog) && ($blog->blog_img)) ? '' : 'categoryimage'}}"
                                               id="categoryimage" type="file" name="blog_img">


                                        @if(($blog) && ($blog->blog_img))
                                            <div class="imagediv{{ $blog->blog_id }}">
                                                <img width="150"
                                                     src="/uploads/blogs/thumbs/{{$blog->blog_img}}"/>
                                                {{--<a href="#" id="{{$blog->blog_id}}"--}}
                                                {{--class="delete_category_image btn btn-danger btn-xs"--}}
                                                {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i--}}
                                                {{--class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>--}}
                                            </div>
                                        @endif


                                    </div>
                                    @if ($errors->has('blog_img'))
                                        <span class="text-danger">{{ $errors->first('blog_img') }}</span>
                                    @endif

                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Tags</label>
                                    <div class="input-group">

                                        <input class="form-control" id="blog_tags" type="text"
                                               placeholder="Blog Tags"
                                               name="blog_tags"
                                               value="{{ !empty(old('blog_tags')) ? old('blog_tags') : ((($blog) && ($blog->blog_tags)) ? $blog->blog_tags : '') }}">

                                    </div>
                                    @if ($errors->has('blog_tags'))
                                        <span class="text-danger help-block">{{ $errors->first('blog_tags') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Description</label>
                                    <div class="input-group">
                                        <textarea name="blog_desc" id="blog_desc" rows="15"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{!! !empty(old('blog_desc')) ? old('blog_desc') : ((($blog) && ($blog->blog_desc)) ? $blog->blog_desc : '') !!}</textarea>

                                    </div>
                                    @if ($errors->has('blog_desc'))
                                        <span class="text-danger help-block">{{ $errors->first('blog_desc') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Status</label>
                                    <div class="input-group">
                                        <select name="blog_status" id="blog_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('blog_status')) && old('blog_status')==$ks)  ? 'selected' : ((($blog) && ($blog->blog_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    @if ($errors->has('blog_status'))
                                        <span class="text-danger help-block">{{ $errors->first('blog_status') }}</span>
                                    @endif
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/blogs/js/blogs.js')}}"></script>

@endsection