@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blogCategories'=>'Blog Categories',
               ''=>'Add New'
               ),'Add New Category'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn container">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blogcategories" action="{{ route('addNewBlogCategories') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="bc_id" value="{{ $bc_id }}">


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Category Name</label>
                                    <div class="input-group">

                                        <input class="form-control" id="bc_name" type="text"
                                               placeholder="Category Name"
                                               name="bc_name"
                                               value="{{ !empty(old('bc_name')) ? old('bc_name') : ((($category) && ($category->bc_name)) ? $category->bc_name : '') }}">

                                    </div>
                                    @if ($errors->has('bc_name'))
                                        <span class="text-danger help-block">{{ $errors->first('bc_name') }}</span>
                                    @endif
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class="form-control-label">Category Image</label>
                                    <div class="input-group">

                                        <input class="form-control {{ (($category) && ($category->bc_img)) ? '' : 'categoryimage'}}"
                                               id="categoryimage" type="file" name="bc_img">


                                        @if(($category) && ($category->bc_img))
                                            <div class="imagediv{{ $category->bc_id }}">
                                                <img width="150"
                                                     src="/uploads/blogs/categories/thumbs/{{$category->bc_img}}"/>
                                                {{--<a href="#" id="{{$category->bc_id}}"--}}
                                                {{--class="delete_category_image btn btn-danger btn-xs"--}}
                                                {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i--}}
                                                {{--class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>--}}
                                            </div>
                                        @endif


                                    </div>
                                    @if ($errors->has('bc_img'))
                                        <span class="text-danger">{{ $errors->first('bc_img') }}</span>
                                    @endif

                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Status</label>
                                    <div class="input-group">
                                        <select name="bc_status" id="bc_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('bc_status')) && old('bc_status')==$ks)  ? 'selected' : ((($category) && ($category->bc_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    @if ($errors->has('bc_status'))
                                        <span class="text-danger help-block">{{ $errors->first('bc_status') }}</span>
                                    @endif
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/blogs/js/categories.js')}}"></script>

@endsection