@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'freegifts'=>'Free Gifts',
               ''=>'Add New'
               ),'Add New Gift'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn container">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="freegifts" action="{{ route('addNewFreeGifts') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="free_gift_id" value="{{ $free_gift_id }}">


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Product name</label>
                                    <div class="input-group">

                                        <input class="form-control" id="free_gift_name" type="text"
                                               placeholder="Product name"
                                               name="free_gift_name"
                                               value="{{ !empty(old('free_gift_name')) ? old('free_gift_name') : ((($freegift) && ($freegift->free_gift_name)) ? $freegift->free_gift_name : '') }}">

                                    </div>
                                    @if ($errors->has('free_gift_name'))
                                        <span class="text-danger help-block">{{ $errors->first('free_gift_name') }}</span>
                                    @endif
                                </div>


                                <div class="form-group">

                                    <label for="hf-email" class="form-control-label">Product Image</label>
                                    <div class="input-group">

                                        <input class="form-control {{ (($freegift) && ($freegift->free_gift_cover_image)) ? '' : 'productcoverimage'}}"
                                               id="productcoverimage" type="file" name="free_gift_cover_image">


                                    </div>
                                    <i>Note: upload images size is 800px/800px</i>
                                    @if ($errors->has('free_gift_cover_image'))
                                        <span class="text-danger">{{ $errors->first('free_gift_cover_image') }}</span>
                                    @endif

                                    @if(($freegift) && ($freegift->free_gift_cover_image))
                                        <div class="coverimagediv{{ $freegift->free_gift_id }}">
                                            <img src="/uploads/products/thumbs/{{$freegift->free_gift_cover_image}}"/>
                                            <a href="#" id="{{$freegift->free_gift_id}}"
                                               class="delete_cover_image btn btn-danger btn-xs"
                                               onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                        class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                        </div>
                                    @endif


                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Product Price</label>
                                    <div class="input-group">

                                        <input class="form-control" id="free_gift_price" type="text"
                                               placeholder="Product Price"
                                               name="free_gift_price"
                                               value="{{ !empty(old('free_gift_price')) ? old('free_gift_price') : ((($freegift) && ($freegift->free_gift_price)) ? $freegift->free_gift_price : '') }}">

                                    </div>
                                    @if ($errors->has('free_gift_price'))
                                        <span class="text-danger help-block">{{ $errors->first('free_gift_price') }}</span>
                                    @endif
                                </div>

                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Description</label>
                                    <div class="input-group">
                                        <textarea name="free_gift_description" id="free_gift_description" rows="15"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{!! !empty(old('free_gift_description')) ? old('free_gift_description') : ((($freegift) && ($freegift->free_gift_description)) ? $freegift->free_gift_description : '') !!}</textarea>

                                    </div>
                                    @if ($errors->has('free_gift_description'))
                                        <span class="text-danger help-block">{{ $errors->first('free_gift_description') }}</span>
                                    @endif
                                </div>

                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Status</label>
                                    <div class="input-group">
                                        <select name="free_gift_status" id="free_gift_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('free_gift_status')) && old('free_gift_status')==$ks)  ? 'selected' : ((($freegift) && ($freegift->free_gift_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    @if ($errors->has('free_gift_status'))
                                        <span class="text-danger help-block">{{ $errors->first('free_gift_status') }}</span>
                                    @endif
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/freegifts/js/freegifts.js')}}"></script>

@endsection