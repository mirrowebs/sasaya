@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               ''=>'Update Profile'
               ),'Update Profile'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">


            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                        @if (Session::has('flash_message_error'))
                        <br/>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message_error' ) }}</strong>
                        </div>
                    @endif

                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="updateProfile" action="{{ route('profile') }}"
                                  accept-charset="UTF-8" class="form-horizontal">
                                {{ csrf_field() }}

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" type="text" placeholder="Name"
                                               name="name" id="name"
                                               value="{{ !empty(old('name')) ? old('name') : (((Auth::user()) && (Auth::user()->name)) ? Auth::user()->name : '') }}">
                                        @if ($errors->has('name'))
                                            <span class="text-danger help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Email</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" type="text" placeholder="Email"
                                               name="email" id="email"
                                               value="{{ !empty(old('email')) ? old('email') : (((Auth::user()) && (Auth::user()->email)) ? Auth::user()->email : '') }}" readonly>
                                        @if ($errors->has('email'))
                                            <span class="text-danger help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')
    <script src="{{url('backend/modules/users/js/updateProfile.js')}}"></script>
@endsection