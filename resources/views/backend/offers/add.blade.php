@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'offers'=>'Offers',
               ''=>'Add New'
               ),'Add New Offer'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn container">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="offers" action="{{ route('addNewOffers') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="o_id" value="{{ $o_id }}">


                                <div class=" form-group">
                                    <label for="hf-email" class=" form-control-label">Offer Title</label>
                                    <div class="input-group">

                                        <input class="form-control" id="o_title" type="text"
                                               placeholder="Offer Title"
                                               name="o_title"
                                               value="{{ !empty(old('o_title')) ? old('o_title') : ((($offer) && ($offer->o_title)) ? $offer->o_title : '') }}">
                                        @if ($errors->has('o_title'))
                                            <span class="text-danger help-block">{{ $errors->first('o_title') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Offer Title Link</label>

                                    <div class="input-group">

                                        <input class="form-control" id="o_title_link" type="text"
                                               placeholder="Offer title Link"
                                               name="o_title_link"
                                               value="{{ !empty(old('o_title_link')) ? old('o_title_link') : ((($offer) && ($offer->o_title_link)) ? $offer->o_title_link : '') }}">
                                        @if ($errors->has('o_title_link'))
                                            <span class="text-danger help-block">{{ $errors->first('o_title_link') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class=" form-group">

                                    <label for="hf-email" class="form-control-label">Offer Image</label>
                                    <div class="input-group">

                                        <input class="form-control {{ (($offer) && ($offer->o_image)) ? '' : 'offerimage'}}"
                                               id="offerimage" type="file" name="o_image">
                                        @if ($errors->has('o_image'))
                                            <span class="text-danger">{{ $errors->first('o_image') }}</span>
                                        @endif
                                    </div>


                                    @if(($offer) && ($offer->o_image))
                                        <div class="imagediv{{ $offer->o_id }}">
                                            <img src="/uploads/offers/thumbs/{{$offer->o_image}}"/>
                                            <a href="#" id="{{$offer->o_id}}"
                                               class="delete_offer_image btn btn-danger btn-xs"
                                               onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                        class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                        </div>
                                    @endif

                                </div>





                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Offer Discount</label>
                                    <div class="input-group">

                                        <input class="form-control" id="o_discount" type="text"
                                               placeholder="Offer Discount"
                                               name="o_discount"
                                               value="{{ !empty(old('o_discount')) ? old('o_discount') : ((($offer) && ($offer->o_discount)) ? $offer->o_discount : '') }}">
                                        @if ($errors->has('o_discount'))
                                            <span class="text-danger help-block">{{ $errors->first('o_discount') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <label for="hf-email" class=" form-control-label">
                                        Demo Youtube URL
                                    </label>
                                    <div class="input-group">

                                        <input class="form-control" id="o_video_link" type="text"
                                               placeholder="Offer Video Link"
                                               name="o_video_link"
                                               value="{{ !empty(old('o_video_link')) ? old('o_video_link') : ((($offer) && ($offer->o_video_link)) ? $offer->o_video_link : '') }}">
                                        @if ($errors->has('o_video_link'))
                                            <span class="text-danger help-block">{{ $errors->first('o_video_link') }}</span>
                                        @endif
                                    </div>
                                    <span>
                                        EX: https://www.youtube.com/embed/{ID}
                                    </span>
                                </div>


                                {{--<div class="row form-group">--}}
                                {{--<div class="col col-md-3">--}}
                                {{--<label for="hf-email" class=" form-control-label">Description</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-12 col-md-9">--}}
                                {{--<textarea name="banner_description" id="banner_description" rows="9"--}}
                                {{--placeholder="Description..."--}}
                                {{--class="form-control ckeditor">{{ !empty(old('banner_description')) ? old('banner_description') : ((($offer) && ($offer->banner_description)) ? $offer->banner_description : '') }}</textarea>--}}
                                {{--@if ($errors->has('banner_description'))--}}
                                {{--<span class="text-danger help-block">{{ $errors->first('banner_description') }}</span>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                {{--</div>--}}


                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Status</label>
                                    <div class="input-group">
                                        <select name="o_status" id="o_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('o_status')) && old('o_status')==$ks)  ? 'selected' : ((($offer) && ($offer->banner_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('o_status'))
                                            <span class="text-danger help-block">{{ $errors->first('o_status') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/offers/js/offers.js')}}"></script>

@endsection