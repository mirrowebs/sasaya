<!-- Header-->
<header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-7">

        </div>

        <div class="col-sm-5">

            <div class="user-area dropdown float-right">
                {!! Auth::user()->name !!}  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" src="/backend/images/admin.jpg" alt="User Avatar">
                </a>

                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="{{ route('profile') }}"><i class="fa fa- user"></i>My Profile</a>
                    <a class="nav-link" href="{{ route('changePassword') }}"><i class="fa fa- user"></i>Change Password</a>
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power -off"></i>Logout</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>

        </div>
    </div>

</header><!-- /header -->
<!-- Header-->