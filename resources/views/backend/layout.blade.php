<!doctype html>
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Lolokplease - Admin">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    @include('backend._partials.stylesheets')

    @yield('headerStyles')
</head>

<body>

@include('backend._partials.leftsidemenu')

<div id="right-panel" class="right-panel">

@include('backend._partials.header')

@yield('content')
<!-- add content here -->

</div>

<!-- end page container -->
@include('backend._partials.footer')

@include('backend._partials.scripts')


<script src="/niceEdit/nicEdit.js" type="text/javascript"></script>

<script>

    bkLib.onDomLoaded(function () {

        var textareas = $('.ckeditor');

        for (var i = 0; i < textareas.length; i++) {
            var myNicEditor = new nicEditor({fullPanel : true});
            myNicEditor.panelInstance(textareas[i]);

        }
    });




</script>

@yield('footerScripts')

</body>


