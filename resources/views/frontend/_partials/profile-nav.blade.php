<ul class="myaccountleftnav">
    <li class="@if ($active_menu == 'profile') ltnavactive @endif"><a href="{{ route('userprofile') }}"><i
                    class="fas fa-user"></i> Profile Information</a></li>
    <li class="@if ($active_menu == 'changePassword') ltnavactive @endif"><a href="{{ route('userChangePassword') }}"><i
                    class="fas fa-unlock-alt"></i> Change Password</a></li>
    <li class="@if ($active_menu == 'orders') ltnavactive @endif"><a href="{{ route('orders') }}"><i
                    class="fas fa-check-circle"></i> My Orders</a></li>
    <li class="@if ($active_menu == 'userAddressBook') ltnavactive @endif"><a href="{{ route('userAddressBook') }}">
            <i class="fas fa-map-marker-alt"></i> Address Book</a></li>
    <li class="@if ($active_menu == 'wishList') ltnavactive @endif"><a href="{{ route('wishList') }}"><i
                    class="fas fa-heart"></i> My Wish List</a></li>
    <li>

        <a href="{{ route('userlogout') }}"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-user"></i>
            Logout</a>
        <form id="logout-form" action="{{ route('userlogout') }}" method="POST"
              style="display: none;"> {{ csrf_field() }} </form>

    </li>
</ul>
