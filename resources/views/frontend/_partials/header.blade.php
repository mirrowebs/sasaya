<header>
    <div class="top-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 text-center text-md-left">
                    <i class="fa fa-clock-o" aria-hidden="true"></i> Mon - Sat 9.00 - 19.00.pm | <span><a href="#"><i
                                class="fa fa-envelope-o" aria-hidden="true"></i> info@gmail.com </a></span> | <span
                        class="m-block">+91 88888 88888</span>
                </div>
                <div class="col-md-4 text-center text-md-right social-links">
                    <a href="#">Blog</a>
                    <a href="#">Contact</a>
                    <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
            </div>


        </div>
    </div>

    <?php
    $getCategories = getCategories();
    ?>

    <nav class="navbar navbar-expand-lg navbar-light pt-1 pb-1">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('home') }}"><img src="/frontend/images/logo.png" alt="Sasaaya"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                    </li>

                    @if(count($getCategories)>0)
                        @foreach($getCategories as $catkey=>$category)



                            <?php
                            $subcategories = getSubcategoriesByCategory($catkey)
                            ?>


                            <li class="nav-item {{ (count($subcategories)>0) ? 'dropdown' : ''}}">
                                <a class="nav-link dropdown-toggle"
                                   href="{{ route('userproducts',['category'=>$catkey]) }}"
                                   id="navbardrop" data-toggle="{{ (count($subcategories)>0) ? 'dropdown' : ''}}">
                                    {{ $category }}
                                </a>
                                @if(count($subcategories)>0)
                                    <div class="dropdown-menu">
                                        @foreach($subcategories as $scatkey=>$scategory)
                                            <a class="dropdown-item"
                                               href="{{ route('userproducts',['category'=>$catkey,'subcategory'=>$scatkey]) }}">
                                                {{ $scategory }}
                                            </a>
                                        @endforeach
                                    </div>
                                @endif
                            </li>


                        @endforeach
                    @endif




                    <li class="nav-item">
                        <a class="nav-link icon-link icon-search" href="javascript:void(0)"><i class="fa fa-search"
                                                                                               aria-hidden="true"></i></a>
                    </li>
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a class="nav-link icon-link icon-cart" href="javascript:void(0)"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>--}}
                    {{--                    </li>--}}


                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('cartDetails') }}">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            <span></span> Cart (<span
                                id="cartcontents">{{ Cart::getContent()->count() }}</span>)</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link icon-link" href="{{ route('wishList') }}"><i class="fa fa-heart"
                                                                                        aria-hidden="true"></i></a>
                    </li>


                    {{--                    <li class="nav-item">--}}
                    {{--                        <a class="nav-link login-link" href="news.html"><i class="fa fa-lock" aria-hidden="true"></i>Log in or <br /> Register</a>--}}
                    {{--                    </li>--}}


                    @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                <i class="far fa-user"></i> Account <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('userprofile') }}">Your Account</a>
                                <a class="dropdown-item" href="{{ route('orders') }}">Your Orders</a>
                                <a href="{{ route('userlogout') }}" class="dropdown-item"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('userlogout') }}" method="POST"
                                      style="display: none;"> {{ csrf_field() }} </form>
                            </div>
                        </li>
                    @else
                        <li class="">

                            <a class="nav-link login-link" href="{{ route('userlogin') }}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                Login
                            </a>
                        </li>
                        <li class="">

                            <a class="nav-link login-link" href="{{ route('register') }}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                Register
                            </a>
                        </li>
                    @endauth


                </ul>

            </div>
        </div>
    </nav>
</header>



