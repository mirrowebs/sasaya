<!-- script files -->
<script src="/frontend/js/scripts.js"></script>

<script type="text/javascript">

    $(function () {

        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            })
        });

        $('#contactUs').validate({
            ignore: [],
            errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: 'Please enter name'
                },
                phone: {
                    required: 'Please enter phone',
                    number: 'Please enter a valid phone number'
                },
                email: {
                    required: 'Please enter email'
                }
            },
            submitHandler: function (form) {
                $('.contactusformStatus').html('');
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: "{{ route('contact') }}",
                    data: $(form).serialize(),
                    success: function (response) {
                        if (response.status == 1) {
                            $('#contactUs')[0].reset();
                            $('.contactusformStatus').html('<div class="alert alert-success">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                '</div>');
                        } else {
                            $('.contactusformStatus').html('<div class="alert alert-danger">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>' + response.errors + '</strong>\n' +
                                '</div>');
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        });


        $('#saveSubscribers').validate({
            ignore: [],
            errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {
                nl_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                nl_email: {
                    required: 'Please enter email'
                }
            },
            submitHandler: function (form) {
                $('.subscribersformStatus').html('');
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: "{{ route('saveSubscribers') }}",
                    data: $(form).serialize(),
                    success: function (response) {
                        if (response.status == 1) {
                            $('#saveSubscribers')[0].reset();
                            $('.subscribersformStatus').html('<div class="alert alert-success">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                '</div>');
                        } else {
                            $('.subscribersformStatus').html('<div class="alert alert-danger">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>' + response.errors + '</strong>\n' +
                                '</div>');
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        });


        $('.addToCartInProduct').on('click', function () {
            var id = $(this).data('id');
            var qty = $('.productqty').val();
            var selected = [];
            $("input:checkbox[class=addonproducts]:checked").each(function () {
                selected.push($(this).val());
            });
            var datastring = selected.join();

            if (id != '' && qty != 0) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('addToCart') }}',
                    type: 'POST',
                    data: 'id=' + id + '&qty=' + qty + '&linkedproducts=' + datastring,
                    success: function (response) {
                        $('#cartcontents').text(response);
                        $('.addonproducts').prop('checked', false);
                        notifications('Item added to the cart');
                    }
                });
            }
        });


        // Add to cart

        $('.addToCart').on('click', function () {
            var id = $(this).data('id');
            var datastring = '';
            var qty = 1;
            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('addToCart') }}',
                    type: 'POST',
                    data: 'id=' + id + '&qty=' + qty + '&linkedproducts=' + datastring,
                    success: function (response) {
                        $('#cartcontents').text(response);
                        notifications('Item added to the cart');
                    }
                });
            }
        });
        $('.addToWishList').on('click', function () {
            var id = $(this).data('id');
            var userid = $(this).data('userid');
            if (userid) {
                if (id != '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('addToWishList') }}',
                        type: 'POST',
                        // data: {'id': id, 'qty': 1},
                        data: 'id=' + id,
                        success: function (response) {
                            if (response == 1) {
                                $('.productaddwishlist' + id).addClass("likeactive");
                                notifications('Item added from wishlist');
                            } else if (response == 2) {
                                $('.productaddwishlist' + id).removeClass("likeactive");
                                notifications('Removed from wishlist');
                            } else {
                                alert("Try again");
                            }
                        }
                    });
                }
            } else {
                alert("Please login to add products to wishlist");
            }

        });
    });


    function notifications($title) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            title: $title,
            message: '',

        }, {
            type: "success",
            allow_dismiss: true,
            placement: {
                from: "top",
                align: "center"
            },
            delay: 500,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }


</script>
