<div class="order-summary">
    <h5 class="pb-3">Order Summary ({{ Cart::getContent()->count() }} Product(s))</h5>
    <table class="table-borderless">
        @foreach( Cart::getContent() as $item)
            <tr>
                <td colspan="2"><strong>{{ $item->name }}</strong></td>
            </tr>
            <tr class="border-bottom">
                <td>Qty: {{ $item->quantity }}</td>
                <td align="right">Rs:{{ $item->getPriceSum() }}</td>
            </tr>
        @endforeach
        <tr>
            <td>Total</td>
            <td align="right">Rs:{{ Cart::getSubTotal() }}</td>
        </tr>
        <tr class="border-bottom">
            <td>Delivery Charges</td>
            <td align="right">+ Rs: {{ paymentDeliveryCharges(Cart::getSubTotal()) }}</td>
        </tr>
        <tr class="border-bottom">
            <td>You pay</td>
            <td align="right"><strong class="finalprice">+ Rs:{{ Cart::getTotal() }}</strong></td>
        </tr>
    </table>
</div>