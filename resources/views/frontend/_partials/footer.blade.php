

<section class="free-shipping h-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-6 col-md-3">
                <div><img src="/frontend/images/free-shipping.jpg"/></div>
                <h4>Free Shipping</h4>
                <p>On orders over 100$</p>
            </div>
            <div class="col-6 col-md-3">
                <div><img src="/frontend/images/money-back.jpg"/></div>
                <h4>Money Back 100%</h4>
                <p>Within 30 Days after delivery</p>
            </div>
            <div class="col-6 col-md-3">
                <div><img src="/frontend/images/working-time.jpg"/></div>
                <h4>Working Time</h4>
                <p>Mon-Sun: 8.00-20.00</p>
            </div>
            <div class="col-6 col-md-3">
                <div><img src="/frontend/images/phone.jpg"/></div>
                <h4>1 (234) 567 89 01</h4>
                <p>Order by phone</p>
            </div>
        </div>
    </div>
</section>
<?php
$newsLists= newsLists();
?>
@if(count($newsLists)> 0)

    <section class="news-section-top">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-12 col-lg-12">
                    <div class="latest-news-events">
                        <div class="news-img-box">
                            <h2 class="news-heading">Latest News</h2>
                        </div>
                        <div class="news-list-main">
                            <div class="row">

                                @foreach($newsLists AS $blog)

                                    <?php

                                    $blogCategory = getBlogCategoryById($blog->blog_bc_id);
                                    ?>

                                    <div class="col-lg-4">

                                        <div class="news-main">
                                            <div class="profile-thumb">
                                                <a href="{{ route('blog',['blog_category'=>$blogCategory->bc_alias,'blog_alias'=>$blog->blog_alias]) }}">
                                                    <img src="/uploads/blogs/thumbs/{{ $blog->blog_img }}">
                                                </a>
                                            </div>
                                            <div class="profile-des">
                                                <a href="#">
                                                    <h3>{{ $blog->blog_name }}</h3>
                                                    <p>{{ ($blog->blog_desc ? str_limit($blog->blog_desc, 80) : '') }}</p>
                                                </a>
                                                <div class="row date-readmore no-gutters">
                                                    <div class="col-6">
                                                        <div class="date-time"><label>
                                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                {{ (isset($blog->created_at)) ? \Carbon\Carbon::parse($blog->created_at)->format('d-m-Y') : '-'  }}
                                                            </label></div>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <div class="text-right"><a href="{{ route('blog',['blog_category'=>$blogCategory->bc_alias,'blog_alias'=>$blog->blog_alias]) }}">Read More</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                @endforeach



                            </div>
                            <p class="mb-0"><a class="read-more" href="#" title="View All News " target="_blank">View
                                    All News <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a></p>

                        </div>
                    </div>


                </div>
                {{--                    <div class="col-md-6 col-lg-4">--}}
                {{--                        <div class="latest-news-events latest-events">--}}
                {{--                            <div class="news-img-box">--}}
                {{--                                <h2 class="news-heading">Latest Event</h2>--}}
                {{--                            </div>--}}
                {{--                            <div class="news-list-main">--}}


                {{--                                <div class="news-main">--}}
                {{--                                    <div class="profile-thumb">--}}
                {{--                                        <a href="#"><img src="/frontend/images/profile-3.jpg"></a>--}}
                {{--                                    </div>--}}
                {{--                                    <div class="profile-des">--}}
                {{--                                        <a href="#"><h3>Dilmah founder welcomes tea eAuction</h3>--}}
                {{--                                            <p>Says move was unbelievable and great step...</p>--}}
                {{--                                        </a>--}}
                {{--                                        <div class="row no-gutters date-readmore">--}}
                {{--                                            <div class="col-6">--}}
                {{--                                                <div class="date-time"><label><i class="fa fa-clock-o"--}}
                {{--                                                                                 aria-hidden="true"></i> April 08,--}}
                {{--                                                        2020</label></div>--}}
                {{--                                            </div>--}}
                {{--                                            <div class="col-6 text-right">--}}
                {{--                                                <div class="text-right"><a href="#">Read More</a></div>--}}
                {{--                                            </div>--}}
                {{--                                        </div>--}}
                {{--                                    </div>--}}
                {{--                                </div>--}}
                {{--                                <p class="mb-0"><a class="read-more" href="#" title="View All Events " target="_blank">View--}}
                {{--                                        All Events <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a></p>--}}


                {{--                            </div>--}}
                {{--                        </div>--}}


                {{--                    </div>--}}
            </div>
        </div>
    </section>

@endif

<section class="h-section subscribe-now">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6 form-group subscribe-label"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                Subscribe Now
            </div>
            <div class="col-sm-6 form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Your email address">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-warning">Subscribe</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 form-group">
                <h4>Company Info</h4>
                <ul class="footer-links">
                    <li><a href="#">My Account</a></li>
                    <li><a href="#">Checkout</a></li>
                    <li><a href="#">Shopping Cart</a></li>
                    <li><a href="#">Wishlist</a></li>
                    <li><a href="#">Custom link</a></li>
                    <li><a href="#">Find a Store</a></li>
                    <li><a href="#">Press & Talent</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 form-group">
                <h4>Company Info</h4>
                <ul class="footer-links">
                    <li><a href="{{ route('blog') }}">Blog</a></li>
                    <li><a href="#">Store Location</a></li>
                    <li><a href="#">My Account</a></li>
                    <li><a href="#">Orders Tracking</a></li>
                    <li><a href="#">Size Guide </a></li>
                    <li><a href="#">FAQs</a></li>
                    <li><a href="#">California Privacy Rights</a></li>
                    <li><a href="#">CA Transparency in Store</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 form-group">
                <h4>Information</h4>
                <ul class="footer-links">
                    <li><a href="#">About US</a></li>
                    <li><a href="{{ route('contact') }}">Conatct Us</a></li>
                    <li><a href="#">Careers </a></li>
                    <li><a href="#">Delivery Information</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Teams of Use</a></li>
                    <li><a href="#">Affiliate Program</a></li>
                    <li><a href="#">Business With Us</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 form-group">
                <h4>Get In Touch With Us</h4>
                <table width="100%" class="mb-3">
                    <tbody><tr>
                        <td><svg class="svg-inline--fa fa-map-marker-alt fa-w-12" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg=""><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path></svg><!-- <i class="fas fa-map-marker-alt"></i> --></td>
                        <td>
                            <p>H.No: 10-1124, Plot No: 72, Road No:32, Banjarahills, Hyderabad, Telangana, India -
                                500035</p>
                        </td>
                    </tr>
                    <tr>
                        <td><svg class="svg-inline--fa fa-phone-volume fa-w-12" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone-volume" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg=""><path fill="currentColor" d="M97.333 506.966c-129.874-129.874-129.681-340.252 0-469.933 5.698-5.698 14.527-6.632 21.263-2.422l64.817 40.513a17.187 17.187 0 0 1 6.849 20.958l-32.408 81.021a17.188 17.188 0 0 1-17.669 10.719l-55.81-5.58c-21.051 58.261-20.612 122.471 0 179.515l55.811-5.581a17.188 17.188 0 0 1 17.669 10.719l32.408 81.022a17.188 17.188 0 0 1-6.849 20.958l-64.817 40.513a17.19 17.19 0 0 1-21.264-2.422zM247.126 95.473c11.832 20.047 11.832 45.008 0 65.055-3.95 6.693-13.108 7.959-18.718 2.581l-5.975-5.726c-3.911-3.748-4.793-9.622-2.261-14.41a32.063 32.063 0 0 0 0-29.945c-2.533-4.788-1.65-10.662 2.261-14.41l5.975-5.726c5.61-5.378 14.768-4.112 18.718 2.581zm91.787-91.187c60.14 71.604 60.092 175.882 0 247.428-4.474 5.327-12.53 5.746-17.552.933l-5.798-5.557c-4.56-4.371-4.977-11.529-.93-16.379 49.687-59.538 49.646-145.933 0-205.422-4.047-4.85-3.631-12.008.93-16.379l5.798-5.557c5.022-4.813 13.078-4.394 17.552.933zm-45.972 44.941c36.05 46.322 36.108 111.149 0 157.546-4.39 5.641-12.697 6.251-17.856 1.304l-5.818-5.579c-4.4-4.219-4.998-11.095-1.285-15.931 26.536-34.564 26.534-82.572 0-117.134-3.713-4.836-3.115-11.711 1.285-15.931l5.818-5.579c5.159-4.947 13.466-4.337 17.856 1.304z"></path></svg><!-- <i class="fas fa-phone-volume"></i> --></td>
                        <td>
                            <p>+63 918 4084 694</p>
                        </td>
                    </tr>
                    <tr>
                        <td><svg class="svg-inline--fa fa-envelope fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg><!-- <i class="fas fa-envelope"></i> --></td>
                        <td>
                            <p>info@sayas.com</p>
                        </td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
    </div>
    <div class="text-center"><img src="/frontend/images/payment.jpg" /></div>
    <div class="text-center">All Rights Reserved by Sayas e-Commerce Online store 2020</div>
</footer>


{{--<footer>--}}
{{--    <!-- top footer -->--}}
{{--    <div class="topfooter">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-3">--}}
{{--                    <h3 class="fcoltitle text-uppercase">About Sasaya</h3>--}}
{{--                    <p class="pb-3">We possess within us two minds. So far I have written only of the scious mind.We--}}
{{--                        further know that the subconscious has recorded every event. This is just perfect theme.</p>--}}
{{--                    <h3 class="fcoltitle text-uppercase">News Letter</h3>--}}
{{--                    <!-- news letter form -->--}}
{{--                    <div class="subscribe">--}}
{{--                        <form class="newsform w-100 mt-2" action="{{ route('saveSubscribers') }}"--}}
{{--                              id="saveSubscribers">--}}
{{--                            <input type="text" placeholder="Enter Your Email" name="nl_email">--}}
{{--                            <input type="submit" value="Subscribe with us" class="btnsubscribe">--}}
{{--                            <div class="subscribersformStatus">--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                    <!--/ news letter form -->--}}
{{--                </div>--}}
{{--                <div class="col-lg-3">--}}
{{--                    <h3 class="fcoltitle text-uppercase">Company</h3>--}}
{{--                    <ul>--}}
{{--                        <li><a href="{{ route('aboutUs') }}">About us</a></li>--}}
{{--                        <li><a href="{{ route('blog') }}">Blog</a></li>--}}
{{--                        <li><a href="{{ route('faqs') }}">faq</a></li>--}}
{{--                        <li><a href="{{ route('contact') }}">Contact</a></li>--}}
{{--                        <li><a href="{{ route('termsAndConditions') }}">Terms & Conditions</a></li>--}}
{{--                        <li><a href="{{ route('privacyPolicy') }}">Privacy Policy</a></li>--}}
{{--                        <li><a href="{{ route('returnPolicy') }}">Return Policy </a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                @if(count(getCategories())>0)--}}
{{--                    <div class="col-lg-3">--}}
{{--                        <h3 class="fcoltitle text-uppercase">Categories</h3>--}}

{{--                        <ul>--}}
{{--                            @foreach(getCategories() as $catkey=>$category)--}}
{{--                                <li><a href="{{ route('userproducts',['category'=>$catkey]) }}">{{ $category }}</a></li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}

{{--                    </div>--}}
{{--                @endif--}}
{{--                <div class="col-lg-3">--}}
{{--                    <h3 class="fcoltitle text-uppercase">Get in Touch with us</h3>--}}
{{--                    <table width="100%" class="mb-3">--}}
{{--                        <tr>--}}
{{--                            <td><i class="fas fa-map-marker-alt"></i></td>--}}
{{--                            <td>--}}
{{--                                <p>H.No: 10-1124, Plot No: 72, Road No:32, Banjarahills, Hyderabad, Telangana, India ---}}
{{--                                    500035</p>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td><i class="fas fa-phone-volume"></i></td>--}}
{{--                            <td>--}}
{{--                                <p>+63 918 4084 694</p>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td><i class="fas fa-envelope"></i></td>--}}
{{--                            <td>--}}
{{--                                <p>info@sayas.com</p>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                    </table>--}}
{{--                    <h3 class="fcoltitle text-uppercase">Follow us on</h3>--}}
{{--                    <ul class="nav footersocial">--}}
{{--                        <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>--}}
{{--                        <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>--}}
{{--                        <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!--/ top footer -->--}}
{{--    <!-- bottom footer-->--}}
{{--    <div class="bottomfooter position-relative">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <p class="pt-2">All Rights Reserved by Sayas e-Commerce Online store 2018</p>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 text-right">--}}
{{--                    <img src="/frontend/images/paymenticon.png" alt="" title="">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <a id="movetop" class="" href="#"><i class="fas fa-arrow-up"></i></a>--}}
{{--    </div>--}}
{{--    <!--/ bottom footer -->--}}
{{--</footer>--}}

