<header class="">
    <!-- nav bar -->
    <div class="container blognav">
        <nav id="topNav" class="navbar-expand-lg  pt-5">

            <div class="blogbrand text-center">
                <a href="{{ route('home') }}"><img src="/frontend/images/logo.png" alt="sasaya"
                                                   title="Sasaya Online e Commerce Store"></a>
            </div>

            <div id="navbardropmain" class="navbar-collapse collapse justify-content-center">
                <ul class="navbar-nav">
                    <li class="nav-item @if (\Request::segment(2) == '') navactive @endif">
                        <a class="nav-link" href="{{ route('blog') }}"><i class="fas fa-home"></i> Home</a>
                    </li>

                    @if(count(getBlogCategories())>0)
                        @foreach(getBlogCategories() as $bcatkey=>$bcat)

                            <li class="nav-item @if (\Request::segment(2) == $bcatkey) navactive @endif">
                                <a class="nav-link"
                                   href="{{ route('blog',['blog_category'=>$bcatkey]) }}"> {{ $bcat }}</a>
                            </li>

                        @endforeach
                    @endif

                </ul>
            </div>
        </nav>
    </div>
    <!--/nav bar -->
</header>