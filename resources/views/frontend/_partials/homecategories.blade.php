<div id="parentHorizontalTab">
    <ul class="resp-tabs-list hor_1">

        @foreach(getCategoriesById() as $catkey=>$category)
            <li>{{ $category }}</li>
        @endforeach

    </ul>
    <div class="resp-tabs-container hor_1">
    @foreach(getCategoriesById() as $catkey=>$category)

        @php
            $products=getProductsByCategory($catkey);
        @endphp
        @if(count($products)>0)
            <!-- gift articles -->
                <div>
                    <div class="row">

                        @foreach($products as $pkey=>$product)



                                  <?php

                                $imagePath = '';
                                if (count($product->productImages) > 0 && isset($product->productImages[0])) {

                                    $getimagePath = productImageUrl('thumb');

                                    $imagePath = $getimagePath . $product->productImages[0]->pi_image_name;

                                } else {
                                    $imagePath = '';
                                }


                                ?>




                            <div class="col-lg-3 col-sm-6 col-md-4">
                                <div class="singleproduct">
                                    <figure class="text-center position-relative">
                                        <a href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}">


                                            <img class="img-fluid" src="{{ $imagePath }}" alt=""
                                                 title="">
                                        </a>
                                        {{--<span class="discount position-absolute">-18%</span>--}}
                                        <!-- hover -->
                                        <div class="prohover">

                                            <ul>
                                                {{--<li><a href="javascript:void(0)" data-toggle="tooltip"--}}
                                                {{--data-placement="bottom" title="Add to Cart"--}}
                                                {{--data-id="{{ $product->product_id }}" class="addToCart"><i--}}
                                                {{--class="fas fa-shopping-cart"></i></a></li>--}}
                                                <li>

                                                    <a class="btn btn-info" href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}">
                                                        View Product
                                                        <i class="fas fa-external-link-alt"></i>
                                                    </a>

                                                </li>

                                            </ul>
                                        </div>
                                        <!--/ hover-->
                                    </figure>
                                    <article class="text-center py-2">
                                        <a class="d-block text-center"
                                           href="{{ route('productDetails',['product_alias'=>$product->p_alias]) }}">{{ $product->p_name }}</a>
                                        <ul class="text-center">
                                            <li class="oldprice"><span><i
                                                            class="fas fa-rupee-sign"></i>{{ $product->p_mrp_price }}</span>
                                            </li>
                                            <li><i class="fas fa-rupee-sign"></i>{{ $product->p_sel_price }}</li>
                                        </ul>
                                    </article>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else

                <div>
                    <div class="row">

                        No Products Found

                    </div>
                </div>

            @endif
        <!--/ Gift Articles-->
        @endforeach

    </div>
</div>
<!--/tab-->