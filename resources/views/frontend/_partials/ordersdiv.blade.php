@php
    $address=unserialize($aorder->order_delivery_address);
@endphp
<div class="s-product border-bottom py-3">
    <h4 class="ordtitle fmed">{{ ($aorder->order_status=='Pending') ? 'Failed' : $aorder->order_status }} </h4>
    <ul class="orddetlist py-4">
        <li><span>Name:</span> <span
                    class="fgray pl-2">{{ $address['ua_name'] }}</span>
        </li>
        <li><span>Ref:</span> <span
                    class="fgray pl-2">{{ $aorder->order_reference_number }}</span>
        </li>
        <li><span>Order Date &amp; Time:</span> <span
                    class="fgray pl-2">{{ \Carbon\Carbon::parse($aorder->created_at)->format('d/m/Y H:i:s')}}</span>
        </li>
    </ul>
    <div class="row py-2 ordrow">

        @foreach($aorder->orderItems as $items)
            @if($items->oitem_is_freegift==1)
                <?php

                if (!empty($items->getFreeProduct->free_gift_cover_image)) {
                    $pname = '/uploads/products/thumbs/' . $items->getFreeProduct->free_gift_cover_image;
                } else {

                    $pname = '/frontend/images/data/plant01.png';
                }
                ?>


                <div class="col-lg-2 col-sm-2">
                    <figure class="timezone cartimg">
                        <a href="{{ route('orderDetails',['order'=>$aorder->order_reference_number]) }}">
                            <img class="img-fluid" src="{{ $pname }}">
                        </a>
                    </figure>
                </div>
                <div class="col-lg-6 col-sm-4 align-self-center">
                    <h5 class="fmed h6">{{ $items->getFreeProduct->free_gift_name }}</h5>
                    <p class="fgray">{!! str_limit($items->getFreeProduct->free_gift_description, 150)  !!}</p>

                </div>
                <div class="col-lg-2 col-sm-3 align-self-center">
                    <h5 class="fmed h6">{{ $items->oitem_qty }} X {{ $items->oitem_product_price }}</h5>
                </div>
                <div class="col-lg-2 col-sm-3 align-self-center">
                    <h5 class="fmed h6"><i
                                class="fas fa-rupee-sign"></i>{{ $items->oitem_sub_total }}</h5>
                </div>


            @else

                <?php

                if (!empty($items->getProduct->product_cover_image)) {
                    $pname = '/uploads/products/thumbs/' . $items->getProduct->product_cover_image;
                } else {
                    $productImages = getProductImages($items->getProduct->product_id);
                    if (count($productImages) > 0) {
                        $pname = '/uploads/products/thumbs/' . $productImages[0]->pi_image_name;
                    } else {
                        $pname = '/frontend/images/data/plant01.png';

                    }
                }
                ?>


                <div class="col-lg-2 col-sm-2">
                    <figure class="timezone cartimg">
                        <a href="{{ route('orderDetails',['order'=>$aorder->order_reference_number]) }}">
                            <img class="img-fluid" src="{{ $pname }}">
                        </a>
                    </figure>
                </div>
                <div class="col-lg-6 col-sm-4 align-self-center">
                    <h5 class="fmed h6">{{ $items->getProduct->product_name }}</h5>
                    <p class="fgray">{!! str_limit($items->getProduct->product_description, 150)  !!}</p>
                    <p class="fgrey">
                        <?php echo $items->oitem_product_spl ? '<b>Color:</b> ' . strtoupper($items->oitem_product_spl) : ''; ?>
                        <?php echo $items->oitem_product_size ? '<b>Color:</b> ' . strtoupper($items->oitem_product_size) : ''; ?>
                    </p>

                </div>
                <div class="col-lg-2 col-sm-3 align-self-center">
                    <h5 class="fmed h6">{{ $items->oitem_qty }} X {{ $items->oitem_product_price }}</h5>
                </div>
                <div class="col-lg-2 col-sm-3 align-self-center">
                    <h5 class="fmed h6"><i
                                class="fas fa-rupee-sign"></i>{{ $items->oitem_sub_total }}</h5>
                </div>

            @endif

        @endforeach

        <div class="col-lg-12" align="right">
            Delivery Charge : <i
                    class="fas fa-rupee-sign"></i> {{ $aorder->order_shipping_price }}
        </div>

    </div>

    <div class="row">
        <div class="col-lg-8 col-sm-12 align-self-center">
            <div class="paybtns pt-3">
                @if ($aorder->order_status=='Pending')
                    {{--<a--}}
                    {{--href="#"--}}
                    {{--class="cbtn btn text-uppercase fgray">PAYNOW</a>--}}
                @endif

                <a href="{{ route('orderDetails',['order'=>$aorder->order_reference_number]) }}"
                   class="cbtn btn text-uppercase fgray">Order Info
                    <a href="{{ route('orderInvoice',['order'=>$aorder->order_reference_number]) }}"
                       class="cbtn btn text-uppercase fgray ml-1" target="_blank">Invoice</a>
            </div>
        </div>
        <div class="col-lg-4 col-sm-12 align-self-right text-center">
            <h1 class="price h1"><i class="fas fa-rupee-sign" style="font-size:34px;"></i>
                {{ $aorder->order_total_price }}</h1>
        </div>
    </div>
</div>