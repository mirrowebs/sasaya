@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')
    <!--main -->


        @if(count($banners)>0)

            <section class="banner-sec">
                <div id="mainBanner" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">

                        @foreach($banners as $banner)
                            <li data-target="#demo" data-slide-to="{{ $loop->iteration-1 }}"
                                class="{{ $loop->iteration==1?'active':'' }}"></li>


                            <li data-target="#mainBanner" data-slide-to="0" class="active"></li>

                        @endforeach


                    </ol>
                    <div class="carousel-inner">


                        @foreach($banners as $banner)


                            <div
                                class="carousel-item {{ $loop->iteration==1?'active':'' }} slider0{{ $loop->iteration-1 }}">
                                <picture>
                                    <source media="(min-width: 1024px)" type="image/png"
                                            srcset="/uploads/banners/{{ $banner->banner_image }}">
                                    <source media="(min-width: 750px)" type="image/png"
                                            srcset="/uploads/banners/{{ $banner->banner_image }}">
                                    <source media="(max-width: 749px)" type="image/png"
                                            srcset="/uploads/banners/{{ $banner->banner_image }}">
                                    <img class="" src="/uploads/banners/{{ $banner->banner_image }}"
                                         title="{{ $banner->banner_title }}">
                                </picture>
                            </div>




                        @endforeach


                    </div>
                    <a class="carousel-control-prev" href="#mainBanner" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#mainBanner" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </section>



        @endif


        <?php
        //    dd($newsLists);
        ?>





        {{-- <section class="h-section our-collection ">
             <h2 class="text-center mb-4">Our Collection</h2>
             <div class="container">
                 <div class="row home-collections">
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/green-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Green tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/black-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Black tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/ctc-chai.jpg" class="img-fluid">
                             <h3 class="product-name">CTC Chai</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/iced-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Iced tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/wellness-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Wellness tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/green-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Green tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/black-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Black tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/ctc-chai.jpg" class="img-fluid">
                             <h3 class="product-name">CTC Chai</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/iced-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Iced tea</h3></a>
                     </div>
                     <div class="col-6 col-sm-4 col-lg-3 text-center">
                         <a href="#"><img src="/frontend/images/wellness-tea.jpg" class="img-fluid">
                             <h3 class="product-name">Wellness tea</h3></a>
                     </div>
                 </div>
             </div>
         </section>--}}

        @if(count($products_lists)> 0)
            <section class="h-section our-products">
                <h2 class="text-center mb-4">Products</h2>
                <div class="container">
                    <div class="row home-collections">

                        @foreach($products_lists AS $product)

                            <?php

                            $imagePath = '';
                            if (count($product->productImages) > 0 && isset($product->productImages[0])) {

                                $getimagePath = productImageUrl('thumb');

                                $imagePath = $getimagePath . $product->productImages[0]->pi_image_name;

                            } else {
                                $imagePath = '';
                            }


                            ?>

                            <div class="col-6 col-sm-4 col-lg-3 text-center form-group">
                                <a href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}">
                                    <img src="{{ $imagePath }}" class="img-fluid">
                                    <h3 class="product-name">
                                        {{ $product->p_name }}
                                    </h3>
                                    <p class="price">

                                        <span class="offer-price">Rs.{{ $product->p_sel_price }}</span>
                                        <span class="text-strike">Rs.{{ $product->p_mrp_price }}</span>


                                    </p>
                                </a>
                                <div>
                                    <a href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}"
                                       class="btn btn-success" type="button">Add to Cart</a>
                                </div>
                            </div>

                        @endforeach


                    </div>
                </div>
            </section>

        @endif


        <section class="h-section shop-by-categories">
            <h2 class="text-center mb-4">Shop By Categories</h2>
            <div class="container">
                <div class="row">
                    <div class="col-6 col-sm-3 form-group">
                        <a href="#"><img src="/frontend/images/cat-1.jpg" class="img-fluid"/></a>
                    </div>
                    <div class="col-6 col-sm-3 form-group">
                        <a href="#"><img src="/frontend/images/cat-2.jpg" class="img-fluid"/></a>
                    </div>
                    <div class="col-6 col-sm-3 form-group">
                        <a href="#"><img src="/frontend/images/cat-3.jpg" class="img-fluid"/></a>
                    </div>
                    <div class="col-6 col-sm-3 form-group">
                        <a href="#"><img src="/frontend/images/cat-4.jpg" class="img-fluid"/></a>
                    </div>
                </div>
            </div>
        </section>
        {{--    <section class="h-section testimonials">--}}
        {{--        <h2 class="text-center mb-4">Client Says</h2>--}}
        {{--        <div class="container text-center">--}}
        {{--            <div class="owl-carousel owl-theme">--}}
        {{--                <div class="item">--}}
        {{--                    <div class="client-img"><img src="/frontend/images/client-1.jpg"/></div>--}}
        {{--                    <h4 class="client-name">John Smith</h4>--}}
        {{--                    <p class="client-designation">UI Developer</p>--}}
        {{--                    <p class="client-description">Lorem Ipsum is simply dummy text of the printing and typesetting--}}
        {{--                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an--}}
        {{--                        unknown printer took a galley of type and scrambled it to make a type specimen book. It has--}}
        {{--                        survived not only five centuries</p></div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--    </section>--}}




@endsection
@section('footerScripts')
@endsection
