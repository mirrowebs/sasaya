<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
{{--    <title>@yield('title')</title>--}}
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/frontend/images/favicon-32x32.png">
    <!-- styles -->
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/frontend/css/style_old.css">
</head>


<body>

<header class="fixed-top">
    <!--news update -->
    <div class="newsupdates py-2 position-relative">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <a href="#" class="text-uppercase">Buy more then <span>1000 select</span> free gift</a>
                </div>
            </div>
        </div>
        <a href="#"><span class="closenews position-absolute"><i class="fas fa-times"></i></span></a>
    </div>
    <!--/ news update -->
    <!--top header -->
    <div class="topheader pt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link pl-0 pr-0">Welcome to Sayas Online Store </a></li>
                        <li class="nav-item hidexs"><a class="nav-link pl-0 pr-0"><i class="far fa-clock"></i> MON -
                                FRI: 10:00AM-18:00PM </a></li>
                        <li class="nav-item hidexs"><a class="nav-link pl-0 pr-0"><i class="fas fa-phone-volume"></i> +7
                                849 55 4267 </a></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <nav class="navbar navbar-expand-md pt-0">
                        <!-- Toggler/collapsibe Button -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#collapsibleNavbar">
                            <i class="fas fa-bars"></i>
                        </button>
                        <!-- Navbar links -->
                        <div class="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" target="_blank" href="{{ route('blog') }}">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                                </li>
                                @auth
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            <i class="far fa-user"></i> Account <i class="fas fa-caret-down"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ route('userprofile') }}">Your Account</a>
                                            <a class="dropdown-item" href="{{ route('orders') }}">Your Orders</a>
                                            <a href="{{ route('userlogout') }}" class="dropdown-item"
                                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                            <form id="logout-form" action="{{ route('userlogout') }}" method="POST"
                                                  style="display: none;"> {{ csrf_field() }} </form>
                                        </div>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('userlogin') }}">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">Register</a>
                                    </li>
                                @endauth
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('cartDetails') }}"><span></span> Cart (<span
                                            id="cartcontents">{{ Cart::getContent()->count() }}</span>)</a>
                                </li>
                                @auth
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('wishList') }}"><i class="far fa-heart"></i>
                                            Wish List</a>
                                    </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--/ top header -->
    <!-- nav bar -->
    <nav id="topNav" class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="{{ route('home') }}"><img src="/frontend/images/logo.png" alt="sasaya"
                                                                title="Sasaya Online e Commerce Store"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbardropmain">
            Menu
        </button>
        <div id="navbardropmain" class="navbar-collapse collapse">
            <ul class="navbar-nav">
                <li class="nav-item navactive">
                    <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home"></i> Home</a>
                </li>


                @if(count(getCategories())>0)
                    @foreach(getCategories() as $catkey=>$category)



                        <?php
                        $subcategories = getSubcategoriesByCategory($catkey)
                        ?>

                        <li class="nav-item {{ (count($subcategories)>0) ? 'dropdown' : ''}}">
                            <a class="nav-link dropdown-toggle" href="{{ route('userproducts',['category'=>$catkey]) }}"
                               id="navbardrop" data-toggle="{{ (count($subcategories)>0) ? 'dropdown' : ''}}">
                                {{ $category }}
                            </a>
                            @if(count($subcategories)>0)
                                <div class="dropdown-menu">
                                    @foreach($subcategories as $scatkey=>$scategory)
                                        <a class="dropdown-item"
                                           href="{{ route('userproducts',['category'=>$catkey,'subcategory'=>$scatkey]) }}">
                                            {{ $scategory }}
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </li>


                    @endforeach
                @endif

            </ul>
            <div class="ml-auto">
                <!-- search -->
                <div class="search float-left pt-1">
                    <input type="text" placeholder="Search here">
                    <input type="submit" value="Submit">
                </div>
                <!--/ search -->
                <ul class="navbar-nav headersocial">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fab fa-twitter"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--/nav bar -->
</header>
<div class="page-container {{ isset($active_menu) ? $active_menu : '' }}">
@yield('content')
    <footer>
        <!-- top footer -->
        <div class="topfooter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">About Sasaya</h3>
                        <p class="pb-3">We possess within us two minds. So far I have written only of the scious mind.We
                            further know that the subconscious has recorded every event. This is just perfect theme.</p>
                        <h3 class="fcoltitle text-uppercase">News Letter</h3>
                        <!-- news letter form -->
                        <div class="subscribe">
                            <form class="newsform w-100 mt-2" action="{{ route('saveSubscribers') }}"
                                  id="saveSubscribers">
                                <input type="text" placeholder="Enter Your Email" name="nl_email">
                                <input type="submit" value="Subscribe with us" class="btnsubscribe">
                                <div class="subscribersformStatus">
                                </div>
                            </form>
                        </div>
                        <!--/ news letter form -->
                    </div>
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">Company</h3>
                        <ul>
                            <li><a href="{{ route('aboutUs') }}">About us</a></li>
                            <li><a href="{{ route('blog') }}">Blog</a></li>
                            <li><a href="{{ route('faqs') }}">faq</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                            <li><a href="{{ route('termsAndConditions') }}">Terms & Conditions</a></li>
                            <li><a href="{{ route('privacyPolicy') }}">Privacy Policy</a></li>
                            <li><a href="{{ route('returnPolicy') }}">Return Policy </a></li>
                        </ul>
                    </div>
                    @if(count(getCategories())>0)
                        <div class="col-lg-3">
                            <h3 class="fcoltitle text-uppercase">Categories</h3>

                            <ul>
                                @foreach(getCategories() as $catkey=>$category)
                                    <li><a href="{{ route('userproducts',['category'=>$catkey]) }}">{{ $category }}</a></li>
                                @endforeach
                            </ul>

                        </div>
                    @endif
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">Get in Touch with us</h3>
                        <table width="100%" class="mb-3">
                            <tr>
                                <td><i class="fas fa-map-marker-alt"></i></td>
                                <td>
                                    <p>H.No: 10-1124, Plot No: 72, Road No:32, Banjarahills, Hyderabad, Telangana, India -
                                        500035</p>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-phone-volume"></i></td>
                                <td>
                                    <p>+63 918 4084 694</p>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-envelope"></i></td>
                                <td>
                                    <p>info@sasaaya.com</p>
                                </td>
                            </tr>
                        </table>
                        <h3 class="fcoltitle text-uppercase">Follow us on</h3>
                        <ul class="nav footersocial">
                            <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--/ top footer -->
        <!-- bottom footer-->
        <div class="bottomfooter position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <p class="pt-2">All Rights Reserved by sasaaya e-Commerce Online store {{ date('Y') }}</p>
                    </div>
                    <div class="col-lg-6 text-right">
                        <img src="/frontend/images/paymenticon.png" alt="" title="">
                    </div>
                </div>
            </div>
            <a id="movetop" class="" href="#"><i class="fas fa-arrow-up"></i></a>
        </div>
        <!--/ bottom footer -->
    </footer>

</div>




@include('frontend._partials.scripts')

@yield('footerScripts')

</body>

</html>

