@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
    <style>
        address.addbox {
            cursor: pointer;

        }

        .userAddressid {
            opacity: 0;
        }

        .selectedAddress {
            /*border: 2px solid #000 !important;*/

            border: solid 2px black !important;
            color: black;
            padding: 24px;
            position: relative;
        }

        .selectedAddress::before,
        .selectedAddress::after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 54px;
            height: 54px;
            background-image: url(http://www.clker.com/cliparts/p/e/X/9/d/4/check-mark-white-md.png),
            linear-gradient(135deg, green 50%, transparent 50%);
            background-size: 70% auto, cover;
            background-position: auto, left top;
            background-repeat: no-repeat;
        }
    </style>
@endsection

@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Checkout <span>{{ Cart::getContent()->count() }} (Products) </span></h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('cartDetails') }}">Cart</a></li>
                                <li class="nav-item"><a class="nav-link">Delivery Address</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row checkout">


                        <!-- checkout left -->
                        <div class="col-lg-9">
                            @if (Session::has('flash_message'))
                                <br/>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('flash_message' ) }}</strong>
                                </div>
                            @endif
                            <div class="resp-tabs-container hor_1">
                                <h5 class="h5 p-3 mb-4 checkoutheading">Delivery Address</h5>
                                <!-- Delivery address-->
                                <div class="row">
                                    <!-- address form -->
                                    <div class="col-lg-7">
                                        <form method="POST" action="{{ route('userAddAddressBook') }}"
                                              accept-charset="UTF-8" class="userAddress" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="ua_id">
                                            <input type="hidden" name="flag" value="1">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Pincode:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="ua_pincode">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Email:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="ua_email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Full Name</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="ua_name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Address</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <textarea class="form-control" name="ua_address"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Landmark</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control"
                                                               placeholder="Ex: Near Hockey Stadium" name="ua_landmark">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>City</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control"
                                                               placeholder="City name" name="ua_city">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>State</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control"
                                                               placeholder="State name" name="ua_state">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Country</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <select name="ua_country" id="ua_country" class="form-control">
                                                            <?php
                                                            $countries = getCountries(); ?>
                                                            @foreach($countries as $ks=>$s)
                                                                <option value="{{ $ks }}">
                                                                    {{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Mobile Number</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="ua_phone">
                                                    </div>
                                                </div>
                                            </div>

                                            {{--<div class="form-group">--}}
                                            {{--<div class="row">--}}
                                            {{--<div class="col-lg-4 text-right">--}}
                                            {{--<label>Address Type</label>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-lg-8">--}}
                                            {{--<select class="form-control w-100">--}}
                                            {{--<option>Select Address Type</option>--}}
                                            {{--<option>Home</option>--}}
                                            {{--<option>Office Pradesh</option>--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <label><input class="mr-10" type="checkbox" name="ua_defult"
                                                                      value="1"><span class="pl-1">Make this Default Address</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="submit" value="SAVE" class="btn text-uppercase">

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <form method="POST" action="{{ route('paymentConfirmation') }}"
                                              accept-charset="UTF-8"
                                              class="form-horizontal selectionOfAddress"
                                              enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <input type="hidden" name="address_id"
                                                   class="assignUserAddress">
                                            <input type="submit" class="btn text-uppercase"
                                                   value="Review Order"/>
                                        </form>
                                    </div>
                                    <!--/ address form -->
                                    <!-- saved address-->
                                    <div class="col-lg-5">
                                        <div class="savedaddress">
                                            <h4>Saved Address</h4>
                                            @if(count($userAddresses)>0)
                                                @foreach($userAddresses as $adds)
                                                    <address
                                                            class="addbox position-relative border rounded  align-items-center">
                                                        <input type="radio" name="userAddressid" class="userAddressid"
                                                               value="{{ $adds->ua_id }}"
                                                               @if($adds->ua_defult==1) checked="checked" @endif/>
                                                        <h5>{{ $adds->ua_name }}</h5>
                                                        <p>{{ $adds->ua_address }}, {{ $adds->ua_landmark }},
                                                            {{ $adds->ua_city }},{{ $adds->ua_state }}
                                                            ,{{ $adds->getCountry->country_name }}
                                                            - {{ $adds->ua_pincode }}</p>
                                                        <p>+91 {{ $adds->ua_phone }}</p>
                                                        @if($adds->ua_defult==1)
                                                            <span class="addresslabel home">Default</span>
                                                        @endif
                                                        <p class="editdel">
                                                            <span>
                                                                <a href="#" data-toggle="modal"
                                                                   data-target="#updateAddress{{ $adds->ua_id }}"><i
                                                                            class="far fa-edit"></i> Edit</a>

                                                            </span>
                                                            <span>


                                                                <form method="POST" id="useraddress"
                                                                      action="{{ route('userAddressBook') }}"
                                                                      accept-charset="UTF-8">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="flag" value="1"/>
                                                            <input type="hidden" name="ua_id"
                                                                   value="{{ $adds->ua_id }}"/>

                                                                    <button type="submit"
                                                                            title="Delete Address"
                                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i class="far fa-trash-alt"></i> Delete</button>
                                                                    </a>

                                                                </form>




                                                            </span>
                                                            <!-- The Modal -->
                                                        <div class="modal" id="updateAddress{{ $adds->ua_id }}">
                                                            <div class="modal-dialog">
                                                                <!-- address form -->
                                                                <form method="POST"
                                                                      action="{{ route('userAddAddressBook') }}"
                                                                      accept-charset="UTF-8"
                                                                      class="form-horizontal userAddress"
                                                                      enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="ua_id"
                                                                           value="{{ $adds->ua_id }}">
                                                                    <input type="hidden" name="flag" value="1">
                                                                    <div class="modal-content">
                                                                        <!-- Modal Header -->
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Update Delivery
                                                                                Address</h4>
                                                                            <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                                                        </div>
                                                                        <!-- Modal body -->
                                                                        <div class="modal-body">

                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Name"
                                                                                       class="form-control"
                                                                                       name="ua_name"
                                                                                       value="{{ !empty(old('ua_name')) ? old('ua_name') : ((($adds) && ($adds->ua_name)) ? $adds->ua_name : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Phone"
                                                                                       class="form-control"
                                                                                       name="ua_phone"
                                                                                       value="{{ !empty(old('ua_phone')) ? old('ua_phone') : ((($adds) && ($adds->ua_phone)) ? $adds->ua_phone : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Address"
                                                                                       class="form-control"
                                                                                       name="ua_address"
                                                                                       value="{{ !empty(old('ua_address')) ? old('ua_address') : ((($adds) && ($adds->ua_address)) ? $adds->ua_address : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text"
                                                                                       placeholder="Landmark"
                                                                                       class="form-control"
                                                                                       name="ua_landmark"
                                                                                       value="{{ !empty(old('ua_landmark')) ? old('ua_landmark') : ((($adds) && ($adds->ua_landmark)) ? $adds->ua_landmark : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="City"
                                                                                       class="form-control"
                                                                                       name="ua_city"
                                                                                       value="{{ !empty(old('ua_city')) ? old('ua_city') : ((($adds) && ($adds->ua_city)) ? $adds->ua_city : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="State"
                                                                                       class="form-control"
                                                                                       name="ua_state"
                                                                                       value="{{ !empty(old('ua_state')) ? old('ua_state') : ((($adds) && ($adds->ua_state)) ? $adds->ua_state : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <select name="ua_country"
                                                                                        id="ua_country"
                                                                                        class="form-control">
                                                                                    <option value="">Select Country
                                                                                    </option>
                                                                                    <?php
                                                                                    $countries = getCountries(); ?>
                                                                                    @foreach($countries as $ks=>$s)
                                                                                        <option value="{{ $ks }}" {{ (!empty(old('ua_country')) && old('ua_country')==$ks)  ? 'selected' : ((($adds) && ($adds->ua_country == $ks)) ? 'selected' : '') }}>
                                                                                            {{ $s }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Email"
                                                                                       class="form-control"
                                                                                       name="ua_email"
                                                                                       value="{{ !empty(old('ua_email')) ? old('ua_email') : ((($adds) && ($adds->ua_email)) ? $adds->ua_email : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Pincode"
                                                                                       class="form-control"
                                                                                       name="ua_pincode"
                                                                                       value="{{ !empty(old('ua_pincode')) ? old('ua_pincode') : ((($adds) && ($adds->ua_pincode)) ? $adds->ua_pincode : '') }}">
                                                                            </div>
                                                                        {{--<div class="form-group">--}}
                                                                        {{--<input type="checkbox" name="ua_defult"--}}
                                                                        {{--value="1" {{ ((($adds) && ($adds->ua_defult==1)) ? 'checked' : '') }}>--}}
                                                                        {{--<label>Default Address</label>--}}
                                                                        {{--</div>--}}

                                                                        <!--/ address form -->
                                                                        </div>
                                                                        <!-- Modal footer -->
                                                                        <div class="modal-footer justify-content-center pt-0 border-0">
                                                                            <button type="submit"
                                                                                    class="btn text-uppercase cbtn">
                                                                                Confirm
                                                                            </button>
                                                                            <button type="button"
                                                                                    class="btn text-uppercase cbtn"
                                                                                    data-dismiss="modal">Cancel
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!--/ modal for add address -->

                                                        </p>
                                                        @if($adds->ua_defult!=1)
                                                            <button type="button"
                                                                    class="btn setUserDefaultAddress"
                                                                    id="{{ $adds->ua_id }}"> Set as
                                                                Default
                                                            </button>
                                                        @endif
                                                    </address>
                                                @endforeach



                                            @else
                                                <address class="">
                                                    <h5>No records found</h5>
                                                </address>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/ saved address -->
                                </div>
                                <!--/ delivery address-->
                            </div>
                        </div>
                        <!--/ checkout left -->
                        <!-- Order summary -->
                        <div class="col-lg-3">
                            @include('frontend._partials.checkoutsummary')
                        </div>
                        <!--/ order summary -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>



    <!--/ main-->
    <form method="POST" action="{{ route('makeDefaultAddress') }}" accept-charset="UTF-8"
          class="form-horizontal" id="makeDefaultAddress" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="ua_id" class="makeDefaultAddress">
        <input type="hidden" name="flag" value="1">
        <input type="hidden" name="ua_defult" value="1">
    </form>
@endsection
@section('footerScripts')
    <script src="{{url('frontend/modules/users/js/users.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('.setUserDefaultAddress').on('click', function () {
                $('.makeDefaultAddress').val($(this).attr('id'));
                $('#makeDefaultAddress').submit();
            });
            $('.selectionOfAddress').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                // errorElement: 'div',
                // errorPlacement: function (error, e) {
                //     e.parents('.form-group').append(error);
                // },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    address_id: {
                        required: true
                    }
                },
                messages: {
                    address_id: {
                        required: 'Select address to process the order'
                    }
                },
            });
            $("input[type=\"radio\"]").parents('.addbox').removeClass("selectedAddress");
            $("input[type=\"radio\"]:checked").parents('.addbox').addClass('selectedAddress');
            $(".addbox input[type=radio]").click(function () {
                if ($(this).prop("checked")) {

                    $("input[type=\"radio\"]").parents('.addbox').removeClass("selectedAddress");
                    $("input[type=\"radio\"]:checked").parents('.addbox').addClass('selectedAddress');
                    // $("input[type=\"radio\"]").parents('.addbox').attr("style", "border:none");
                    // $("input[type=\"radio\"]:checked").parents('.addbox').attr("style", "border:2px solid #000 !important");
                }
            });
            getAddress();
            $(".userAddressid").click(function () {
                getAddress();
            });
        });

        function getAddress() {
            $('.assignUserAddress').val($('input[name=userAddressid]:checked').val());
        }
    </script>
@endsection