@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Product Wishlist</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">User Name will be
                                        here</a></li>
                                <li class="nav-item"><a class="nav-link">Wishlist</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            @include('frontend._partials.profile-nav')
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">My Wishlist</h5>
                                <!-- account right body -->
                                <div class="rightprofile ">
                                    <div class="ordertab">
                                        <div class="s-product  py-3">
                                            @if(count($wishlistProducts)>0)
                                                @foreach($wishlistProducts as $list)

                                                    <?php
                                                    if (!empty($list->getProduct->product_cover_image)) {
                                                        $pname = '/uploads/products/thumbs/' . $list->getProduct->product_cover_image;
                                                    } else {
                                                        $productImages = getProductImages($list->getProduct->product_id);
                                                        if (count($productImages) > 0) {
                                                            $pname = '/uploads/products/thumbs/' . $productImages[0]->pi_image_name;
                                                        } else {
                                                            $pname = '/frontend/images/data/plant01.png';

                                                        }
                                                    }
                                                    ?>

                                                    <div class="row py-4 ordrow border-bottom">
                                                        <div class="col-lg-2">
                                                            <figure class="cartimg">
                                                                <a href="{{ route('productDetails',['product_alias'=>$list->getProduct->product_alias]) }}"><img
                                                                            src="{{ $pname }}"> </a>
                                                            </figure>
                                                        </div>
                                                        <div class="col-lg-6 align-self-center">
                                                            <h5 class="fmed h6">{{ $list->getProduct->product_name }}</h5>
                                                            <p class="fgray">{!! str_limit($list->getProduct->product_description, 150)  !!}</p>
                                                            <div class="paybtns pt-3">
                                                                <form method="POST"
                                                                      action="{{ route('wishList') }}"
                                                                      accept-charset="UTF-8" class="form-horizontal"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="wishlist_id"
                                                                           value="{{ $list->wishlist_id }}"/>
                                                                    <button type="submit"
                                                                            class="cbtn btn text-uppercase fgray"
                                                                            title="Delete Product"
                                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                        Remove
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 align-self-right text-center">
                                                            <h1 class="price h1"><i
                                                                        class="fas fa-rupee-sign"></i> {{ $list->getProduct->product_price }}
                                                            </h1>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="row py-4 ordrow border-bottom">
                                                    No Products Found
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')
@endsection