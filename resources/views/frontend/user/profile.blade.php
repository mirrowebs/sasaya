@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Account Profile </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link">Account Information</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>

                            @include('frontend._partials.profile-nav')
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            @if (Session::has('flash_message'))
                                <br/>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('flash_message' ) }}</strong>
                                </div>
                            @endif
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">Profile Information</h5>
                                <form method="POST" id="profileInfo" action="{{ route('userprofile') }}"
                                      accept-charset="UTF-8" enctype="multipart/form-data">

                                {{ csrf_field() }}
                                    <!-- account right body -->
                                    <div class="rightprofile">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <figure class="propic">
                                                    @if(($user) && ($user->image))
                                                        <div class="imagediv{{ $user->id }}">

                                                            <img src="/uploads/users/thumbs/{{$user->image}}" alt=""
                                                                 title="">

                                                            <a href="#" id="{{$user->id}}"

                                                               class="delete_user_image btn btn-danger "
                                                               onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                                Delete
                                                            </a>

                                                        </div>

                                                    @else

                                                        <img src="/frontend/images/dummypic.jpg" alt="" title="">
                                                        <!-- file  input-->
                                                    @endif
                                                    <div class="input-file-container">
                                                        <table class="inputfile">
                                                            <tr>
                                                                <td>
                                                                    <input class="input-file" id="my-file" type="file"
                                                                           name="image">
                                                                    <label tabindex="0" for="my-file"
                                                                           class="input-file-trigger"><i
                                                                                class="fas fa-camera"></i> Upload Image</label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <p class="file-return"></p>

                                                    <!--/ file input -->
                                                </figure>
                                            </div>
                                            <div class="col-lg-8 rightprofile">

                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Name</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="Praveen Kumar" name="name"
                                                                   value="{{ !empty(old('name')) ? old('name') : ((($user) && ($user->name)) ? $user->name : '') }}"
                                                                   class="form-control">
                                                            @if ($errors->has('name'))
                                                                <span class="text-danger help-block">{{ $errors->first('name') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Your Gender</label>
                                                    </div>
                                                    <div class="col-lg-8 align-self-center">
                                                        <div class="form-group">
                                                            <label class="pr-3 pt-0">
                                                                <input type="radio" name="gender"
                                                                       value="Male" {{ (!empty(old('gender')) && old('gender')=="Male")  ? 'selected' : ((($user) && ($user->gender == "Male")) ? 'checked' : '') }}><span
                                                                        class="px-2">Male</span></label>
                                                            <label class="pr-3 pt-0">
                                                                <input type="radio" name="gender"
                                                                       value="Female" {{ (!empty(old('gender')) && old('gender')=="Female")  ? 'selected' : ((($user) && ($user->gender == "Female")) ? 'checked' : '') }}><span
                                                                        class="px-2">Female</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Date of Birth</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="dob"
                                                                   class="form-control datepicker" name="dob"
                                                                   value="{{ !empty(old('dob')) ? old('dob') : ((($user) && ($user->dob)) ? \Carbon\Carbon::parse($user->dob)->format('d-m-Y') : '') }}">
                                                            @if ($errors->has('dob'))
                                                                <span class="text-danger help-block">{{ $errors->first('dob') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Phone Number</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder=" " name="mobile"
                                                                   value="{{ !empty(old('mobile')) ? old('mobile') : ((($user) && ($user->mobile)) ? $user->mobile : '') }}"
                                                                   class="form-control">
                                                            @if ($errors->has('mobile'))
                                                                <span class="text-danger help-block">{{ $errors->first('mobile') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Email Address</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="santosh_n@yahoo.com" name="email" readonly
                                                                   value="{{ !empty(old('email')) ? old('email') : ((($user) && ($user->email)) ? $user->email : '') }}"
                                                                   class="form-control"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group mt-2">
                                                            <input type="submit" class="btn" value="Edit Profile">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->


@endsection
@section('footerScripts')
    <script src="{{url('frontend/modules/users/js/users.js')}}"></script>

@endsection