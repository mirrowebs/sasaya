@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">My Orders</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('orders') }}">My Orders</a></li>
                                <li class="nav-item"><a class="nav-link">Order Details</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            @include('frontend._partials.profile-nav')
                        </div>
                        <!--/ left account nav -->

                    @php
                        $address=unserialize($orderDetails->order_delivery_address);
                    @endphp


                    <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <table class="table no-border">
                                    <tr class="border-bottom">
                                        <td><h5 class="h5 fmed pb-3">Order
                                                Number {{ $orderDetails->order_reference_number }}</h5></td>
                                        <td align="right">

                                            <a href="{{ route('orderInvoice',['order'=>$orderDetails->order_reference_number]) }}"
                                               target="_blank">Invoice</a> &nbsp; |&nbsp;&nbsp;

                                            <a href="{{ route('orders') }}">Back to My Orders</a></td>
                                    </tr>
                                </table>
                                <!-- account right body -->
                                <div class="rightprofile ">
                                    <h4 class="ordtitle fgreen fmed py-3">{{ $orderDetails->order_status }}</h4>
                                    @foreach($orderDetails->orderItems as $items)

                                        @if($items->oitem_is_freegift==1)

                                            <?php

                                            if (!empty($items->getFreeProduct->free_gift_cover_image)) {
                                                $pname = '/uploads/products/thumbs/' . $items->getFreeProduct->free_gift_cover_image;
                                            } else {

                                                $pname = '/frontend/images/data/plant01.png';
                                            }
                                            ?>



                                            <div class="row py-2 ordrow ordertab">
                                                <div class="col-lg-2">
                                                    <figure class="cartimg">
                                                        <a href="javascript:void(0)"><img src="{{ $pname }}"> </a>
                                                    </figure>
                                                </div>
                                                <div class="col-lg-6 align-self-center">
                                                    <h5 class="fmed h6">{{ $items->getFreeProduct->free_gift_name }} </h5>
                                                    <p class="fgray">{!! str_limit($items->getFreeProduct->free_gift_description, 150)  !!}</p>
                                                </div>
                                                <div class="col-lg-4 align-self-right text-center">
                                                    <h6 class="price h6"><i
                                                                class="fas fa-rupee-sign"></i> {{ $items->oitem_product_price }}
                                                        X {{ $items->oitem_qty }}</h6>
                                                </div>
                                            </div>
                                        @else

                                            <?php
                                            $product = getProductInfobyid($items->oitem_product_id);
                                            $productImages = getProductImages($items->oitem_product_id);

                                            if (!empty($product->product_cover_image)) {
                                                $pname = '/uploads/products/thumbs/' . $product->product_cover_image;
                                            } else {
                                                if (count($productImages) > 0) {
                                                    $pname = '/uploads/products/thumbs/' . $productImages[0]->pi_image_name;
                                                } else {
                                                    $pname = '/frontend/images/data/plant01.png';

                                                }
                                            }
                                            ?>


                                            <div class="row py-2 ordrow ordertab">
                                                <div class="col-lg-2">
                                                    <figure class="cartimg">
                                                        <a href="javascript:void(0)"><img src="{{ $pname }}"> </a>
                                                    </figure>
                                                </div>
                                                <div class="col-lg-6 align-self-center">
                                                    <h5 class="fmed h6">{{ $items->getProduct->product_name }} </h5>
                                                    <p class="fgray">{!! str_limit($items->getProduct->product_description, 150) !!}</p>
                                                </div>
                                                <div class="col-lg-4 align-self-right text-center">
                                                    <h6 class="price h6"><i
                                                                class="fas fa-rupee-sign"></i> {{ $items->oitem_product_price }}
                                                        X {{ $items->oitem_qty }}</h6>
                                                </div>
                                            </div>

                                        @endif

                                    @endforeach
                                <!-- order status -->
                                    <div class="ord-status d-flex border-bottom dashedbrd">

                                    </div>
                                    <!--/ order status -->
                                    <!--delivery details -->
                                    {{--<div class="deliverydet py-2 border-bottom dashedbrd">--}}
                                    {{--<h4 class="ordtitle fmed py-3">Delivery Information</h4>--}}
                                    {{--<table width="100%" class="table-orderdetails">--}}
                                    {{--<tr>--}}
                                    {{--<td width="30%">Tracking number</td>--}}
                                    {{--<td><span class="fgray">69592503783</span></td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                    {{--<td width="30%">Courier company</td>--}}
                                    {{--<td><span class="fgray">Bluedart</span></td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                    {{--<td width="30%">Delivery Date</td>--}}
                                    {{--<td><span class="fgray">Delivered on Thu, Mar 8th '18--}}
                                    {{--</span></td>--}}
                                    {{--</tr>--}}
                                    {{--</table>--}}
                                    {{--</div>--}}
                                    <div class="deliverydet py-2 border-bottom dashedbrd">
                                        <h4 class="ordtitle fmed py-3">Delivery Address</h4>
                                        <table width="100%" class="table-orderdetails">
                                            <tr>
                                                <td width="30%">Name</td>
                                                <td><span class="fgray">{{ $address['ua_name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Address</td>
                                                <td><span class="fgray">{{ $address['ua_address'] }}
                                                        ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                                        ,{{ $address['ua_country'] }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Phone</td>
                                                <td><span class="fgray">{{ $address['ua_phone'] }}</span></td>
                                            </tr>
                                        </table>
                                    </div>
                                    {{--<div class="deliverydet py-2 border-bottom dashedbrd">--}}
                                    {{--<h4 class="ordtitle fmed py-3">Payment Mode and Delivery Time</h4>--}}
                                    {{--<table width="100%" class="table-orderdetails">--}}
                                    {{--<tr>--}}
                                    {{--<td width="30%">Payment mode</td>--}}
                                    {{--<td><span class="fgray">Credit Card</span></td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                    {{--<td width="30%">Delivery time</td>--}}
                                    {{--<td><span class="fgray">Packages are delivered between 09:00 -19:00 from Monday to Saturday. There are no deliveries on Sunday and on public holidays.</span>--}}
                                    {{--</td>--}}
                                    {{--</tr>--}}
                                    {{--</table>--}}
                                    {{--</div>--}}
                                    <div class="row py-3">
                                        <div class="col-lg-4 text-right ml-auto text-right">
                                            <table class="finalprice" width="100%;">
                                                <tr>
                                                    <td>Net Amount</td>
                                                    <td>:</td>
                                                    <td>
                                                        <span><i class="fas fa-rupee-sign"></i>{{ $orderDetails->order_total_price }}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Shipping</td>
                                                    <td>:</td>
                                                    <td><span><i class="fas fa-rupee-sign"></i>{{ $orderDetails->order_shipping_price }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Total amount</td>
                                                    <td>:</td>
                                                    <td>
                                                        <span><i class="fas fa-rupee-sign"></i>{{ $orderDetails->order_total_price }}</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!--/ delivelry details -->
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')

@endsection