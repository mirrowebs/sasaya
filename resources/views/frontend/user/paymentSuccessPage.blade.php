@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Order response Page</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link">Order response Page</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container payment">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="paymentcol px-4 py-3 border-rounded">

                                @if($latest_order->order_status=="Success")
                                    <article class="pb-4 mb-4 border-bottom text-center">
                                        <img src="/frontend/images/check.png" alt="">
                                        <h5 class="h5">Your Order has been successfully submitted !</h5>
                                        <p>Your order will be deliverd within 4-5 working days</p>
                                    </article>
                                @else
                                    <article class="pb-4 mb-4 border-bottom text-center">
                                        <img src="/frontend/images/check.png" alt="">
                                        <h5 class="h5">Your Order unable to complete the payment</h5>
                                        <p>Please complete the payment within 24 hours to avoid the order being
                                            cancelled automatically.</p>
                                    </article>

                                @endif
                                <article>
                                    <h5 class="h5">Order Details</h5>
                                    <table class="table tableresp">
                                        <thead>
                                        <tr>
                                            <th>Order Number</th>
                                            <th>Address</th>
                                            <th class="pricepayment">Total Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $latest_order->order_reference_number }}</td>
                                            <td>{{ $latest_order_address['ua_name'] . $latest_order_address['ua_phone'] }}
                                                ,<br/>
                                                {{ $latest_order_address['ua_address'] .$latest_order_address['ua_city'] . $latest_order_address['ua_state'] . $latest_order_address['ua_country'] }}
                                            </td>
                                            <td><h3 class="h3">Rs: {{ $latest_order->order_total_price }}</h3></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </article>
                            </div>

                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')

@endsection