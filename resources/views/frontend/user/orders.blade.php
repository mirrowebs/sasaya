@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">My Orders</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link">My Orders</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            @include('frontend._partials.profile-nav')
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">My Orders</h5>
                                <!-- account right body -->
                                <div class="rightprofile">
                                    <!--Horizontal Tab-->
                                    <div class="ordertab">
                                        <div id="parentHorizontalTab">
                                            <ul class="resp-tabs-list hor_1">
                                                <li>All <span>({{ count($all_orders) }}) </span></li>
                                                <li>Failed <span>({{ count($pending_orders) }}) </span></li>
                                                <li>Completed <span>({{ count($completed_orders) }})</span> </li>
                                                <li>Cancel <span>({{ count($completed_orders) }})</span></li>
                                            </ul>
                                            <div class="resp-tabs-container hor_1">


                                                <div>
                                                    @if(count($all_orders)>0)
                                                        @foreach($all_orders as $aorder)

                                                            @include('frontend._partials.ordersdiv')

                                                        @endforeach
                                                    @else
                                                        <div class="s-product border-bottom py-3">
                                                            No orders Found
                                                        </div>
                                                    @endif

                                                </div>
                                                <!--/ all products -->
                                                <!-- awaiting payments -->
                                                <div>
                                                    @if(count($pending_orders)>0)
                                                        @foreach($pending_orders as $aorder)

                                                            @include('frontend._partials.ordersdiv')

                                                        @endforeach
                                                    @else
                                                        <div class="s-product border-bottom py-3">
                                                            No orders Found
                                                        </div>
                                                    @endif
                                                </div>
                                                <!--/ awaiting payments -->
                                                <!-- shipping -->
                                                <div>
                                                    @if(count($completed_orders)>0)
                                                        @foreach($completed_orders as $aorder)

                                                            @include('frontend._partials.ordersdiv')

                                                        @endforeach
                                                    @else
                                                        <div class="s-product border-bottom py-3">
                                                            No orders Found
                                                        </div>
                                                    @endif
                                                </div>
                                                <!--/ shipping -->
                                                <!-- completed -->
                                                <div>
                                                    @if(count($cancel_orders)>0)
                                                        @foreach($cancel_orders as $aorder)

                                                            @include('frontend._partials.ordersdiv')

                                                        @endforeach
                                                    @else
                                                        <div class="s-product border-bottom py-3">
                                                            No orders Found
                                                        </div>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!--/Horizontal Tab-->
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')

@endsection