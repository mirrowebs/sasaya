@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Checkout <span>3 (Products) </span></h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('cartDetails') }}">Cart</a></li>
                                <li class="nav-item"><a class="nav-link">Review Order</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row checkout">
                        <!-- checkout left -->
                        <div class="col-lg-9">
                            <h5 class="h5 p-3 m-0 checkoutheading">Review Order</h5>
                            <div class="resp-tabs-container hor_1">
                                <!-- Review Order-->
                                <div class="reveworder">
                                    <table class="table tableresp">
                                        <thead>
                                        <tr>
                                            <th>Item Details</th>
                                            <th align="center">Qty</th>
                                            <th>Sub Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(Cart::getContent()->count()>0)
                                            @foreach( Cart::getContent() as $item)

                                                <?php
                                                $product = getProductInfobyid($item->id);
                                                $productImages = getProductImages($item->id);

                                                if (!empty($product->product_cover_image)) {
                                                    $pname = '/uploads/products/thumbs/' . $product->product_cover_image;
                                                } else {
                                                    if (count($productImages) > 0) {
                                                        $pname = '/uploads/products/thumbs/' . $productImages[0]->pi_image_name;
                                                    } else {
                                                        $pname = '/frontend/images/data/plant01.png';

                                                    }
                                                }
                                                ?>

                                                <tr>
                                                    <td>
                                                        <div class="cartfigsection">
                                                            <figure class="cartfig float-left">
                                                                <a href="javascript:void(0)"><img src="{{ $pname }}"
                                                                                                  class="img-fluid"
                                                                                                  alt=""
                                                                                                  title=""></a>
                                                            </figure>
                                                            <article>
                                                                <h4 class="pb-1">{{ $item->name }}</h4>
                                                                <p>
                                                                    <small>{!! ($item->attributes->has('description') ? str_limit($item->attributes->description, 150) : '') !!} </small>
                                                                </p>

                                                            </article>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        {{ $item->quantity }}
                                                    </td>

                                                    <td>
                                                        <table class="subtable">
                                                            <tr>

                                                                <td>Rs: {{ $item->getPriceSum() }}</td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td >
                                                    <p>Standard Delivery
                                                        By {{ \Carbon\Carbon::parse(date('d-m-Y'))->addDays('5')->format('d-m-Y') }}</p>
                                                </td>
                                                <td align="right">

                                                    Delivery Charge
                                                </td>
                                                <td>

                                                    Rs: {{ paymentDeliveryCharges(Cart::getSubTotal()) }}
                                                </td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td colspan="3">No Data Found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                        <tfoot class="cartfoot">

                                        <tr>
                                            <td >
                                                @if(count($freegifts)>0)
                                                    <a href="javascript:void(0)" data-toggle="modal"
                                                       data-target="#mygift">Slect
                                                        Free Gift</a>
                                                @endif
                                            </td>
                                            <td align="right">
                                                <a class="text-uppercase" href="{{ route('deliveryAddress') }}">Back to
                                                    Delivery Address</a>
                                            </td>
                                            <td>

                                                <a href="{{ route('saveOrders') }}">PAY NOW
                                                    Rs: {{ Cart::getTotal() }}</a>


                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!--/Review Order-->
                            </div>
                        </div>
                        <!--/ checkout left -->
                        <!-- Order summary -->
                        <div class="col-lg-3">
                            @include('frontend._partials.checkoutsummary')
                        </div>
                        <!--/ order summary -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->


    <!-- free gifts starts  -->
    <div id="mygift" class="modal fade modalcustom" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">Select Your Free Gifts</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- free gifts -->
                    <div class="free-gifts">

                        <table class="table tableresp">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Gift Image</th>
                                <th>Name</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($freegifts as $gift)

                                <?php
                                if (!empty($gift->free_gift_cover_image)) {
                                    $pname = '/uploads/products/thumbs/' . $gift->free_gift_cover_image;
                                } else {
                                        $pname = '/frontend/images/data/plant01.png';
                                }
                                ?>


                                <tr>
                                    <td><input type="radio" name="selgift" value="{{ $gift->free_gift_uniqid }}">
                                    </td>
                                    <td><img src="{{ $pname }}" style="width:75px;"></td>
                                    <td>{{ $gift->free_gift_name }}</td>
                                    <td>{!! str_limit($gift->free_gift_description, 150) !!}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default confirmGift" data-dismiss="modal">Confirm Gifts
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- free gifts ends -->

    <!-- onload gifts -->
    <div id="giftmodal" class="modal fade modalcustom" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">Free Gifts on Every Purchase</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <img src="/frontend/images/freegiftimg.jpg" class="img-fluid" alt="" title="">

                    <!-- table for gift worth-->
                    <table class="table tableresp">
                        <thead>
                        <tr>
                            <th>Shopping Value</th>
                            <th>Gift Worth</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <strong>Rs: 750 and Above </strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                            </td>
                            <td>Rs: 250</td>
                            <td><a href="javascript:void(0)">View Gifts</a></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Rs: 1200 and Above</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                            </td>
                            <td>Rs: 450</td>
                            <td><a href="javascript:void(0)">View Gifts</a></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Rs: 2200 and Above</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                            </td>
                            <td>Rs: 650</td>
                            <td><a href="javascript:void(0)">View Gifts</a></td>
                        </tr>
                        </tbody>
                    </table>
                    <!--/ table for gift worth -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Continue Shopping</button>
                </div>
            </div>
        </div>
    </div>
    <!--/ on load gifts -->

@endsection
@section('footerScripts')
    <script type="text/javascript">

        $(window).on('load', function () {
            $('#giftmodal').modal('show');
        });
        $('.confirmGift').click(function () {
            var id = $('input[name=selgift]:checked').val();
            ;
            var datastring = '';
            var qty = 1;
            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('addfreeGiftToCart') }}',
                    type: 'POST',
                    data: 'id=' + id + '&qty=' + qty + '&linkedproducts=' + datastring,
                    success: function (response) {
                        $('#cartcontents').text(response);
                        notifications('Item added to the cart');
                        location.reload();
                    }
                });
            }
        });
    </script>
@endsection
