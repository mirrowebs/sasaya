@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
    <style>
        table {
            border-collapse: collapse;
        }

        th, td {
            border: 1px solid orange;
            padding: 10px;
            text-align: left;
        }

        .rating {
            float: left;
        }

        /* :not(:checked) is a filter, so that browsers that don’t support :checked don’t
          follow these rules. Every browser that supports :checked also supports :not(), so
          it doesn’t make the test unnecessarily selective */
        .rating:not(:checked) > input {
            position: absolute;
            top: -9999px;
            clip: rect(0, 0, 0, 0);
        }

        .rating:not(:checked) > label {
            float: right;
            width: 1em;
            /* padding:0 .1em; */
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 300%;
            /* line-height:1.2; */
            color: #ddd;
        }

        .rating:not(:checked) > label:before {
            content: '★ ';
        }

        .rating > input:checked ~ label {
            color: dodgerblue;

        }

        .rating:not(:checked) > label:hover,
        .rating:not(:checked) > label:hover ~ label {
            color: dodgerblue;

        }

        .rating > input:checked + label:hover,
        .rating > input:checked + label:hover ~ label,
        .rating > input:checked ~ label:hover,
        .rating > input:checked ~ label:hover ~ label,
        .rating > label:hover ~ input:checked ~ label {
            color: dodgerblue;

        }

        .rating > label:active {
            position: relative;
            top: 2px;
            left: 2px;
        }

    </style>
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <article>
                                <h2 class="">{{ $product->product_name }}</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link"
                                                        href="{{ route('userproducts',['category'=>$product->getCategory->category_alias]) }}">{{ $product->getCategory->category_name }}</a>
                                </li>
                                <li class="nav-item"><a class="nav-link">{{ $product->p_name }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row">
                        <!-- product gallery -->
                        <div class="col-lg-6 col-sm-12">

                            <div>

                                <div class="bzoom_wrap">


                                    <ul id="bzoom" data-imagecount="{{count($productImages)}}">
                                        @if (count($productImages)>0)
                                            @foreach ($productImages as $image)


                                                <?php
                                                $getimagePath = productImageUrl();

                                                $imagePath = $getimagePath['thumb'].'/' . $image->pi_image_name;
                                                $imageThumbPath = $getimagePath['imgs'].'/' . $image->pi_image_name;

                                                //                                                dd($imagePath);

                                                ?>


                                                <li>
                                                    <img class="bzoom_thumb_image img-fluid"
                                                         src="{{ $imageThumbPath }}"
                                                         title="first img"/>
                                                    <img class="bzoom_big_image "
                                                         src="{{ $imagePath }}"/>

                                                </li>
                                            @endforeach
                                        @else

                                            <li>
                                                <img class="bzoom_thumb_image"
                                                     src="https://via.placeholder.com/500x500.png?text=Visit+WhoIsHostingThis.com 1"
                                                     title="first img"/>
                                                <img class="bzoom_big_image"
                                                     src="https://via.placeholder.com/500x500.png?text=Visit+WhoIsHostingThis.com 1"/>
                                            </li>

                                        @endif
                                    </ul>
                                </div>
                            </div>


                        </div>
                        <!--/ product gallery -->
                        <!-- product basic information -->
                        <div class="col-lg-6 col-sm-12">
                            <div class="rtproduct">
                                <h3 class="h3">{{ $product->p_name }}</h3>
                                <p class="price py-2 border-bottom">
                                    <span class="oldprice">Rs: {{ $product->p_mrp_price }}</span>
                                    <span class="ocolor">Rs: {{ $product->p_sel_price }}</span>

                                </p>
                                <table class="rttable">
                                    <tr>
                                        <td>Availability</td>
                                        <td>:</td>
                                        <td>{{ availability($product->p_availability) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Product Type</td>
                                        <td>:</td>
                                        <td>{{ $product->p_type }}</td>
                                    </tr>
                                    <tr>
                                        <td>Colour</td>
                                        <td>:</td>
                                        <td>{{ $product->p_color }}</td>
                                    </tr>
                                    <tr>
                                        <td>Category</td>
                                        <td>:</td>
                                        <td>{{ $product->getCategory->category_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Quntity</td>
                                        <td>:</td>
                                        <td>
                                            <select class="form-control itemQty1 productqty">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <a href="javascript:void(0)" data-toggle="tooltip"
                                               data-placement="bottom" title="Add to Wish list"
                                               data-id="{{ $product->p_id }}" data-userid="{{ Auth::id() }}"
                                               class="btn btn-dark productaddwishlist{{ $product->p_id }} addToWishList @if(in_array($product->p_id,$wishlistProducts))likeactive @endif">
                                                <i class="far fa-heart"></i> Add to Wishlist
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>

                                            <ul class="nav">

                                                <li class="nav-item">
                                                    <a href="#" data-toggle="tooltip"
                                                       data-id="{{ $product->p_id }}"
                                                       data-placement="bottom" title="Add to Cart"
                                                       class="addToCartInProduct"><i
                                                                class="fas fa-shopping-cart"></i> Add to Cart</a>
                                                </li>
                                                <li class="nav-item">

                                                </li>
                                                <li class="nav-item">

                                                    <form class="buyProductForm" action="{{ route('buyProduct') }}"
                                                          id="" method="GET">

                                                        <input type="hidden" name="id"
                                                               value="{{ $product->p_id }}"/>
                                                        <input type="hidden" name="qty" class="pqty"/>
                                                        <input type="hidden" name="size" class="psize"/>
                                                        <input type="hidden" name="color" class="pcolor"/>
                                                        <input type="hidden" name="linkedproducts" class="lproducts"/>
                                                        <a href="javascript:void(0)" class="buyproduct" data-toggle="tooltip"
                                                           data-placement="bottom" title="Buy Now"><i
                                                                    class="fas fa-rupee-sign"></i> Buy Now</a>

                                                    </form>


                                                </li>
                                            </ul>


                                        </td>
                                    </tr>
                                </table>

                                <br/>
                                <br/>
                                <br/>

                                <?php
                                if (!empty($product->p_linked_products)) {

                                    $linked_products = explode(',', $product->p_linked_products);
                                } else {
                                    $linked_products = array();
                                }
                                ?>

                                @if(count($linked_products)>0)
                                    <h3 class="h6">Add On Products</h3>
                                    <table class="table table-bordered" border="1">
                                        @foreach($linked_products as $productalias)

                                            <?php
                                            $productInfo = getProductInfo($productalias);

                                            $imagePath = '';
                                            if (count($product->productImages) > 0 && isset($product->productImages[0])) {

                                                $getimagePath = productImageUrl('thumb');

                                                $imagePath = $getimagePath . $product->productImages[0]->pi_image_name;

                                            } else {
                                                $imagePath = '';
                                            }


                                            ?>
                                            @if($productInfo)

                                                <tr>
                                                    <td align="left" valign="top">
                                                        <div class="input-group">
                                                            <input type="checkbox" name="add-product"
                                                                   class="addonproducts"
                                                                   value="{{ $productInfo->product_id }}"/>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <img width="50"
                                                             src="{{ $imagePath }}"/>
                                                    </td>
                                                    <td>
                                                        <h3 class="h6">{{ $productInfo->p_name }}</h3>
                                                        <p class="price py-2 border-bottom">
                                                            <span class="oldprice">Rs: {{ $productInfo->p_mrp_price }}</span>
                                                            <span class="ocolor">Rs: {{ $productInfo->p_sel_price }}</span>
                                                            {{--<span class="sm">32% off</span>--}}
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </table>
                            @endif

                            <!-- share this product -->
                                <p class="pt-3">Share this product</p>
                                <ul class="sharepro nav pt-2">
                                    <li class="nav-item"><a class="fb" href="javascript:void(0)" target="_blank"><i
                                                    class="fab fa-facebook-f"></i> Share <span>0</span></a></li>
                                    <li class="nav-item"><a class="tw" href="javascript:void(0)" target="_blank"><i
                                                    class="fab fa-twitter"></i> Tweet <span>0</span></a></li>
                                    <li class="nav-item"><a class="pin" href="javascript:void(0)" target="_blank"><i
                                                    class="fab fa-pinterest-p"></i> Pin it <span>0</span></a></li>
                                    <li class="nav-item"><a class="gplus" href="javascript:void(0)" target="_blank"><i
                                                    class="fab fa-google-plus-g"></i> <span>+1</span></a></li>
                                </ul>
                                <!--/ share this product -->

                            </div>
                        </div>
                        <!--/ product basic information -->
                    </div>

                    <!--row tab-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="productdetailtab pt-4">
                                <!--tab-->
                                <div id="parentHorizontalTab">
                                    <ul class="resp-tabs-list hor_1">
                                        <li>Overview</li>
                                        <li>Specifications</li>
                                        <li>Review</li>
                                    </ul>
                                    <div class="resp-tabs-container hor_1">
                                        <!-- Overview-->
                                        <div>
                                            <div class="row">


                                                <div class="text-justify p-2">{!! $product->p_primary_material !!}</div>

                                            </div>
                                        </div>
                                        <!--/ Overview-->
                                        <!-- Specifications-->
                                        <div>
                                            <div class="specsdetails">
                                                {!! $product->product_tech_details !!}
                                            </div>
                                        </div>
                                        <!--/Specifications-->
                                        <!-- Rating-->
                                        <div>
                                            <div class="specsdetails">
                                                <div class="saveReviewStatus">
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="h5 py-2">Customer Reviews</h5>
                                                        <h6 class="h6"><span
                                                                    class="fred">{{ $overall_rating->count() }} </span>Reviews
                                                        </h6>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3 ratecol align-self-center">
                                                        <h2 class="fbold fred">{{ $overall_rating->average('review_rating') }}</h2>
                                                        <p class="ratestars"><span><i
                                                                        class="fas fa-star fred"></i></span> <span><i
                                                                        class="fas fa-star fred"></i></span><span><i
                                                                        class="fas fa-star fred"></i></span><span><i
                                                                        class="fas fa-star fred"></i></span><span><i
                                                                        class="fas fa-star-half-alt fred"></i></span>
                                                        </p>
                                                        <p class="ratep">{{ $overall_rating->average('review_rating') }}
                                                            out of 5 stars</p>
                                                    </div>
                                                    <!-- rate percentage -->


                                                    <div class="col-lg-5">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="50px">
                                                                    <span>{{ getRatingsBasedOnGroup($ratings,5) }}</span>
                                                                </td>
                                                                <td>
                                                                    <div class="progress" style="height:10px">
                                                                        <div class="progress-bar"
                                                                             style="width:80%;height:10px">

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="80px">
                                                                    <span class="fmed starq">
                                                                        5 Star
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50px">
                                                                    <span>{{ getRatingsBasedOnGroup($ratings,4) }}</span>
                                                                </td>
                                                                <td>
                                                                    <div class="progress" style="height:10px">
                                                                        <div class="progress-bar"
                                                                             style="width:60%;height:10px"></div>
                                                                    </div>
                                                                </td>
                                                                <td width="80px"><span class="fmed starq">
                                                                        4 Star</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50px">
                                                                    <span>{{ getRatingsBasedOnGroup($ratings,3) }}</span>
                                                                </td>
                                                                <td>
                                                                    <div class="progress" style="height:10px">
                                                                        <div class="progress-bar"
                                                                             style="width:20%;height:10px"></div>
                                                                    </div>
                                                                </td>
                                                                <td width="80px"><span class="fmed starq">
                                                                        3 Star</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50px">
                                                                    <span>{{ getRatingsBasedOnGroup($ratings,2) }}</span>
                                                                </td>
                                                                <td>
                                                                    <div class="progress" style="height:10px">
                                                                        <div class="progress-bar"
                                                                             style="width:2%;height:10px"></div>
                                                                    </div>
                                                                </td>
                                                                <td width="80px"><span class="fmed starq">
                                                                        2 Star</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50px">
                                                                    <span>{{ getRatingsBasedOnGroup($ratings,1) }}</span>
                                                                </td>
                                                                <td>
                                                                    <div class="progress" style="height:10px">
                                                                        <div class="progress-bar"
                                                                             style="width:1%;height:10px"></div>
                                                                    </div>
                                                                </td>
                                                                <td width="80px"><span class="fmed starq">
                                                                        1 Star</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <!--/ rate percentage -->
                                                    <!-- image -->
                                                    <div class="col-lg-4 text-center align-self-center revdiv">
                                                        {{--<figure class="revimg mx-auto">--}}
                                                        {{--<img src="/frontend/images/data/acc01.png" alt="" title=""--}}
                                                        {{--class="img-fluid">--}}
                                                        {{--</figure>--}}

                                                        @auth

                                                        @else

                                                        @endauth

                                                        <button type="button" data-id="{{ Auth::id() }}"
                                                                class="btn btn-danger btn2 rateAndReview">
                                                            WRITE REVIEW
                                                        </button>

                                                        <!-- The Modal -->
                                                        <div class="modal" id="reviewModel">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">

                                                                    <!-- Modal Header -->
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title"> WRITE REVIEW</h4>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                    </div>

                                                                    <!-- Modal body -->
                                                                    <div class="modal-body">

                                                                        <form class="newsform w-100 mt-2"
                                                                              action="{{ route('savereview') }}"
                                                                              id="savereview">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="rating">
                                                                                        <input type="radio" id="star5"
                                                                                               name="review_rating"
                                                                                               value="5"/><label
                                                                                                for="star5" title="Meh">5
                                                                                            stars</label>
                                                                                        <input type="radio" id="star4"
                                                                                               name="review_rating"
                                                                                               value="4"/><label
                                                                                                for="star4"
                                                                                                title="Kinda bad">4
                                                                                            stars</label>
                                                                                        <input type="radio" id="star3"
                                                                                               name="review_rating"
                                                                                               value="3"/><label
                                                                                                for="star3"
                                                                                                title="Kinda bad">3
                                                                                            stars</label>
                                                                                        <input type="radio" id="star2"
                                                                                               name="review_rating"
                                                                                               value="2"/><label
                                                                                                for="star2"
                                                                                                title="Sucks big tim">2
                                                                                            stars</label>
                                                                                        <input type="radio" id="star1"
                                                                                               name="review_rating"
                                                                                               value="1"/><label
                                                                                                for="star1"
                                                                                                title="Sucks big time">1
                                                                                            star</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="input-group">
                                                                                <textarea class="form-control"
                                                                                          name="review_comment"></textarea>
                                                                            </div>
                                                                            <button type="submit"
                                                                                    class="btn btn-danger">Submit
                                                                            </button>
                                                                        </form>


                                                                    </div>

                                                                    {{--<!-- Modal footer -->--}}
                                                                    {{--<div class="modal-footer">--}}
                                                                    {{--<button type="button" class="btn btn-danger"--}}
                                                                    {{--data-dismiss="modal">Close--}}
                                                                    {{--</button>--}}
                                                                    {{--</div>--}}

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <!--/ image -->
                                                </div>

                                                <!-- help ful -->
                                                <div class="helpfulreview">
                                                    <h5 class="h5 py-1">Most Helpful</h5>

                                                    <!-- row review -->
                                                    <div class="row reviewrow border-bottom">
                                                        <div class="col-lg-1">
                                                            <figure>
                                                                <img src="/frontend/images/data/gift06.png">
                                                            </figure>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <h4 class="h4">Product Name will be here</h4>
                                                            <p class="pb-3">India Standard Charger (2A Fast Charging</p>

                                                            <h6 class="h6">Product is Very Nice and Good for
                                                                experience</h6>
                                                            <ul class="nav navhelpimg">
                                                                <li class="nav-item">
                                                                    <img src="/frontend/images/data/salts06.png">
                                                                </li>
                                                                <li class="nav-item">
                                                                    <img src="/frontend/images/data/salts06.png">
                                                                </li>
                                                                <li class="nav-item">
                                                                    <img src="/frontend/images/data/salts06.png">
                                                                </li>
                                                            </ul>
                                                            <!-- delivery to -->
                                                            <div class="deliverto pt-2">
                                                                <p class="fmed text-right"><i
                                                                            class="far fa-thumbs-up"></i> Likes(131)</p>
                                                                <div class="inputdelivery py-2">
                                                                    <input class="float-left" type="text"
                                                                           placeholder="Write down your reply"/>
                                                                    <input class="float-left" value="Reply"
                                                                           type="submit"/>
                                                                </div>
                                                            </div>
                                                            <!-- /delivery to -->

                                                        </div>
                                                        <div class="col-lg-2">
                                                            <p>February 3, 2018</p>
                                                        </div>
                                                    </div>
                                                    <!-- /row review -->

                                                    <!-- row review -->
                                                    <div class="row reviewrow border-bottom">
                                                        <div class="col-lg-1">
                                                            <figure>
                                                                <img src="/frontend/images/data/gift06.png">
                                                            </figure>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <h4 class="h4">Product Name will be here</h4>
                                                            <p class="pb-3">India Standard Charger (2A Fast Charging</p>

                                                            <h6 class="h6">Product is Very Nice and Good for
                                                                experience</h6>
                                                            <ul class="nav navhelpimg">
                                                                <li class="nav-item">
                                                                    <img src="/frontend/images/data/salts06.png">
                                                                </li>
                                                                <li class="nav-item">
                                                                    <img src="/frontend/images/data/salts06.png">
                                                                </li>
                                                                <li class="nav-item">
                                                                    <img src="/frontend/images/data/salts06.png">
                                                                </li>
                                                            </ul>
                                                            <!-- delivery to -->
                                                            <div class="deliverto pt-2">
                                                                <p class="fmed text-right"><i
                                                                            class="far fa-thumbs-up"></i> Likes(131)</p>
                                                                <div class="inputdelivery py-2">
                                                                    <input class="float-left" type="text"
                                                                           placeholder="Write down your reply"/>
                                                                    <input class="float-left" value="Reply"
                                                                           type="submit"/>
                                                                </div>
                                                            </div>
                                                            <!-- /delivery to -->

                                                        </div>
                                                        <div class="col-lg-2">
                                                            <p>February 3, 2018</p>
                                                        </div>
                                                    </div>
                                                    <!-- /row review -->

                                                </div>
                                                <!--/ help ful review -->
                                            </div>

                                        </div>
                                        <!--/Rating-->
                                    </div>
                                </div>
                                <!--/ tab-->
                            </div>
                        </div>
                    </div>
                    <!--/row tab-->
                @if(count($related_products)>0)
                    <!-- related products -->
                        <div class="relatedproducts">
                            <div class="row justify-content-center pb-4">
                                <div class="col-lg-6 text-center position-relative">
                                    <h5 class="h5">Related Products</h5>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($related_products as $relproducts)

                                    <?php

                                    $relatedImagePath = '';
                                    if (count($relproducts->productImages) > 0 && isset($relproducts->productImages[0])) {

                                        $getimagePath = productImageUrl('thumb');

                                        $relatedImagePath = $getimagePath . $relproducts->productImages[0]->pi_image_name;

                                    } else {
                                        $relatedImagePath = '';
                                    }


//                                    $pname = '/frontend/images/data/plant01.png';
                                    ?>


                                    <div class="col-lg-3 col-sm-6 col-md-4">
                                        <div class="singleproduct">
                                            <figure class="text-center position-relative">
                                                <a href="{{ route('productDetails',['p_alias'=>$relproducts->p_alias]) }}">
                                                    <img
                                                            class="img-fluid"
                                                            src="{{ $relatedImagePath }}"
                                                            alt=""
                                                            title="">
                                                </a>
                                            {{--<span class="discount position-absolute">-18%</span>--}}
                                            <!-- hover -->
                                                <div class="prohover">
                                                    <ul>
                                                        {{--<li><a href="javascript:void(0)" data-toggle="tooltip"--}}
                                                        {{--data-placement="bottom" title="Add to Cart"><i--}}
                                                        {{--class="fas fa-shopping-cart"></i></a></li>--}}
                                                        <li>

                                                            <a class="btn btn-info" href="{{ route('productDetails',['p_alias'=>$relproducts->p_alias]) }}">
                                                                View Product
                                                                <i class="fas fa-external-link-alt"></i>
                                                            </a>

                                                        </li>

                                                    </ul>
                                                </div>
                                                <!--/ hover-->
                                            </figure>
                                            <article class="text-center py-2">
                                                <a class="d-block text-center"
                                                   href="{{ route('productDetails',['p_alias'=>$relproducts->p_alias]) }}">{{ $relproducts->p_name }}</a>
                                                <ul class="text-center">
                                                    <li class="oldprice"><span><i
                                                                    class="fas fa-rupee-sign"></i>{{ $relproducts->p_mrp_price }}</span>
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-rupee-sign"></i>{{ $relproducts->p_sel_price }}
                                                    </li>
                                                </ul>
                                            </article>
                                        </div>
                                    </div>

                                @endforeach
                            </div>

                        </div>
                        <!--/ related products -->
                    @endif
                </div>

            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')
    <script>
        $(function () {

            $("#bzoom").zoom({
                zoom_area_width: 500,
                autoplay_interval: 3000,
                small_thumbs: 4,
                autoplay: false
            });


            $('#savereview').validate({
                ignore: [],
                errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                rules: {
                    review_rating: {
                        required: true
                    }
                },
                messages: {
                    review_rating: {
                        required: 'Please select rating'
                    }
                },
                submitHandler: function (form) {
                    $('.saveReviewStatus').html('');
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: "{{ route('savereview') }}",
                        data: $(form).serialize(),
                        success: function (response) {
                            if (response.status == 1) {
                                $('#reviewModel').modal('hide');
                                $('#savereview')[0].reset();
                                $('.saveReviewStatus').html('<div class="alert alert-success">\n' +
                                    '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                    '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                    '</div>');
                            } else {
                                $('.saveReviewStatus').html('<div class="alert alert-danger">\n' +
                                    '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                    '    <strong>' + response.errors + '</strong>\n' +
                                    '</div>');
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });


            $('.rateAndReview').click(function () {
                var id = $(this).data('id');
                if (id) {
                    $('#reviewModel').modal('show');
                } else {
                    alert('Please login to write a review');
                }

            });
            $('.buyproduct').click(function () {

                if ($('.productqty').val()) {
                    var qty = $('.productqty').val();
                } else {
                    var qty = 1;
                }
                var selected = [];
                $("input:checkbox[class=addonproducts]:checked").each(function () {
                    selected.push($(this).val());
                });
                var datastring = selected.join();

                $('.lproducts').val(datastring);
                $('.pqty').val(qty);
                $('.buyProductForm').submit();
            });

        });
    </script>
@endsection
