<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <title>Sasaya Online E-Commerce store</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/frontend/css/style_old.css">
</head>

<body>
<!--main login and register -->
<main>
    <section class="sign">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 leftsign"></div>
                <div class="col-lg-6 align-self-center">
                    <div class="signin mx-auto">
                        <figure class="text-center signlogo"><a href="{{ route('home') }}"><img
                                        src="/frontend/images/logo.png" alt="" title=""></a></figure>
                        <article class="text-center py-2">
                            <h3>Sign in</h3>
                            <h4 class="pt-3 pb-2">Enter your details below.</h4>
                        </article>
                        <form method="POST" action="{{ route('userlogin') }}" aria-label="{{ __('Login') }}"
                              class="formsign pt-4"
                              id="login">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Enter User Name</label>
                                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" type="text"
                                       placeholder="Enter Your User name" name="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback"
                                          role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Enter Password</label>
                                <input class="form-control" type="password" placeholder="Enter Password"
                                       name="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback"
                                          role="alert"> <strong>{{ $errors->first('password') }}</strong>   </span>
                                @endif
                            </div>
                            <div class="form-group text-right">
                                <a href="{{ route('password.reset') }}">Forgot Password?</a>
                            </div>

                            <div class="text-center"><input class="btn" type="submit" value="LOGIN"></div>
                        </form>
                        <p class="text-center pt-4">Don't have an account?
                            <span><a href="{{ route('register') }}">Signup</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--/ main login and register -->
<!-- footer scripts -->
<script src="/frontend/js/scripts.js"></script>
<script src="/frontend/modules/users/js/login.js"></script>
<!--/ footer scripts -->
</body>
</html>
