<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
{{--    <title>@yield('title')</title>--}}
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('frontend._partials.stylesheets')
</head>


<body>

@include('frontend._partials.header')

@yield('content')


@include('frontend._partials.footer')

@include('frontend._partials.scripts')

@yield('footerScripts')

</body>

</html>

