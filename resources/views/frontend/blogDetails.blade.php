@extends('frontend.bloglayout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage pt-0">
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row blogdetail">
                        <!-- blog left -->
                        <div class="col-lg-8">
                            <figure><img src="/uploads/blogs/{{ $blog->blog_img }}" alt="" title=""
                                         class="img-fluid w-100"></figure>
                            <article class="">
                                <h4 class="py-3">{{ $blog->blog_name }}</h4>
                                <p class="text-justify pb-2">
                                    {!! $blog->blog_desc !!}
                                </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted
                                        on {{ ($blog->created_at) ? \Carbon\Carbon::parse($blog->created_at)->format('d-m-Y') : '-'  }}</li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </article>
                        </div>
                        <!--/ blog left -->

                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->

    </main>
    <!--/ main-->

@endsection
@section('footerScripts')
@endsection
