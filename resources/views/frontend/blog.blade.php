@extends('frontend.bloglayout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage pt-0">
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">


                    <div class="row">
                        @if(count($blogs)>0)
                            @foreach($blogs as $blog)

                                <?php

                                $blogCategory = getBlogCategoryById($blog->blog_bc_id);
                                ?>

                                <div class="col-lg-4">
                                    <div class="blogcol  border-bottom pb-3">
                                        <figure class="pt-2">

                                            <a href="{{ route('blog',['blog_category'=>$blogCategory->bc_alias,'blog_alias'=>$blog->blog_alias]) }}">
                                                <img src="/uploads/blogs/thumbs/{{ $blog->blog_img }}" alt="" title=""
                                                     class="img-fluid">
                                            </a>
                                        </figure>
                                        <h4 class="pb-2">
                                            <a href="{{ route('blog',['blog_category'=>$blogCategory->bc_alias,'blog_alias'=>$blog->blog_alias]) }}">{{ $blog->blog_name }}</a>
                                        </h4>
                                        <p class="text-justify">{{ ($blog->blog_desc ? str_limit($blog->blog_desc, 150) : '') }} </p>
                                        <ul class="bloginfo nav">
                                            <li class="nav-item">Posted
                                                on {{ ($blog->created_at) ? \Carbon\Carbon::parse($blog->created_at)->format('d-m-Y') : '-'  }}</li>
                                            <li class="nav-item">Posted by Admin</li>
                                        </ul>
                                    </div>
                                </div>

                            @endforeach
                        @else
                            <div class="col-lg-4">
                                No blogs found
                            </div>

                        @endif

                    </div>

                    @if(count($blogs)>0)
                        <div class="row">
                            <div class="col-md-10">
                                <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                     aria-live="polite">Showing {{ $blogs->firstItem() }}
                                    to {{ $blogs->lastItem() }} of {{ $blogs->total() }} entries
                                </div>
                            </div>
                            <div class="col-md-1 text-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                    {!! $blogs->appends(['filters' => Request::get('filters')])->render() !!}
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->

    </main>
    <!--/ main-->

@endsection
@section('footerScripts')
@endsection
