@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                @if($product_sub_category)
                                    <h2 class="">{{ $subcategoryinfo->category_name }} <span>({{ count($products) }}
                                            Items)</span></h2>
                                @else
                                    <h2 class="">{{ $categoryInfo->category_name }} <span>({{ count($products) }}
                                            Items)</span></h2>
                                @endif
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}p">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Category</a></li>
                                <li class="nav-item"><a class="nav-link">{{ $categoryInfo->category_name }}</a></li>
                                @if($product_sub_category)
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Sub Category</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link">{{ $subcategoryinfo->category_name }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->

            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row filterrow pb-4">
                        <div class="col-lg-8">
                            {{--<span><i class="fas fa-search"></i></span><input type="text"--}}
                            {{--placeholder="Search with keyword">--}}
                        </div>
                        <div class="col-lg-4 ">

                            <form class="form-horizontal flitersForm" method="post" action="{{ Request::url() }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <select class="form-control fliters" name="fliters">
                                        <option value="popular">Popular</option>
                                        <option value="price_low_to_high" {{ ($search == 'price_low_to_high' ? "selected":"") }}>
                                            Price low to high
                                        </option>
                                        <option value="price_high_to_low" {{ ($search == 'price_high_to_low' ? "selected":"") }}>
                                            Price High to Low
                                        </option>
                                        <option value="fresh_arrivals" {{ ($search == 'fresh_arrivals' ? "selected":"") }}>
                                            Fresh Arrivals
                                        </option>
                                        <option value="offer_products" {{ ($search == 'offer_products' ? "selected":"") }}>
                                            Offers Products
                                        </option>
                                    </select>
                                </div>
                            </form>

                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row products -->
                    <div class="row">

                        @if(count($products)>0)
                            @foreach($products as $pkey=>$product)

                                <?php

                                $imagePath = '';
                                if (count($product->productImages) > 0 && isset($product->productImages[0])) {

                                    $getimagePath = productImageUrl('thumb');

                                    $imagePath = $getimagePath . $product->productImages[0]->pi_image_name;

                                } else {
                                    $imagePath = '';
                                }


                                ?>

                                <div class="col-lg-3 col-sm-6 col-md-4">
                                    <div class="singleproduct">
                                        <figure class="text-center position-relative">
                                            <a href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}">

                                                @if($imagePath!='')
                                                    <img width="150" src="{{$imagePath}}" class="img-fluid"/>
                                                @endif

                                            </a>


                                            <div class="prohover">



                                                <ul>
                                                    {{--<li><a href="javascript:void(0)" data-toggle="tooltip"--}}
                                                    {{--data-placement="bottom" title="Add to Cart"--}}
                                                    {{--data-id="{{ $product->product_id }}" class="addToCart"><i--}}
                                                    {{--class="fas fa-shopping-cart"></i></a></li>--}}
                                                    <li>

                                                        <a class="btn btn-info" href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}">
                                                            View Product
                                                            <i class="fas fa-external-link-alt"></i>
                                                        </a>

                                                    </li>

                                                </ul>
                                            </div>


                                        </figure>
                                        <article class="text-center py-2">
                                            <a class="d-block text-center"
                                               href="{{ route('productDetails',['p_alias'=>$product->p_alias]) }}">
                                                {{ $product->p_name }}
                                            </a>
                                            <ul class="text-center">
                                                <li class="oldprice">
                                                    <span>
                                                        <i class="fas fa-rupee-sign"></i>
                                                        {{ $product->p_mrp_price }}
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fas fa-rupee-sign"></i>{{ $product->p_sel_price }}
                                                </li>
                                            </ul>
                                        </article>
                                    </div>
                                </div>

                            @endforeach
                        @else

                            <div class="no-records">
                                No Products Found
                            </div>

                        @endif

                    </div>
                    <!--/ row products -->
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->

    </main>
    <!--/ main-->


@endsection
@section('footerScripts')
    <script type="text/javascript">
        $(function () {
            $(".fliters").change(function () {
                $(".flitersForm").submit();
            });
        });
    </script>
@endsection