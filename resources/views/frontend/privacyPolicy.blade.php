@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Privacy Policy </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->

            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row contentpage">
                        <div class="col-lg-12">
                            <h4 class="pb-2">Sasaya has provided this Privacy Policy to familiarise You with:</h4>
                            <p class="text-justify pb-3">
                                By providing us your Information or by making use of the facilities provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by Snapdeal as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.
                            </p>
                            <p class="text-justify pb-3">
                                Jasper Infotech Private Limited and its subsidiaries and affiliates and Associate Companies (individually and/ or collectively, "Snapdeal") is/are concerned about the privacy of the data and information of users (including sellers and buyers/customers whether registered or non-registered) accessing, offering, selling or purchasing products or services on Snapdeal's websites, mobile sites or mobile applications ("Website") on the Website and otherwise doing business with Snapdeal. "Associate Companies" here shall have the same meaning as ascribed in Companies Act, 2013.
                                The terms "We" / "Us" / "Our" individually and collectively refer to each entity being part of the definition of Snapdeal and the terms "You" /"Your" / "Yourself" refer to the users. This Privacy Policy is a contract between You and the respective Snapdeal entity whose Website You use or access or You otherwise deal with. This Privacy Policy shall be read together with the respective Terms Of Use or other terms and condition of the respective Snapdeal entity and its respective Website or nature of business of the Website.
                            </p>
                            <h5>
                                <span>1</span>Sharing and disclosure of Your Information
                            </h5>
                            <p class="text-justify pb-3">
                                You hereby unconditionally agree and permit that Snapdeal may transfer, share, disclose or part with all or any of Your Information, within and outside of the Republic of India to various Snapdeal entities and to third party service providers / partners / banks and financial institutions for one or more of the Purposes or as may be required by applicable law. In such case we will contractually oblige the receiving parties of the Information to ensure the same level of data protection that is adhered to by Snapdeal under applicable law.
                            </p>
                            <p class="text-justify pb-3">
                                You acknowledge and agree that, to the extent permissible under applicable laws, it is adequate that when Snapdeal transfers Your Information to any other entity within or outside Your country of residence, Snapdeal will place contractual obligations on the transferee which will oblige the transferee to adhere to the provisions of this Privacy Policy.
                            </p>
                            <p class="text-justify pb-3">
                                Snapdeal may share statistical data and other details (other than Your Personal Information) without your express or implied consent to facilitates various programmes or initiatives launched by Snapdeal, its affiliates, agents, third party service providers, partners or banks & financial institutions, from time to time. We may transfer/disclose/share Information (other than Your Personal Information) to those parties who support our business, such as providing technical infrastructure services, analyzing how our services are used, measuring the effectiveness of advertisements, providing customer / buyer services, facilitating payments, or conducting academic research and surveys. These affiliates and third party service providers shall adhere to confidentiality obligations consistent with this Privacy Policy. Notwithstanding the above, We use other third parties such as a credit/debit card processing company, payment gateway, pre-paid cards etc. to enable You to make payments for buying products or availing services on Snapdeal. When You sign up for these services, You may have the ability to save Your card details for future reference and faster future payments. In such case, We may share Your relevant Personal Information as necessary for the third parties to provide such services, including your name, residence and email address. The processing of payments or authorization is solely in accordance with these third parties policies, terms and conditions and we are not in any manner responsible or liable to You or any third party for any delay or failure at their end in processing the payments.
                            </p>
                            <h5>
                                <span>2</span>Links to third party websites
                            </h5>
                            <p class="text-justify pb-3">
                                Links to third-party advertisements, third-party websites or any third party electronic communication service may be provided on the Website which are operated by third parties and are not controlled by, or affiliated to, or associated with, Snapdeal unless expressly specified on the Website.
                            </p>
                            <p class="text-justify pb-3">
                                Snapdeal is not responsible for any form of transmission, whatsoever, received by You from any third party website. Accordingly, Snapdeal does not make any representations concerning the privacy practices or policies of such third parties or terms of use of such third party websites, nor does Snapdeal control or guarantee the accuracy, integrity, or quality of the information, data, text, software, music, sound, photographs, graphics, videos, messages or other materials available on such third party websites. The inclusion or exclusion does not imply any endorsement by Snapdeal of the third party websites, the website's provider, or the information on the website. The information provided by You to such third party websites shall be governed in accordance with the privacy policies of such third party websites and it is recommended that You review the privacy policy of such third party websites prior to using such websites.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')
@endsection