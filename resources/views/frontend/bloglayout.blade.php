<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
{{--    <title>@yield('title')</title>--}}
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/frontend/images/favicon-32x32.png">
    <!-- styles -->
    <link rel="stylesheet" href="/frontend/css/style_old.css">
</head>


<body>

@include('frontend._partials.blogheader')

@yield('content')


<footer>
    <!-- top footer -->
    <div class="topfooter">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <h3 class="fcoltitle text-uppercase">About Sasaya</h3>
                    <p class="pb-3">We possess within us two minds. So far I have written only of the scious mind.We
                        further know that the subconscious has recorded every event. This is just perfect theme.</p>
                    <h3 class="fcoltitle text-uppercase">News Letter</h3>
                    <!-- news letter form -->
                    <div class="subscribe">
                        <form class="newsform w-100 mt-2" action="{{ route('saveSubscribers') }}"
                              id="saveSubscribers">
                            <input type="text" placeholder="Enter Your Email" name="nl_email">
                            <input type="submit" value="Subscribe with us" class="btnsubscribe">
                            <div class="subscribersformStatus">
                            </div>
                        </form>
                    </div>
                    <!--/ news letter form -->
                </div>
                <div class="col-lg-3">
                    <h3 class="fcoltitle text-uppercase">Company</h3>
                    <ul>
                        <li><a href="{{ route('aboutUs') }}">About us</a></li>
                        <li><a href="{{ route('blog') }}">Blog</a></li>
                        <li><a href="{{ route('faqs') }}">faq</a></li>
                        <li><a href="{{ route('contact') }}">Contact</a></li>
                        <li><a href="{{ route('termsAndConditions') }}">Terms & Conditions</a></li>
                        <li><a href="{{ route('privacyPolicy') }}">Privacy Policy</a></li>
                        <li><a href="{{ route('returnPolicy') }}">Return Policy </a></li>
                    </ul>
                </div>
                @if(count(getCategories())>0)
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">Categories</h3>

                        <ul>
                            @foreach(getCategories() as $catkey=>$category)
                                <li><a href="{{ route('userproducts',['category'=>$catkey]) }}">{{ $category }}</a></li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                <div class="col-lg-3">
                    <h3 class="fcoltitle text-uppercase">Get in Touch with us</h3>
                    <table width="100%" class="mb-3">
                        <tr>
                            <td><i class="fas fa-map-marker-alt"></i></td>
                            <td>
                                <p>H.No: 10-1124, Plot No: 72, Road No:32, Banjarahills, Hyderabad, Telangana, India -
                                    500035</p>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-phone-volume"></i></td>
                            <td>
                                <p>+63 918 4084 694</p>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-envelope"></i></td>
                            <td>
                                <p>info@sayas.com</p>
                            </td>
                        </tr>
                    </table>
                    <h3 class="fcoltitle text-uppercase">Follow us on</h3>
                    <ul class="nav footersocial">
                        <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li class="nav-item"><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--/ top footer -->
    <!-- bottom footer-->
    <div class="bottomfooter position-relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <p class="pt-2">All Rights Reserved by Sayas e-Commerce Online store 2018</p>
                </div>
                <div class="col-lg-6 text-right">
                    <img src="/frontend/images/paymenticon.png" alt="" title="">
                </div>
            </div>
        </div>
        <a id="movetop" class="" href="#"><i class="fas fa-arrow-up"></i></a>
    </div>
    <!--/ bottom footer -->
</footer>

@include('frontend._partials.scripts')

@yield('footerScripts')

</body>

</html>

