@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Your Cart </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link">Cart</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row cart">
                        <!-- cart items -->
                        <div class="col-lg-12">
                            <table class="table tableresp">
                                <thead>
                                <tr>
                                    <th>Item Details</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Sub Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(Cart::getContent()->count()>0)
                                    @foreach( Cart::getContent() as $item)
                                        <tr>
                                            <td>
                                                <div class="cartfigsection">
                                                    <figure class="cartfig float-left">
                                                        <a href="#">

                                                            <?php
                                                            $product = getProductInfobyid($item->id);
                                                            $productImages = getProductImages($item->id);

                                                            if (!empty($product->product_cover_image)) {
                                                                $pname = '/uploads/products/thumbs/' . $product->product_cover_image;
                                                            } else {
                                                                if (count($productImages) > 0) {
                                                                    $pname = '/uploads/products/thumbs/' . $productImages[0]->pi_image_name;
                                                                } else {
                                                                    $pname = '/frontend/images/data/plant01.png';

                                                                }
                                                            }
                                                            ?>

                                                            <img
                                                                    src="{{ $pname }}"
                                                                    class="img-fluid" alt="" title="">
                                                        </a>
                                                    </figure>
                                                    <article>
                                                        <h4 class="pb-1">{{ $item->name }} </h4>
                                                        <p>{!! ($item->attributes->has('description') ? str_limit($item->attributes->description, 150) : '') !!}  </p>
                                                        <p class="pt-2">
                                                            <a href="javascript:void(0)"
                                                               data-id="{{ $item->id }}"
                                                               class="removeCartItem"><i class="fas fa-times"></i>
                                                                Remove</a>
                                                            <a href="javascript:void(0)" data-id="{{ $item->id }}" data-userid="{{ Auth::id() }}" class="btn btn-dark productaddwishlist{{ $item->id }} addToWishList @if(in_array($item->id,$wishlistProducts))likeactive @endif"><i class="far fa-heart"></i>
                                                                Add to
                                                                Wishlist</a>
                                                        </p>
                                                    </article>
                                                </div>
                                            </td>
                                            <td>Rs: {{ $item->price }}</td>
                                            <td>
                                                <select class="form-control itemQty{{ $item->id }}"
                                                        onchange="updatecarts({{ $item->id }})">
                                                    <?php
                                                    for($i = 0;$i < 5;$i++){
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php echo ($item->quantity == $i) ? 'selected' : '' ?>

                                                    ><?php echo $i;?></option>
                                                    <?php
                                                    }

                                                    ?>
                                                </select>
                                            </td>
                                            <td>RS: {{ $item->getPriceSum() }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">No Data Found</td>
                                    </tr>
                                @endif
                                </tbody>
                                @if(Cart::getContent()->count()>0)
                                    <tfoot class="cartfoot">
                                    <tr>

                                        <td><p>Standard Delivery By {{ \Carbon\Carbon::parse(date('d-m-Y'))->addDays('5')->format('d-m-Y') }}</p></td>
                                        <td>
                                            <p>Subtotal:</p>
                                            <p>Delivery Charges:</p>
                                        </td>
                                        <td>
                                            <p>Rs: {{ Cart::getSubTotal() }} </p>
                                            <p>+Rs: {{ paymentDeliveryCharges(Cart::getSubTotal()) }}</p>
                                        </td>
                                        <td colspan="2" align="right">
                                            <a href="{{ route('deliveryAddress') }}">Proceed to Pay
                                                Rs: {{ Cart::getTotal() }}</a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                @endif
                            </table>
                        </div>
                        <!--/ cart items -->
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->


@endsection
@section('footerScripts')
    <script>
        function updatecarts(rowid) {
            var id = rowid;
            var qty = $('.itemQty' + id).val();

            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('updateItemFromCart') }}',
                    type: 'POST',
                    data: 'id=' + id + '&qty=' + qty,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        }

        $('.removeCartItem').on('click', function () {
            var id = $(this).data('id');

            if (id != '') {

                var r = confirm("Are you sure you want to remove from cart!");
                if (r == true) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('removeItemFromCart') }}',
                        type: 'POST',
                        data: 'id=' + id,
                        success: function (response) {
                            location.reload();
                        }
                    });
                }


            }
        });
    </script>
@endsection
