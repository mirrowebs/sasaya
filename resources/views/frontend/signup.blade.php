<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <title>Register with Sasaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/frontend/css/style_old.css">
</head>

<body>
<!--main login and register -->
<main>
    <section class="sign">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 leftsign"></div>
                <div class="col-lg-6 align-self-center">
                    <div class="signin mx-auto">
                        <figure class="text-center signlogo"><a href="{{ route('home') }}"><img
                                        src="/frontend/images/logo.png" alt="" title=""></a></figure>
                        <article class="text-center py-2">
                            <h3>Sign up</h3>
                            <h4 class="pt-3 pb-2">Register with Sasaya</h4>
                        </article>
                        <form method="POST" action="{{ route('register') }}" class="formsign pt-4" id="register"
                              autocomplete="off">
                            {{ csrf_field() }}
                            <input type="hidden" name="role" value="1">
                            <input type="hidden" name="status" value="active">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" type="text"
                                       placeholder="Enter your Email" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                         <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <input class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}"
                                       type="text" placeholder="Enter your Mobile Number" name="mobile">
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                         <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Create Password</label>
                                <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       id="password" type="password" placeholder="Create Password" name="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input class="form-control" id="password-confirm" type="password"
                                       placeholder="Confirm Password" name="password_confirmation">
                            </div>
                            <div class="text-center">
                                <input class="btn" type="submit" value="Signup">
                            </div>

                        </form>

                        <p class="text-center pt-4">Already have an Account?
                            <span><a href="{{ route('userlogin') }}">Signin</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--/ main login and register -->
<!-- footer scripts -->
<script src="/frontend/js/scripts.js"></script>
<script src="/frontend/modules/users/js/register.js"></script>

<!--/ footer scripts -->
</body>
</html>
