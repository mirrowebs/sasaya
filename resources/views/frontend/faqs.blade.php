@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Faq </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link">Faq</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page header -->

            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6" >
                            <div class="accordion">
                                <h3 class="panel-title">The standard Lorem Ipsum passage, used since the 1500s</h3>
                                <div class="panel-content">
                                    <p class="text-justify">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                                </div>
                                <h3 class="panel-title">1914 translation by H. Rackham</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Finibus Bonorum et Malorum", written by Cicero </h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">1914 translation by H. Rackham</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Where can I get some?</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Where does it come from?</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6" >
                            <div class="accordion">
                                <h3 class="panel-title">The standard Lorem Ipsum passage, used since the 1500s</h3>
                                <div class="panel-content">
                                    <p class="text-justify">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                                </div>
                                <h3 class="panel-title">1914 translation by H. Rackham</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Finibus Bonorum et Malorum", written by Cicero </h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">1914 translation by H. Rackham</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Where can I get some?</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                                <h3 class="panel-title">Where does it come from?</h3>
                                <div class="panel-content">
                                    <p class="text-justify">Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->

@endsection
@section('footerScripts')
@endsection