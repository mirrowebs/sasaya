$(document).ready(function () {

    $('#register').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {

            email: {
                required: true
            },
            mobile: {
                required: true
            },
            password: {
                required: true,
                maxlength: 12,
                minlength: 6
            },
            password_confirmation: {
                required: true,
                equalTo: "#password",
                maxlength: 12,
                minlength: 6
            }
        },
        messages: {
            email: {
                required: 'Please enter email'
            },
            mobile: {
                required: 'Please enter mobile'
            },
            password: {
                required: 'Please enter new password'
            },
            password_confirmation: {
                required: 'Please confirm password',
                equalTo: "The confirm password and new password must match."
            }
        },
    });
});
