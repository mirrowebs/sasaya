require('../../../js/app');
require('./easyResponsiveTabs');
require('./jquery.basictable.min');
require('./simplegallery.min');
require('./html5-shiv');
require('./accordian');
require('./xzoom.min');
require('jquery-validation');
require('bootstrap4-notify');
require('@fortawesome/fontawesome-free/js/all');

require('owl.carousel/src/js/owl.carousel');
require('./custom');
