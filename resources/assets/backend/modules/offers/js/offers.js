jQuery(document).ready(function () {
    $('.delete_offer_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deleteofferimage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.imagediv' + image).remove();
                    $('#offerimage').addClass('offerimage');
                    imageRules();
                }
            });
        }
    });
    jQuery('#offers').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            o_title: {
                required: true
            },
            o_discount: {
                required: true
            },
            o_title_link: {
                required: true
            },
            o_video_link: {
                required: true
            },
            o_status: {
                required: true
            }
        },
        messages: {
            o_title: {
                required: 'Please enter title'
            },
            o_discount: {
                required: 'Please enter discount'
            },
            o_title_link: {
                required: 'Please enter Link'
            },
            o_video_link: {
                required: 'Please enter Link'
            },
            o_status: {
                required: 'Please select status'
            }
        },
    });
    imageRules();
});

function imageRules() {
    $(".offerimage").rules("add", {
        required: true,
        messages: {
            required: "Upload image to save"
        }
    });
}