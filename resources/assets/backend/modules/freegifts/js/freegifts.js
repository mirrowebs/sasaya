jQuery(document).ready(function () {

    $('.delete_cover_image').on('click', function () {
        alert('hai');
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deletefreecoverimage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.coverimagediv' + image).remove();
                    $('#productcoverimage').addClass('productcoverimage');
                    imageRules();
                }
            });
        }
    });

    jQuery('#freegifts').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            free_gift_name: {
                required: true
            },
            free_gift_price: {
                required: true
            },
            free_gift_status: {
                required: true
            }
        },
        messages: {
            free_gift_name: {
                required: 'Please enter name'
            },
            free_gift_price: {
                required: 'Please enter price'
            },
            free_gift_status: {
                required: 'Please select status'
            }
        },
    });
});
function imageRules() {
    $(".productcoverimage").rules("add", {
        required: true,
        messages: {
            required: "Upload image to save"
        }
    });
}
