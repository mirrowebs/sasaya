$(document).ready(function () {
    $('.delete_category_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deleteblogimage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.imagediv' + image).remove();
                    $('#categoryimage').addClass('categoryimage');
                    imageRules();
                }
            });
        }
    });
    $('#blogs').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            blog_bc_id: {
                required: true
            },
            blog_name: {
                required: true
            },
             blog_status: {
                required: true
            }
        },
        messages: {
            blog_bc_id: {
                required: 'Please select category'
            },
            blog_name: {
                required: 'Please enter name'
            },
            bc_status: {
                required: 'Please select status'
            }
        },
    });
    imageRules();
});
function imageRules() {
    $(".categoryimage").rules("add", {
        required: true,
        messages: {
            required: "Upload image to save"
        }
    });
}