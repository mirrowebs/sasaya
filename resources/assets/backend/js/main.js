require('../../../js/app');
require('./plugins');
require('jquery-validation');
// require('jquery-ui/external/requirejs/require');
require('jquery-tags-input/src/jquery.tagsinput');

// require('../../../../vendor/jezielmartins/laravel-ckeditor/ckeditor');

// require('@ckeditor/ckeditor5-build-classic/build/ckeditor');


// $.noConflict();

$(function () {

    "use strict";

    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
        new SelectFx(el);
    });

    $('.selectpicker').selectpicker;


    $('#menuToggle').on('click', function (event) {
        $('body').toggleClass('open');
    });

    $('.search-trigger').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });

    $('.search-close').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    // $('.user-area> a').on('click', function(event) {
    // 	event.preventDefault();
    // 	event.stopPropagation();
    // 	$('.user-menu').parent().removeClass('open');
    // 	$('.user-menu').parent().toggleClass('open');
    // });


});

