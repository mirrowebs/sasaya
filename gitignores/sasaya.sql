-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2019 at 03:08 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sasaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` int(10) UNSIGNED NOT NULL,
  `banner_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` longtext COLLATE utf8mb4_unicode_ci,
  `banner_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `banner_title`, `banner_image`, `banner_description`, `banner_link`, `banner_status`, `created_at`, `updated_at`) VALUES
(1, 'This Festival Celebrations with us', '1552303232sliderhome02.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'http://google.com', 'active', '2019-03-11 05:50:32', '2019-03-11 05:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` int(10) UNSIGNED NOT NULL,
  `blog_bc_id` int(11) NOT NULL COMMENT 'Comes from blog categories table',
  `blog_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_desc` longtext COLLATE utf8mb4_unicode_ci,
  `blog_tags` text COLLATE utf8mb4_unicode_ci,
  `blog_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`blog_id`, `blog_bc_id`, `blog_name`, `blog_alias`, `blog_img`, `blog_desc`, `blog_tags`, `blog_status`, `created_at`, `updated_at`) VALUES
(1, 2, 'sample', 'sample', '1552303429blog02.jpg', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'sample,sample1', 'active', '2019-03-11 05:53:50', '2019-03-11 05:53:50'),
(2, 1, 'dfdsfdsf', 'dfdsfdsf', '1552303448blog03.jpg', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'sample,sample1', 'active', '2019-03-11 05:54:08', '2019-03-11 05:54:08');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `bc_id` int(10) UNSIGNED NOT NULL,
  `bc_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bc_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bc_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bc_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`bc_id`, `bc_alias`, `bc_name`, `bc_img`, `bc_status`, `created_at`, `updated_at`) VALUES
(1, 'blog-category1', 'blog category1', '1552303380acc08.png', 'active', '2019-03-11 05:53:00', '2019-03-11 05:53:00'),
(2, 'blog-category-2', 'blog category 2', '1552303392gift01.png', 'active', '2019-03-11 05:53:12', '2019-03-11 05:53:12');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_parent` int(11) NOT NULL DEFAULT '0',
  `category_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_parent`, `category_alias`, `category_image`, `category_status`, `created_at`, `updated_at`) VALUES
(1, 'Gift Articles', 0, 'gift-articles', '1552303026gift01.png', 'active', '2019-03-11 05:47:06', '2019-03-11 05:47:06'),
(2, 'Aromas', 0, 'aromas', '1552303039aromas04.png', 'active', '2019-03-11 05:47:19', '2019-03-11 05:47:19'),
(3, 'TEAS', 0, 'teas', '1552303075teas05.png', 'active', '2019-03-11 05:47:55', '2019-03-11 05:48:04'),
(4, 'Accessories', 0, 'accessories', '1552303106acc02.png', 'active', '2019-03-11 05:48:26', '2019-03-11 05:48:26'),
(5, 'SALT', 0, 'salt', '1552303123salts08.png', 'active', '2019-03-11 05:48:43', '2019-03-11 05:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_phone_prefix` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_code`, `country_phone_prefix`, `country_currency`, `country_language`, `country_status`, `created_at`, `updated_at`) VALUES
(1, 'India', 'ind', '91', 'INR', 'Telugu', 'active', '2019-03-11 05:45:59', '2019-03-11 05:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `free_gifts`
--

CREATE TABLE `free_gifts` (
  `free_gift_id` int(10) UNSIGNED NOT NULL,
  `free_gift_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `free_gift_price` double(8,2) NOT NULL,
  `free_gift_cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `free_gift_uniqid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_gift_description` text COLLATE utf8mb4_unicode_ci,
  `free_gift_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `free_gifts`
--

INSERT INTO `free_gifts` (`free_gift_id`, `free_gift_name`, `free_gift_price`, `free_gift_cover_image`, `free_gift_uniqid`, `free_gift_description`, `free_gift_status`, `created_at`, `updated_at`) VALUES
(1, 'asddasdasdasd', 750.00, '1552395487acc02.png', '11552395487', 'dffgdf dfgdf fdgfg dfdfg', 'active', '2019-03-12 07:28:07', '2019-03-12 07:28:07'),
(2, 'asdasd fdhgdf', 750.00, '1552395514blog01.jpg', '21552395514', 'dgdfgdfg dfgdfg dfgdfg dfgdfgd', 'active', '2019-03-12 07:28:34', '2019-03-12 07:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(22, '2014_10_12_000000_create_users_table', 1),
(23, '2014_10_12_100000_create_password_resets_table', 1),
(24, '2018_12_17_111506_create_categories_table', 1),
(25, '2018_12_17_113913_create_banners_table', 1),
(26, '2018_12_17_115718_create_products_table', 1),
(27, '2018_12_17_120139_create_product_images_table', 1),
(28, '2018_12_18_115708_create_contact_us_table', 1),
(29, '2018_12_18_115903_create_news_letters_table', 1),
(30, '2018_12_18_122204_create_user_address_table', 1),
(31, '2018_12_18_122957_create_countries_table', 1),
(32, '2018_12_18_124257_create_wishlist_table', 1),
(33, '2018_12_19_061741_create_blog_categories_table', 1),
(34, '2018_12_19_110305_create_blogs_table', 1),
(35, '2018_12_19_123710_create_orders_table', 1),
(37, '2018_12_19_131559_create_payment_info_table', 1),
(38, '2019_03_04_145810_create_offers_table', 1),
(39, '2019_03_05_125209_create_reviews_table', 1),
(42, '2019_03_06_093938_create_free_gifts_table', 2),
(43, '2018_12_19_125811_create_order_items_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `news_letters`
--

CREATE TABLE `news_letters` (
  `nl_id` int(10) UNSIGNED NOT NULL,
  `nl_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nl_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `o_id` int(10) UNSIGNED NOT NULL,
  `o_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `o_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `o_discount` double(8,2) DEFAULT NULL,
  `o_title_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `o_video_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `o_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`o_id`, `o_image`, `o_title`, `o_discount`, `o_title_link`, `o_video_link`, `o_status`, `created_at`, `updated_at`) VALUES
(1, '1552303308acc02.png', 'This Festival Celebrations with us', 100.00, 'https://www.lipsum.com', 'https://www.youtube.com', 'active', '2019-03-11 05:51:48', '2019-03-12 03:31:59'),
(2, '1552303344aromas08.png', 'What is Lorem Ipsu', 100.00, 'https://www.lipsum.com/', 'https://youtube.com', 'active', '2019-03-11 05:52:24', '2019-03-11 05:52:24'),
(3, '1552395284aromas03.png', 'sadasd', 750.00, 'https://www.lipsum.com/', 'https://youtube.com', 'active', '2019-03-12 07:24:45', '2019-03-12 07:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_user_id` int(11) NOT NULL,
  `order_discount` double(8,2) DEFAULT NULL,
  `order_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '% or fixed price',
  `order_total_price` double(8,2) NOT NULL,
  `order_delivery_address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_traking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_courier_company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_delivery_expected_date` date DEFAULT NULL,
  `order_payment_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_date` date DEFAULT NULL,
  `order_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_payment_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_shipping_price` double(8,2) DEFAULT NULL,
  `order_sub_total_price` double(8,2) DEFAULT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `order_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_user_id`, `order_discount`, `order_discount_type`, `order_total_price`, `order_delivery_address`, `order_traking_number`, `order_courier_company`, `order_delivery_expected_date`, `order_payment_mode`, `order_payment_date`, `order_reference_number`, `order_payment_transaction_id`, `order_shipping_price`, `order_sub_total_price`, `order_status`, `order_note`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL, 1000.00, 'a:9:{s:7:\"ua_name\";s:5:\"sssss\";s:10:\"ua_address\";s:7:\"asderft\";s:11:\"ua_landmark\";s:9:\"sadasdasd\";s:7:\"ua_city\";s:6:\"asdasd\";s:8:\"ua_state\";s:8:\"asdasdsd\";s:8:\"ua_email\";s:12:\"susu@yui.lop\";s:8:\"ua_phone\";i:2123456789;s:10:\"ua_pincode\";i:432456;s:10:\"ua_country\";s:5:\"India\";}', NULL, NULL, '2019-03-17', NULL, NULL, '3281ffe415a067b5eebb45999b9f2f54', NULL, 100.00, 900.00, 'Pending', NULL, '2019-03-12 07:42:45', '2019-03-12 07:42:45'),
(2, 2, NULL, NULL, 1000.00, 'a:9:{s:7:\"ua_name\";s:5:\"sssss\";s:10:\"ua_address\";s:7:\"asderft\";s:11:\"ua_landmark\";s:9:\"sadasdasd\";s:7:\"ua_city\";s:6:\"asdasd\";s:8:\"ua_state\";s:8:\"asdasdsd\";s:8:\"ua_email\";s:12:\"susu@yui.lop\";s:8:\"ua_phone\";i:2123456789;s:10:\"ua_pincode\";i:432456;s:10:\"ua_country\";s:5:\"India\";}', NULL, NULL, '2019-03-17', NULL, NULL, '33504556e7ebf94ec0a04e4a851e9137', NULL, 100.00, 900.00, 'Pending', NULL, '2019-03-12 07:53:37', '2019-03-12 07:53:37'),
(3, 2, NULL, NULL, 1000.00, 'a:9:{s:7:\"ua_name\";s:5:\"sssss\";s:10:\"ua_address\";s:7:\"asderft\";s:11:\"ua_landmark\";s:9:\"sadasdasd\";s:7:\"ua_city\";s:6:\"asdasd\";s:8:\"ua_state\";s:8:\"asdasdsd\";s:8:\"ua_email\";s:12:\"susu@yui.lop\";s:8:\"ua_phone\";i:2123456789;s:10:\"ua_pincode\";i:432456;s:10:\"ua_country\";s:5:\"India\";}', NULL, NULL, '2019-03-17', NULL, NULL, '8e38b96cf612d16a73b3595ed8d7d6ec', NULL, 100.00, 900.00, 'Pending', NULL, '2019-03-12 07:54:35', '2019-03-12 07:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `oitem_id` int(10) UNSIGNED NOT NULL,
  `oitem_order_id` int(11) NOT NULL,
  `oitem_product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oitem_is_freegift` int(11) NOT NULL COMMENT '1=yes 0=No',
  `oitem_product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oitem_qty` int(11) NOT NULL,
  `oitem_product_price` double(8,2) NOT NULL,
  `oitem_discount_price` double(8,2) DEFAULT NULL,
  `oitem_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '% or fixed price',
  `oitem_sub_total` double(8,2) NOT NULL,
  `oitem_note` text COLLATE utf8mb4_unicode_ci,
  `	oitem_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'order_item' COMMENT 'order item or gift',
  `	oitem_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`oitem_id`, `oitem_order_id`, `oitem_product_id`, `oitem_is_freegift`, `oitem_product_name`, `oitem_qty`, `oitem_product_price`, `oitem_discount_price`, `oitem_discount_type`, `oitem_sub_total`, `oitem_note`, `	oitem_type`, `	oitem_status`, `created_at`, `updated_at`) VALUES
(1, 3, '1', 0, 'jhgjhgjgj', 1, 900.00, NULL, NULL, 900.00, NULL, 'order_item', 'pending', '2019-03-12 07:54:35', '2019-03-12 07:54:35'),
(2, 3, '11552395487', 1, 'asddasdasdasd', 1, 0.00, NULL, NULL, 0.00, NULL, 'order_item', 'pending', '2019-03-12 07:54:35', '2019-03-12 07:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

CREATE TABLE `payment_info` (
  `pf_id` int(10) UNSIGNED NOT NULL,
  `pf_user_id` int(11) NOT NULL,
  `pf_order_id` int(11) NOT NULL,
  `pf_gateway_info` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` longtext COLLATE utf8mb4_unicode_ci,
  `product_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_package_dimension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_part_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_primary_material` text COLLATE utf8mb4_unicode_ci,
  `product_capacity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_no_of_pics` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_real_price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_video_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_tech_details` text COLLATE utf8mb4_unicode_ci,
  `product_shipping_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `product_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '% or fixed price',
  `product_category` int(11) DEFAULT NULL,
  `product_sub_category` int(11) DEFAULT NULL,
  `product_cover_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_linked_products` text COLLATE utf8mb4_unicode_ci,
  `product_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_alias`, `product_description`, `product_weight`, `product_color`, `product_package_dimension`, `product_part_number`, `product_primary_material`, `product_capacity`, `product_no_of_pics`, `product_real_price`, `product_price`, `product_video_id`, `product_tech_details`, `product_shipping_weight`, `product_sku`, `product_type`, `product_availability`, `product_discount`, `product_discount_type`, `product_category`, `product_sub_category`, `product_cover_image`, `product_linked_products`, `product_status`, `created_at`, `updated_at`) VALUES
(1, 'jhgjhgjgj', 'jhgjhgjgj', '<p>asdasdasdasd</p>', '100', 'sadasdsa', '1000', 'sdsdfsdf', 'sadsadasdsad', 'sdsadasd', NULL, 1000.00, 900.00, NULL, '<p>asdasdasdasd</p>', NULL, 'dsdsdsdds', 'sample', 'in_stock', 10.00, '%', 1, NULL, '1552303612acc01.png', 'sadasdasdsd', 'active', '2019-03-11 05:56:52', '2019-03-11 05:56:52'),
(2, 'zxzxczxc', 'zxzxczxc', '<p>dfgdfg</p>', 'xcvxcv', 'cvcbcvb', 'xvxcvxcv', 'xcvxcv', 'dfgdfgdfg', 'dfgdfgdf', NULL, 500.00, 450.00, 'sdfdsf', '<p>dfgdf</p>', NULL, 'zczxczxvxz', 'xcvxcv', 'in_stock', 10.00, '%', 2, NULL, '1552303958aromas07.png', 'gdfgfdgdfg', 'active', '2019-03-11 06:02:38', '2019-03-11 06:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `pi_id` int(10) UNSIGNED NOT NULL,
  `pi_product_id` int(11) NOT NULL,
  `pi_image_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pi_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`pi_id`, `pi_product_id`, `pi_image_name`, `pi_status`, `created_at`, `updated_at`) VALUES
(1, 1, '1552303751acc04.png', 'active', '2019-03-11 05:59:12', '2019-03-11 05:59:12'),
(2, 1, '1552303752acc05.png', 'active', '2019-03-11 05:59:12', '2019-03-11 05:59:12'),
(3, 1, '1552303752aromas05.png', 'active', '2019-03-11 05:59:12', '2019-03-11 05:59:12'),
(4, 1, '1552303752aromas06.png', 'active', '2019-03-11 05:59:12', '2019-03-11 05:59:12'),
(5, 1, '1552303752aromas07.png', 'active', '2019-03-11 05:59:12', '2019-03-11 05:59:12'),
(6, 2, '1552303974aromas02.png', 'active', '2019-03-11 06:02:54', '2019-03-11 06:02:54'),
(7, 2, '1552303974aromas03.png', 'active', '2019-03-11 06:02:54', '2019-03-11 06:02:54'),
(8, 2, '1552303974blog04.jpg', 'active', '2019-03-11 06:02:55', '2019-03-11 06:02:55'),
(9, 2, '1552303975gift01.png', 'active', '2019-03-11 06:02:55', '2019-03-11 06:02:55'),
(10, 2, '1552303975offer02.jpg', 'active', '2019-03-11 06:02:55', '2019-03-11 06:02:55'),
(11, 2, '1552303975offer03.jpg', 'active', '2019-03-11 06:02:55', '2019-03-11 06:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `review_id` int(10) UNSIGNED NOT NULL,
  `review_user_id` int(11) NOT NULL,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_rating` int(11) NOT NULL,
  `review_comment` text COLLATE utf8mb4_unicode_ci,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL COMMENT '404=admin,1=user',
  `mobile` bigint(20) DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `mobile`, `gender`, `dob`, `image`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', 404, 9099098908, 'Female', '2018-09-10', NULL, NULL, '$2y$10$08W9YZIoTse./hH7.zZn5uBx2f5nXLas5EnNl2bJVIVdd2d8Cc1g.', 'active', 'TJQ4XjhqtbHEZJH4ZRqeODewm8VYJbmPiEjT0XRBlBqiBouC7gNC22YB6ptx', NULL, NULL),
(2, 'sample', 'susu@gmail.com', 1, 9809801212, NULL, NULL, NULL, NULL, '$2y$10$J2rqM4tzG0wLLa9X98BfuuCAYYfq4pnindEYVTGWGDCwPD5JEIPny', 'active', 'dSilSCaksnjAolkfMbXzMU7SkPcd2XrHoLC8ZS094D9mAXLynTjJgcS7s5Km', '2019-02-01 00:35:24', '2019-02-01 00:35:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `ua_id` int(10) UNSIGNED NOT NULL,
  `ua_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ua_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ua_phone` bigint(20) DEFAULT NULL,
  `ua_defult` int(11) NOT NULL DEFAULT '0' COMMENT '1=default address',
  `ua_user_id` int(11) NOT NULL,
  `ua_country` int(11) NOT NULL,
  `ua_pincode` int(11) NOT NULL,
  `ua_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`ua_id`, `ua_name`, `ua_address`, `ua_landmark`, `ua_state`, `ua_city`, `ua_email`, `ua_phone`, `ua_defult`, `ua_user_id`, `ua_country`, `ua_pincode`, `ua_status`, `created_at`, `updated_at`) VALUES
(1, 'sssss', 'asderft', 'sadasdasd', 'asdasdsd', 'asdasd', 'susu@yui.lop', 2123456789, 1, 2, 1, 432456, 'active', '2019-03-12 06:38:20', '2019-03-12 06:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `wishlist_id` int(10) UNSIGNED NOT NULL,
  `wishlist_product_id` int(11) NOT NULL,
  `wishlist_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`bc_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `free_gifts`
--
ALTER TABLE `free_gifts`
  ADD PRIMARY KEY (`free_gift_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_letters`
--
ALTER TABLE `news_letters`
  ADD PRIMARY KEY (`nl_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`oitem_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_info`
--
ALTER TABLE `payment_info`
  ADD PRIMARY KEY (`pf_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`ua_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`wishlist_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `bc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `free_gifts`
--
ALTER TABLE `free_gifts`
  MODIFY `free_gift_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `news_letters`
--
ALTER TABLE `news_letters`
  MODIFY `nl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `o_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `oitem_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_info`
--
ALTER TABLE `payment_info`
  MODIFY `pf_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `pi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `review_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `ua_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `wishlist_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
