<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\CountriesController;
use App\Http\Controllers\Backend\FreeGiftsController;
use App\Http\Controllers\Backend\CategoriesController;
use App\Http\Controllers\Backend\BannersController;
use App\Http\Controllers\Backend\OffersController;
use App\Http\Controllers\Backend\BlogCategoriesController;
use App\Http\Controllers\Backend\BlogsController;
use App\Http\Controllers\Backend\ProductsController;
use App\Http\Controllers\Backend\UsersController;

Route::get('/', [HomeController::class, 'index'])->name('home');


/* login module routes */

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('userlogin');
Route::post('/login', [LoginController::class,'login']);
Route::post('/logout', [LoginController::class,'logout'])->name('userlogout');

// Registration Routes...

Route::get('register', [RegisterController::class,'showRegistrationForm'])->name('register');

Route::post('register', [RegisterController::class,'register']);

// Password Reset Routes...d
Route::get('password/reset', [ForgotPasswordController::class,'showLinkRequestForm'])->name('password.reset');
Route::post('password/email', [ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class,'showResetForm'])->name('password.reset.token');
Route::post('password/reset', [ResetPasswordController::class,'reset'])->name('password.update');


// Add to Cart
Route::match(array('GET', 'POST'), '/ajax/addToCart', [HomeController::class, 'addToCart'])->name('addToCart');
// Add Free gift to Cart
Route::match(array('GET', 'POST'), '/ajax/addfreeGiftToCart', [UserController::class,'addfreeGiftToCart'])->name('addfreeGiftToCart');
// Add to Cart for only one product
Route::match(array('GET', 'POST'), '/product/addTocart', [HomeController::class, 'buyProduct'])->name('buyProduct');
// update From Cart
Route::match(array('GET', 'POST'), '/ajax/update/item/',  [HomeController::class,'updateItemFromCart'])->name('updateItemFromCart');
// Remove From Cart
Route::match(array('GET', 'POST'), '/ajax/remove/item/',  [HomeController::class,'removeItemFromCart'])->name('removeItemFromCart');
//Add to wishlist
Route::match(array('GET', 'POST'), '/ajax/addToWishlist', [UserController::class, 'addToWishList'])->name('addToWishList');
// Cart Details
Route::match(array('GET', 'POST'), '/cart/list',  [HomeController::class,'cartDetails'])->name('cartDetails');
// Product List
Route::match(array('GET', 'POST'), '/category/{category}/{subcategory?}',  [HomeController::class,'productslist'])->name('userproducts');
// Product Details
Route::match(array('GET', 'POST'), '/product/view/{p_alias}',  [HomeController::class,'productDetails'])->name('productDetails');
// Contactus
Route::match(array('GET', 'POST'), '/contact-us',  [HomeController::class,'contactUs'])->name('contact');
// Aboutus
Route::match(array('GET', 'POST'), '/about-us',  [HomeController::class,'aboutUs'])->name('aboutUs');
// Blog
Route::match(array('GET', 'POST'), '/blog/{blog_category?}/{blog_alias?}',  [HomeController::class,'blog'])->name('blog');
// Faqs
Route::match(array('GET', 'POST'), '/faqs',  [HomeController::class,'faqs'])->name('faqs');
// Terms And Conditions
Route::match(array('GET', 'POST'), '/terms/conditions',  [HomeController::class,'termsAndConditions'])->name('termsAndConditions');
// Privacy Policy
Route::match(array('GET', 'POST'), '/privacy/policy',  [HomeController::class,'privacyPolicy'])->name('privacyPolicy');
// Return Policy
Route::match(array('GET', 'POST'), '/return/policy',  [HomeController::class,'returnPolicy'])->name('returnPolicy');
// Save Subscribers
Route::post('/ajax/save/subscribers',  [HomeController::class,'saveSubscribers'])->name('saveSubscribers');
// Save Reviews
Route::post('/ajax/save/reviews', [UserController::class,'savereview'])->name('savereview');
// Response from payment gateway
Route::post('/payment/response', [UserController::class,'paymentResponse'])->name('paymentResponse');
// Payment Confirmation
Route::match(array('GET', 'POST'), '/payment/confirmation', [UserController::class,'paymentConfirmation'])->name('paymentConfirmation');
// Payment Success
Route::match(array('GET', 'POST'), '/payment/success/{order}', [UserController::class,'paymentSuccessPage'])->name('paymentSuccessPage');


Route::prefix('user')->group(function () {
    // User Profile
    Route::match(array('GET', 'POST'), '/profile', [UserController::class,'index'])->name('userprofile');
    // Change Password
    Route::match(array('GET', 'POST'), '/change/password', [UserController::class,'changePassword'])->name('userChangePassword');
    // Add to wish list
    Route::match(array('GET', 'POST'), '/wish/list', [UserController::class,'wishList'])->name('wishList');
    // Delete User Image
    Route::post('/ajax/deleteuserimage', [UserController::class,'deleteUserimage'])->name('deleteUserimage');
    // Address Book
    Route::match(array('GET', 'POST'), '/addres/book', [UserController::class,'userAddressBook'])->name('userAddressBook');
    Route::match(array('GET', 'POST'), '/manage/user/addres/book', [UserController::class,'userAddAddressBook'])->name('userAddAddressBook');
    Route::match(array('GET', 'POST'), '/make/default/addres', [UserController::class,'makeDefaultAddress'])->name('makeDefaultAddress');
    // User Orders
    Route::match(array('GET', 'POST'), '/orders', [UserController::class,'orders'])->name('orders');
    Route::match(array('GET', 'POST'), '/order/details/{order}', [UserController::class,'orderDetails'])->name('orderDetails');
    // Confirmation Delivery Address
    Route::match(array('GET', 'POST'), '/confirm/delivery/address', [UserController::class,'deliveryAddress'])->name('deliveryAddress');
    // Payment Confirmation
    Route::match(array('GET', 'POST'), '/save/orders', [UserController::class,'saveOrders'])->name('saveOrders');
    // Order Invoice
    Route::match(array('GET', 'POST'), '/invoice/details/{order}', [UserController::class,'orderInvoice'])->name('orderInvoice');
});


Route::prefix('ajax')->group(function () {

//    Route::post('upload_image','AjaxController@uploadAction')->name('EditorUpload');
    Route::post('ajax/deleteproductimage', 'AjaxController@deleteproductimage')->name('ajax_deleteproductimage');
//    Route::post('ajax/deleteproductcoverimage', 'backend\ProductsController@deleteproductcoverimage')->name('deleteproductcoverimage');

});



Route::prefix('admin')->group(function () {

    /* login module routes */
    Route::get('/', [LoginController::class,'showLoginForm'])->name('login');
    Route::post('/', [LoginController::class,'login']);
    Route::post('/logout', [LoginController::class,'logout'])->name('logout');

    /* Dashboard routes */
    Route::get('/dashboard', [AdminController::class,'index'])->name('dashboard');

    /* Change Password */
    Route::match(array('GET', 'POST'), '/change/password',  [AdminController::class,'changePassword'])->name('changePassword');

    /* update Profile */
    Route::match(array('GET', 'POST'), '/update/profile',  [AdminController::class,'profile'])->name('profile');

    /* Countries module routes */
    Route::match(array('GET', 'POST'), '/countries', [CountriesController::class,'index'])->name('countries');
    Route::match(array('GET', 'POST'), '/countries/manage/{id?}', [CountriesController::class,'addNewCountries'])->name('addNewCountries');

    /* Free Gifts module routes */
    Route::match(array('GET', 'POST'), '/freegifts', [FreeGiftsController::class,'index'])->name('freegifts');
    Route::match(array('GET', 'POST'), '/freegifts/manage/{id?}', [FreeGiftsController::class,'addNewFreeGifts'])->name('addNewFreeGifts');
    Route::post('ajax/deletefreecoverimage',  [FreeGiftsController::class,'deletefreecoverimage'])->name('deletefreecoverimage');

    /* Categories module routes */
    Route::match(array('GET', 'POST'), '/categories', [CategoriesController::class,'index'])->name('categories');
    Route::match(array('GET', 'POST'), '/categories/manage/{id?}', [CategoriesController::class,'addNewCategories'])->name('addNewCategories');
    Route::post('ajax/deletecategoryimage', [CategoriesController::class,'deleteCategoryimage'])->name('deleteCategoryimage');
    Route::post('ajax/getsubcategories', [CategoriesController::class,'getsubcategories'])->name('getsubcategories');

    /* Banners module routes */
    Route::match(array('GET', 'POST'), '/banners',  [BannersController::class,'index'])->name('banners');
    Route::match(array('GET', 'POST'), '/banners/manage/{id?}', [BannersController::class,'addNewBanners'])->name('addNewBanners');
    Route::post('ajax/deletebannerimage', [BannersController::class,'deleteBannerimage'])->name('deleteBannerimage');

    /* Offers module routes */
    Route::match(array('GET', 'POST'), '/offers', [OffersController::class,'index'])->name('offers');
    Route::match(array('GET', 'POST'), '/offers/manage/{id?}', [OffersController::class,'addNewOffers'])->name('addNewOffers');
    Route::post('ajax/deleteoffeerimage', [OffersController::class,'deleteOfferimage'])->name('deleteOfferimage');

    /* Blog Categories module routes */
    Route::match(array('GET', 'POST'), '/blog/categories', [BlogCategoriesController::class,'index'])->name('blogCategories');
    Route::match(array('GET', 'POST'), '/blog/categories/manage/{id?}', [BlogCategoriesController::class,'addNewCategories'])->name('addNewBlogCategories');
    Route::post('ajax/blog/deletecategoryimage',[BlogCategoriesController::class,'deleteCategoryimage'])->name('deleteBlogCategoryimage');

    /* Blog module routes */
    Route::match(array('GET', 'POST'), '/blogs', [BlogsController::class,'index'])->name('blogs');
    Route::match(array('GET', 'POST'), '/blogs/manage/{id?}', [BlogsController::class,'addNewBlogs'])->name('addNewBlogs');
    Route::post('ajax/deleteblogimage', [BlogsController::class,'deleteBlogimage'])->name('deleteBlogimage');

    /* Products module routes */
    Route::match(array('GET', 'POST'), '/products', [ProductsController::class,'index'])->name('products');
    Route::match(array('GET', 'POST'), '/product/overview/{id?}', [ProductsController::class,'productInfo'])->name('productInfo');
    Route::match(array('GET', 'POST'), '/products/manage/{id?}', [ProductsController::class,'addNewProducts'])->name('addNewProducts');

    Route::match(array('GET', 'POST'), '/product/images/{id?}', [ProductsController::class,'addProductImages'])->name('addProductImages');

    /* Users module routes */
    Route::match(array('GET', 'POST'), '/users', [UsersController::class,'index'])->name('users');
    Route::match(array('GET', 'POST'), '/user/Address/{id}', [UsersController::class,'userAddress'])->name('userAddress');
    Route::match(array('GET', 'POST'), '/user/orders/{id}', [UsersController::class,'userOrders'])->name('userAdminOrders');
    Route::match(array('GET', 'POST'), '/user/wishlist/{id}', [UsersController::class,'userWishlist'])->name('userWishlist');

    /* Orders module routes */
    Route::match(array('GET', 'POST'), '/orders', [UsersController::class,'orders'])->name('adminorders');

    /* News Letters routes */
    Route::get('/news/letters', [AdminController::class,'newsLetters'])->name('newsLetters');

    /* Contact Us routes */
    Route::get('/contact/us', [AdminController::class,'contactus'])->name('contactus');


});
