<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'o_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['o_image', 'o_title', 'o_discount', 'o_title_link', 'o_video_link', 'o_status'];
}
