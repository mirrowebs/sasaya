<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class ProductImages extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pi_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pi_product_id', 'pi_image_name', 'pi_status'];


    public static function fileDir($type = '')
    {

        $pathDir = '/uploads/products/';

        if ($type == 'url') {
            $uploadPath = url($pathDir);
            $thumbPath = url($pathDir . 'thumbs/');
        } else {

            $uploadPath = public_path($pathDir);

            if (!file_exists($uploadPath)) {
                mkdir($uploadPath, 0777, true);
            }

            $thumbPath = public_path($pathDir . 'thumbs/');

            if (!file_exists($thumbPath)) {
                mkdir($thumbPath, 0777, true);
            }


        }


        return ['imgs' => $uploadPath, 'thumb' => $thumbPath];

    }

    public static function fileUploads($file)
    {

        $imagesFolder = self::fileDir();

        $uploadPath = $imagesFolder['imgs'];
        $thumbPath = $imagesFolder['thumb'];


        $thumb_width = 400;
        $thumb_height = 400;
        $thumb_quality = 60;


        $extension = $file->getClientOriginalExtension();
        $fileName = time() . '_' . uniqid() . '.' . $extension;


        $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize($thumb_width, $thumb_height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumb_img->save($thumbPath . '/' . $fileName, $thumb_quality);

        $file->move($uploadPath, $fileName);

        return $fileName;

    }

    public static function fileUpload($fileinput, $product_id)
    {


//        dd($product_id);

        foreach ($fileinput as $file) {


            $fileName = self::fileUploads($file);

            $requestdata = array();
            $requestdata['pi_image_name'] = $fileName;
            $requestdata['pi_status'] = 'active';
            $requestdata['pi_product_id'] = $product_id;
           $name = ProductImages::create($requestdata);

//           dd($name);
        }

    }


    public static function removeImage($imgId)
    {


        $images = self::where('pi_id', $imgId)->first();

//        dd($images);

        if (isset($images) && !empty($images)) {
            ProductImages::destroy($imgId);

            $imagesFolder = self::fileDir();

            $uploadPath = $imagesFolder['imgs'];
            $thumbPath = $imagesFolder['thumb'];


            File::delete($uploadPath . $images->pi_image_name);
            File::delete($thumbPath . $images->pi_image_name);
        }
        return true;
    }

}
