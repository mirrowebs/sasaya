<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_address';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'ua_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ua_name', 'ua_address', 'ua_landmark', 'ua_city', 'ua_state', 'ua_email', 'ua_phone', 'ua_defult', 'ua_user_id', 'ua_country', 'ua_status', 'ua_pincode'];

    public function getCountry()
    {
        return $this->belongsTo(Countries::Class, 'ua_country');
    }
}
