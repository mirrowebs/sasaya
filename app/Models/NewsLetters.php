<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsLetters extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_letters';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'nl_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nl_email', 'nl_status'];
}
