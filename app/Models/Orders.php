<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'order_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_user_id', 'order_discount', 'order_discount_type', 'order_total_price', 'order_delivery_address', 'order_traking_number', 'order_courier_company', 'order_delivery_expected_date', 'order_payment_mode', 'order_payment_date', 'order_reference_number', 'order_payment_transaction_id', 'order_status', 'order_note','order_sub_total_price','order_shipping_price'];
    public function orderItems()
    {
        return $this->hasMany(OrderItems::Class, 'oitem_order_id', 'order_id');
    }
}
