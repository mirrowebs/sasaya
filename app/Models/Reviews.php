<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'review_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['review_user_id', 'review_title', 'review_rating', 'review_comment', 'review_status'];

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'review_user_id');
    }

}
