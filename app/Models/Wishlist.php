<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wishlist';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'wishlist_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['wishlist_product_id', 'wishlist_user_id'];

    public function getProduct()
    {
        return $this->belongsTo(Products::Class, 'wishlist_product_id');
    }
}
