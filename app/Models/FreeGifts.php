<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreeGifts extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'free_gifts';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'free_gift_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['free_gift_name', 'free_gift_price', 'free_gift_cover_image', 'free_gift_description', 'free_gift_status','free_gift_uniqid'];
}
