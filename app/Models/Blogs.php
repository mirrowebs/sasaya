<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'blog_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['blog_bc_id', 'blog_name', 'blog_alias', 'blog_img', 'blog_desc', 'blog_tags', 'blog_status'];

    public function getCategory()
    {
        return $this->belongsTo(BlogCategories::Class, 'blog_bc_id');
    }

}
