<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'banner_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['banner_title', 'banner_image', 'banner_description', 'banner_link', 'banner_status'];
}
