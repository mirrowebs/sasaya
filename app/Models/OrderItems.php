<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'oitem_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['oitem_order_id', 'oitem_product_id', 'oitem_product_name', 'oitem_qty', 'oitem_product_price', 'oitem_discount_price', 'oitem_discount_type', 'oitem_sub_total', 'oitem_note', 'oitem_type', 'oitem_status', 'oitem_is_freegift'];

    public function getProduct()
    {
        return $this->belongsTo(Products::Class, 'oitem_product_id');
    }

    public function getFreeProduct()
    {
        return $this->belongsTo(FreeGifts::Class, 'oitem_product_id','free_gift_uniqid');
    }

    public function getOrder()
    {
        return $this->belongsTo(Orders::Class, 'oitem_order_id');
    }
}
