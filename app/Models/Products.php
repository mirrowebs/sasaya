<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'p_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'p_category',
        'p_sub_category',
        'p_name',
        'p_alias',
        'p_sku',
        'p_type',
        'p_weight',
        'p_part_number',
        'p_video_id',
        'p_color',
        'p_mrp_price',
        'p_sel_price',
        'p_capacity',
        'p_package_dimension',
        'p_linked_products',
        'p_availability',
        'p_status',
        'p_primary_material',
        'p_tech_details',
        'p_meta_description',
        'p_meta_keywords',
    ];

    public function getCategory()
    {
        return $this->belongsTo(Categories::Class, 'p_category');
    }
    public function getSubCategory()
    {
        return $this->belongsTo(Categories::Class, 'p_sub_category');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImages::Class, 'pi_product_id', 'p_id');
    }

    static public function createProduct($request)
    {


        $returnMsg = array();




        $requestData = $request->all();

        $requestData['p_alias'] = str_slug($requestData['p_name'], '-');


        if ($requestData['p_id'] == '') {


            $request->validate([
                'p_category' => 'required',
                'p_sub_category' => 'required',

                'p_name' => 'required|unique:products',
                'p_type' => 'required',
                'p_weight' => 'required',
                'p_part_number' => 'required',

//            'p_video_id' => 'required',
//            'p_color' => 'required',

                'p_mrp_price' => 'required',
                'p_sel_price' => 'required',
                'p_package_dimension' => 'required',

                'p_availability' => 'required',
                'p_status' => 'required',

//            'p_primary_material' => 'required',
//            'p_tech_details' => 'required',
            ], [
                'p_category.required' => 'Please select category',
                'p_sub_category.required' => 'Please select sub category',

                'p_name.required' => 'Please enter name',
                'p_name.unique' => 'This product name is already exist',
                'p_type.required' => 'Please enter type',
                'p_weight.required' => 'Please enter wight',
                'p_part_number.required' => 'Please enter part number',

//            'p_video_id.required' => 'Please enter video id',
//            'p_color.required' => 'Please enter color',

                'p_mrp_price.required' => 'select mrp price',
                'p_sel_price.required' => 'Select sel price',
                'p_package_dimension.required' => 'Select packing dimension',

                'p_availability' => 'Select availability',
                'p_status' => 'Select status',

//            'p_primary_material' => 'Enter material',
//            'p_tech_details' => 'Enter tech details',
            ]);


            $p_id = self::create($requestData)->p_id;

            if ($request->hasFile('p_images')) {
                ProductImages::fileUpload($request['p_images'], $p_id);
            }

            $returnMsg['msg_p_id'] = $p_id;
            $returnMsg['msg_code'] = 'create';
            $returnMsg['msg_text'] = 'Product added successfully!';


        } else {
            $p_id = $requestData['p_id'];

            $request->validate([
                'p_category' => 'required',
                'p_sub_category' => 'required',

                'p_name' => 'required|unique:products,p_name,' . $p_id . ',p_id',
//                'p_name' => 'required|unique:products',
                'p_type' => 'required',
                'p_weight' => 'required',
                'p_part_number' => 'required',

//            'p_video_id' => 'required',
//            'p_color' => 'required',

                'p_mrp_price' => 'required',
                'p_sel_price' => 'required',
                'p_package_dimension' => 'required',

                'p_availability' => 'required',
                'p_status' => 'required',

//            'p_primary_material' => 'required',
//            'p_tech_details' => 'required',
            ], [
                'p_category.required' => 'Please select category',
                'p_sub_category.required' => 'Please select sub category',

                'p_name.required' => 'Please enter name',
                'p_name.unique' => 'This product name is already exist',
                'p_type.required' => 'Please enter type',
                'p_weight.required' => 'Please enter wight',
                'p_part_number.required' => 'Please enter part number',

//            'p_video_id.required' => 'Please enter video id',
//            'p_color.required' => 'Please enter color',

                'p_mrp_price.required' => 'select mrp price',
                'p_sel_price.required' => 'Select sel price',
                'p_package_dimension.required' => 'Select packing dimension',

                'p_availability' => 'Select availability',
                'p_status' => 'Select status',

//            'p_primary_material' => 'Enter material',
//            'p_tech_details' => 'Enter tech details',
            ]);


//            dd($p_id);

            if ($request->hasFile('p_images')) {
                ProductImages::fileUpload($request['p_images'], $p_id);
            }

            $products = self::findOrFail($p_id);

            $products->update($requestData);
            $returnMsg['msg_p_id'] = $p_id;
            $returnMsg['msg_code'] = 'update';
            $returnMsg['msg_text'] = 'Product updated successfully!';


        }


        return $returnMsg;


    }


}
