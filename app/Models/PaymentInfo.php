<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_info';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pf_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pf_user_id', 'pf_order_id', 'pf_gateway_info'];

    public function getOrder()
    {
        return $this->belongsTo(Orders::Class, 'pf_order_id');
    }

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'pf_user_id');
    }
}
