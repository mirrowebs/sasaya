<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'p_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['p_order_id', 'p_tracking_id', 'p_bank_ref_no', 'p_order_status', 'p_payment_mode', 'p_card_name', 'p_status_code', 'p_status_message', 'p_currency', 'p_amount', 'p_billing_name', 'p_billing_address', 'p_billing_city', 'p_billing_state', 'p_billing_zip', 'p_billing_country', 'p_billing_tel', 'p_billing_email', 'p_delivery_name', 'p_delivery_address', 'p_delivery_city', 'p_delivery_state', 'p_delivery_zip', 'p_delivery_country', 'p_delivery_tel', 'p_country', 'p_invoice_number'];

}
