<?php


use App\Models\Blogs;

function getCategoriesById($categoryid = null)
{
    if ($categoryid) {
        $countries = \App\Models\Categories::where('category_status', 'active')->where('category_parent', 0)->whereNotIn('category_id', [$categoryid])->orderBy('category_id', 'desc')->pluck('category_name', 'category_id');
    } else {
        $countries = \App\Models\Categories::where('category_status', 'active')->where('category_parent', 0)->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
    }

    return $countries;
}

function getCategoryByID($categoryid)
{
    $category = \App\Models\Categories::where('category_id', $categoryid)->first();
    return $category;
}
function newsLists()
{
    return Blogs::where('blog_status', 'active')->orderBy('blog_bc_id', 'desc')->limit(3)->get();

}

function getCategories()
{
    $categories = \App\Models\Categories::where('category_status', 'active')->where('category_parent', 0)->pluck('category_name', 'category_alias');
    return $categories;

}
function getBlogCategories()
{
    $categories = \App\Models\BlogCategories::where('bc_status', 'active')->orderBy('bc_id', 'desc')->pluck('bc_name', 'bc_alias');
    return $categories;

}

function getBlogCategoriesById()
{
    $categories = \App\Models\BlogCategories::where('bc_status', 'active')->orderBy('bc_id', 'desc')->pluck('bc_name', 'bc_id');
    return $categories;
}

function getProductsByCategory($categoryid = null)
{
    $products = \App\Models\Products::where('p_status', 'active')->where('p_category', $categoryid)->get();
    return $products;
}

function getSubcategoriesByCategory($categoryid = null)
{
    $subcategory = \App\Models\Categories::where('category_alias', $categoryid)->first();
    $categories = \App\Models\Categories::where('category_status', 'active')->where('category_parent', $subcategory->category_id)->pluck('category_name', 'category_alias');
    return $categories;

}

function getWishlistProducts()
{
    $products = \App\Models\Wishlist::where('wishlist_user_id', \Illuminate\Support\Facades\Auth::id())->pluck('wishlist_product_id')->toArray();
    return $products;

}

function getProductInfo($alias)
{
    $productInfo = \App\Models\Products::with('productImages')->where('p_alias', $alias)->first();
    return $productInfo;
}
function getBlogCategoryById($id)
{
    $catInfo = \App\Models\BlogCategories::where('bc_id', $id)->first();
    return $catInfo;
}
function getProductInfobyid($id)
{
    $productInfo = \App\Models\Products::where('p_id', $id)->first();
    return $productInfo;
}

function getProductImages($product_id)
{
    $images = \App\Models\ProductImages::where('pi_product_id', $product_id)->orderBy('pi_id', 'desc')->get();
    return $images;
}
function getCountries()
{
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
    return $countries;

}



