<?php

use \App\Models\ProductImages;


function getCurency($type = 'symbol')
{
    if ($type == '') {
        return 'INR';
    } else {
        return '&#x20B9;';

    }

}
function str_slug($type)
{
  return \Illuminate\Support\Str::slug($type);

}

function str_limit($text, $limit=50)
{
  return \Illuminate\Support\Str::of(strip_tags($text))->limit($limit);

}


function productImageUrl($type = 'all')
{

    if ($type == 'thumb') {
        return ProductImages::fileDir('url')['thumb'] . '/';

    } elseif ($type == 'img') {

        return ProductImages::fileDir('url')['imgs'] . '/';
    } else {

        return ProductImages::fileDir('url');
    }

}

function formValidationError($errors, $inputname)
{
    if ($errors->has($inputname)) {
        ?>
        <div class="text-danger help-block">

            <?php
            echo $errors->first($inputname)
            ?>

        </div>
        <?php
    }
}


function laravelReturnMessageShow()
{
    if (Session::has('flash_message')) {
        ?>
        <br/>
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get('flash_message') ?></strong>
        </div>
        <?php
    }

    if (Session::has('flash_message_error')) {
        ?>

        <br/>
        <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get('flash_message_error') ?></strong>
        </div>

        <?php
    }

}

function availability($flag = null)
{

    $return_data =
        array('in_stock' => 'In Stock', 'out_of_stock' => 'Out Of Stock');
    if ($flag) {
        $return_data = $return_data[$flag];
    }
    return $return_data;

}

function paymentRanges($flag = null)
{

    $return_data =
        array('750' => '750<', '1200' => '1200<', '1500' => '1500<', '2500' => '2500<');
    if ($flag) {
        $return_data = $return_data[$flag];
    }
    return $return_data;

}

function getPaymentRanges($total)
{
    $range = '';
    if ($total > 2500) {
        $range = 2500;
    } elseif ($total > 1500) {
        $range = 1500;
    } elseif ($total > 1200) {
        $range = 1200;
    } elseif ($total > 750) {
        $range = 750;
    }
    return $range;

}

function paymentDeliveryCharges($total)
{
    $deliveryCharge = 0;

    if ($total < 300) {
        $deliveryCharge = 30;
    } elseif ($total < 600) {
        $deliveryCharge = 60;
    } elseif ($total < 800) {
        $deliveryCharge = 80;
    } elseif ($total < 1000) {
        $deliveryCharge = 100;
    }
    return $deliveryCharge;

}


function allStatuses($module, $status = null)
{
    $return_data = "";
    $displayStatus =
        array(
            "general" => array('active' => 'Active', 'inactive' => 'Inactive'),
        );

    if ($module && $status) {
        $return_data = $displayStatus[$module][$status];
    } else {
        $return_data = $displayStatus[$module];
    }
    return $return_data;

}


function getBreadcrumbs($parameters, $title, $url = null)
{
    if ($url) {
        $img = $url;
    } else {
        $img = '/plugins/images/heading-title-bg.jpg';
    }
    $html = '<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> ' . $title . '</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">';
    foreach ($parameters as $key => $value) {
        if (is_array($value)) {

            $route = $key;
            $name = '';
            $options = array();

            foreach ($value as $akey => $avalue) {
                if ($akey == 'name') {
                    $name = $avalue;
                } else {
                    $options = $avalue;
                }
            }

            $url = route($route, $options);

            if (!next($parameters)) {
                $html .= '<li class="active">' . $name . '</li>';

            } else {
                $html .= '<li><a href=' . $url . '>' . $name . '</a></li>';
            }

        } else {
            if (!next($parameters)) {
                $html .= '<li class="active">' . $value . '</li>';
            } else {
                $url = route($key);
                $html .= '<li><a href=' . $url . '>' . $value . '</a></li>';
            }
        }
    }
    $html .= '</ol>
                </div>
            </div>
        </div>
    </div>';
    return $html;
}

function getRatingsBasedOnGroup($ratings, $rateParameter)
{
    $count = 0;
    foreach ($ratings as $rating) {
        if ($rating->review_rating == $rateParameter) {
            $count = $rating->rating_count;
        }
    }
    return $count;
}

function getInvoiceNumber($invNumber)
{
    $now = \Carbon\Carbon::now();
    if ($now->month <= 3) {
        $now->subYear()->year;
        $start_year = $now->format('y');
        $now->addYear()->year;
        $end_year = $now->format('y');
    } else {
        $now->year;
        $start_year = $now->format('y');
        $now->addYear()->year;
        $end_year = $now->format('y');
    }

    switch (strlen($invNumber)) {
        case '1':
            $invBaseNumber = '000';
            break;
        case '2':
            $invBaseNumber = '00';
            break;
        case '3':
            $invBaseNumber = '0';
            break;
        case '4':
            $invBaseNumber = '';
            break;
        default:
            $invBaseNumber = '';
            break;
    }


    $set_invNumber = "SASAYA/" . $start_year . "-" . $end_year . "/" . $invBaseNumber . $invNumber;
    return $set_invNumber;
}

function g_print($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

