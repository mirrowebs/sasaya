<?php

namespace App\Http\Controllers\Backend;

use App\Models\Blogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class BlogsController extends Controller
{

    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = Blogs::findOrFail($requestData['blog_id']);

            if ($file->blog_img != '') {
                File::delete('uploads/blogs/' . $file->blog_img);
                File::delete('uploads/blogs/thumbs/' . $file->blog_img);
            }
            Blogs::destroy($requestData['blog_id']);
            return redirect()->route('blogs')->with('flash_message', 'Blog deleted successfully!');
        } else {
            $blogs = Blogs::orderBy('blog_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'blogs';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'Blogs';
        $data['blogs'] = $blogs;
        return view('backend.blogs.list', $data);
    }

    public function addNewBlogs(Request $request, $id = null)
    {
        $tableInfo = new Blogs();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['blog_id'] != '') {
                $blogs = Blogs::findOrFail($requestData['blog_id']);
                $imageRule = empty($blogs->blog_img) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'blog_bc_id' => 'required',
                'blog_name' => 'required',
                'blog_img' => $imageRule,
                'blog_status' => 'required'
            ], [
                'blog_bc_id.required' => 'Please select category',
                'blog_name.required' => 'Please enter name',
                'blog_img.required' => 'select image to upload',
                'blog_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('blog_img')) {
                $uploadPath = public_path('/uploads/blogs/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['blog_img']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/blogs/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['blog_img']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['blog_img']->move($uploadPath, $fileName);
                $requestData['blog_img'] = $fileName;
            }
            $requestData['blog_alias'] = str_slug($requestData['blog_name'], '-');

            if ($requestData['blog_id'] == '') {

                Blogs::create($requestData);

                $mes = 'Banner added successfully!';
            } else {

                if ($blogs->blog_img != '' && $fileName != '') {

                    File::delete('uploads/blogs/' . $blogs->blog_img);
                    File::delete('uploads/blogs/' . $blogs->blog_img);
                }

                $blogs->update($requestData);
                $mes = 'Blog updated successfully!';
            }
            return redirect()->route('blogs')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['blog_id'] = '';
            $data['blog'] = '';
            if ($id) {
                $data['blog_id'] = $id;
                $data['blog'] = Blogs::findOrFail($id);
            }
            $data['active_menu'] = 'blogs';
            $data['sub_active_menu'] = 'manage-blogs';
            $data['title'] = 'Manage blogs';
            return view('backend.blogs.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = Blogs::findOrFail($request['image']);

        File::delete('uploads/blogs/' . $image->blog_img);
        File::delete('uploads/blogs/thumbs/' . $image->blog_img);

        $update_data = array();
        $update_data['blog_img'] = '';
        $image->update($update_data);
        exit();
    }
}
