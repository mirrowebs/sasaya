<?php

namespace App\Http\Controllers\Backend;

use App\Models\Offers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class OffersController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = Offers::findOrFail($requestData['o_id']);

            if ($file->o_image != '') {
                File::delete('uploads/offers/' . $file->o_image);
                File::delete('uploads/offers/thumbs/' . $file->o_image);
            }
            Offers::destroy($requestData['o_id']);
            return redirect()->route('offers')->with('flash_message', 'Offer deleted successfully!');
        } else {
            $offers = Offers::orderBy('o_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'offers';
        $data['sub_active_menu'] = 'offers-list';
        $data['title'] = 'Offers';
        $data['offers'] = $offers;
        return view('backend.offers.list', $data);
    }

    public function addNewOffers(Request $request, $id = null)
    {
        $tableInfo = new Offers();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['o_id'] != '') {
                $offers = Offers::findOrFail($requestData['o_id']);
                $imageRule = empty($offers->o_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'o_title' => 'required',
                'o_video_link' => 'required',
                'o_image' => $imageRule,
                'o_status' => 'required',
                'o_discount' => 'required',
                'o_title_link' => 'required'
            ], [
                'o_title.required' => 'Please enter title',
                'o_video_link.required' => 'Please enter link',
                'o_image.required' => 'select image to upload',
                'o_status.required' => 'Select status',
                'o_discount.required' => 'Please enter discount',
                'o_title_link.required' => 'Please enter link',
            ]);

            $fileName = '';
            if ($request->hasFile('o_image')) {
                $uploadPath = public_path('/uploads/offers/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['o_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/offers/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['o_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['o_image']->move($uploadPath, $fileName);
                $requestData['o_image'] = $fileName;
            }

            if ($requestData['o_id'] == '') {

                Offers::create($requestData);

                $mes = 'Offer added successfully!';
            } else {

                if ($offers->o_image != '' && $fileName != '') {

                    File::delete('uploads/offers/' . $offers->o_image);
                    File::delete('uploads/offers/thumbs/' . $offers->o_image);
                }

                $offers->update($requestData);
                $mes = 'Offer updated successfully!';
            }
            return redirect()->route('offers')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['o_id'] = '';
            $data['offer'] = '';
            if ($id) {
                $data['o_id'] = $id;
                $data['offer'] = Offers::findOrFail($id);
            }
            $data['active_menu'] = 'offers';
            $data['sub_active_menu'] = 'manage-offers';
            $data['title'] = 'Manage offers';
            return view('backend.offers.add', $data);
        }
    }

    public function deleteOfferimage(Request $request)
    {
        $image = Offers::findOrFail($request['image']);

        File::delete('uploads/offers/' . $image->o_image);
        File::delete('uploads/offers/thumbs/' . $image->o_image);

        $update_data = array();
        $update_data['o_image'] = '';
        $image->update($update_data);
        exit();
    }
}
