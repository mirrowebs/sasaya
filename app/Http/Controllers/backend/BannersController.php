<?php

namespace App\Http\Controllers\Backend;

use App\Models\Banners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class BannersController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = Banners::findOrFail($requestData['banner_id']);

            if ($file->banner_image != '') {
                File::delete('uploads/banners/' . $file->banner_image);
                File::delete('uploads/banners/thumbs/' . $file->banner_image);
            }
            Banners::destroy($requestData['banner_id']);
            return redirect()->route('banners')->with('flash_message', 'Banner deleted successfully!');
        } else {
            $banners = Banners::orderBy('banner_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'banners';
        $data['sub_active_menu'] = 'banners-list';
        $data['title'] = 'Banners';
        $data['banners'] = $banners;
        return view('backend.banners.list', $data);
    }

    public function addNewBanners(Request $request, $id = null)
    {
        $tableInfo = new Banners();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['banner_id'] != '') {
                $banners = Banners::findOrFail($requestData['banner_id']);
                $imageRule = empty($banners->banner_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'banner_title' => 'required',
                'banner_link' => 'required',
                'banner_image' => $imageRule,
                'banner_status' => 'required'
            ], [
                'banner_title.required' => 'Please enter title',
                'banner_link.required' => 'Please enter link',
                'banner_image.required' => 'select image to upload',
                'banner_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('banner_image')) {
                $uploadPath = public_path('/uploads/banners/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['banner_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/banners/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['banner_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['banner_image']->move($uploadPath, $fileName);
                $requestData['banner_image'] = $fileName;
            }

            if ($requestData['banner_id'] == '') {

                Banners::create($requestData);

                $mes = 'Banner added successfully!';
            } else {

                if ($banners->banner_image != '' && $fileName != '') {

                    File::delete('uploads/banners/' . $banners->banner_image);
                    File::delete('uploads/banners/thumbs/' . $banners->banner_image);
                }

                $banners->update($requestData);
                $mes = 'Banner updated successfully!';
            }
            return redirect()->route('banners')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['banner_id'] = '';
            $data['banner'] = '';
            if ($id) {
                $data['banner_id'] = $id;
                $data['banner'] = Banners::findOrFail($id);
            }
            $data['active_menu'] = 'banners';
            $data['sub_active_menu'] = 'manage-banners';
            $data['title'] = 'Manage banners';
            return view('backend.banners.add', $data);
        }
    }

    public function deleteBannerimage(Request $request)
    {
        $image = Banners::findOrFail($request['image']);

        File::delete('uploads/banners/' . $image->banner_image);
        File::delete('uploads/banners/thumbs/' . $image->banner_image);

        $update_data = array();
        $update_data['banner_image'] = '';
        $image->update($update_data);
        exit();
    }
}
