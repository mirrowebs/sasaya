<?php

namespace App\Http\Controllers\Backend;

use App\Models\BlogCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class BlogCategoriesController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = BlogCategories::findOrFail($requestData['bc_id']);

            if ($file->bc_img != '') {
                File::delete('uploads/blogs/categories/' . $file->bc_img);
                File::delete('uploads/blogs/categories/thumbs/' . $file->bc_img);
            }
            BlogCategories::destroy($requestData['bc_id']);
            return redirect()->route('blogCategories')->with('flash_message', 'Category deleted successfully!');
        } else {
            $categories = BlogCategories::orderBy('bc_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'blog-categories';
        $data['sub_active_menu'] = 'blog-categories-list';
        $data['title'] = 'Categories';
        $data['categories'] = $categories;
        return view('backend.blogs.categories.list', $data);
    }

    public function addNewCategories(Request $request, $id = null)
    {
        $tableInfo = new BlogCategories();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['bc_id'] != '') {
                $categories = BlogCategories::findOrFail($requestData['bc_id']);
                $imageRule = empty($categories->bc_img) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'bc_name' => 'required',
                'bc_img' => $imageRule,
                'bc_status' => 'required'
            ], [
                'bc_name.required' => 'Please enter name',
                'bc_img.required' => 'select image to upload',
                'bc_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('bc_img')) {
                $uploadPath = public_path('/uploads/blogs/categories/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['bc_img']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/blogs/categories/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['bc_img']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['bc_img']->move($uploadPath, $fileName);
                $requestData['bc_img'] = $fileName;
            }
            $requestData['bc_alias'] = str_slug($requestData['bc_name'], '-');

            if ($requestData['bc_id'] == '') {

                BlogCategories::create($requestData);

                $mes = 'Banner added successfully!';
            } else {

                if ($categories->bc_img != '' && $fileName != '') {

                    File::delete('uploads/blogs/categories/' . $categories->bc_img);
                    File::delete('uploads/blogs/categories/thumbs/' . $categories->bc_img);
                }

                $categories->update($requestData);
                $mes = 'Category updated successfully!';
            }
            return redirect()->route('blogCategories')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['bc_id'] = '';
            $data['category'] = '';
            if ($id) {
                $data['bc_id'] = $id;
                $data['category'] = BlogCategories::findOrFail($id);
            }
            $data['active_menu'] = 'blog-categories';
            $data['sub_active_menu'] = 'blog-manage-categories';
            $data['title'] = 'Manage categories';
            return view('backend.blogs.categories.add', $data);
        }
    }

    public function deleteCategoryimage(Request $request)
    {
        $image = BlogCategories::findOrFail($request['image']);

        File::delete('uploads/blogs/categories/' . $image->bc_img);
        File::delete('uploads/blogs/categories/thumbs/' . $image->bc_img);

        $update_data = array();
        $update_data['bc_img'] = '';
        $image->update($update_data);
        exit();
    }
}
