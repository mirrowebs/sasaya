<?php

namespace App\Http\Controllers\Backend;

use App\Models\Orders;
use App\Models\UserAddress;
use App\Models\Wishlist;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {

        $users = User::where('id', '!=', Auth::id())->orderBy('id', 'desc')
            ->paginate($this->pagelimit);

        $data = array();
        $data['active_menu'] = 'users';
        $data['sub_active_menu'] = 'users-list';
        $data['title'] = 'Users';
        $data['users'] = $users;
        return view('backend.users.list', $data);
    }
    public function userAddress($id)
    {

        $userAddress = UserAddress::where('ua_user_id', $id)->orderBy('ua_id', 'desc')
            ->paginate($this->pagelimit);

        $data = array();
        $data['active_menu'] = 'users';
        $data['userInfo'] =User::where('id', $id)->first();
        $data['sub_active_menu'] = 'users-address';
        $data['title'] = 'Users Address';
        $data['userAddress'] = $userAddress;
        return view('backend.users.address', $data);
    }
    public function userWishlist($id)
    {

        $wishlist =Wishlist::with('getProduct.productImages')->where('wishlist_user_id', $id)->orderBy('wishlist_id', 'desc')->paginate(PAGE_LIMIT);

        $data = array();
        $data['active_menu'] = 'users';
        $data['userInfo'] =User::where('id', $id)->first();
        $data['sub_active_menu'] = 'users-wishlist';
        $data['title'] = 'Users Orders';
        $data['wishlist'] = $wishlist;
        return view('backend.users.wishlist', $data);
    }
    public function userOrders($id)
    {
        $orders = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', $id)->orderBy('order_id', 'desc')->paginate(PAGE_LIMIT);

        $data = array();
        $data['active_menu'] = 'users';
        $data['userInfo'] =User::where('id', $id)->first();
        $data['sub_active_menu'] = 'users-orders';
        $data['title'] = 'Users Orders';
        $data['orders'] = $orders;
        return view('backend.users.orders', $data);
    }
    public function orders()
    {
        $orders = Orders::with('orderItems.getProduct.productImages')->orderBy('order_id', 'desc')->paginate($this->pagelimit);

        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders-list';
        $data['title'] = 'Orders';
        $data['orders'] = $orders;
        return view('backend.orders', $data);
    }

}
