<?php

namespace App\Http\Controllers\Backend;

use App\Models\Categories;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {

        $requestAll = $request->all();

//        dump($requestAll);

        $keyword = $request->get('search');
        $category = $request->get('category');

        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = Categories::findOrFail($requestData['category_id']);

            if ($file->category_image != '') {
                File::delete('uploads/categories/' . $file->category_image);
                File::delete('uploads/categories/thumbs/' . $file->category_image);
            }
            Categories::destroy($requestData['category_id']);
            return redirect()->route('categories')->with('flash_message', 'Category deleted successfully!');
        } else {
            if (!empty($keyword) && isset($category)) {
                if ($category == 'parent') {
                    $category = 0;
                }
                $categories = Categories::where('category_parent', $category)
                    ->orderBy('category_id', 'desc')
                    ->paginate($this->pagelimit);

            } else {
                $categories = Categories::orderBy('category_id', 'desc')
                    ->paginate($this->pagelimit);
            }

        }
        $data = array();
        $data['active_menu'] = 'categories';
        $data['sub_active_menu'] = 'categories-list';
        $data['title'] = 'Categories';
        $data['categories'] = $categories;
        return view('backend.categories.list', $data);
    }

    public function addNewCategories(Request $request, $id = null)
    {
        $tableInfo = new Categories();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['category_id'] != '') {
                $categories = Categories::findOrFail($requestData['category_id']);
                $imageRule = empty($categories->category_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'category_name' => 'required',
//                'category_image' => $imageRule,
                'category_status' => 'required'
            ], [
                'category_name.required' => 'Please enter name',
//                'category_image.required' => 'select image to upload',
                'category_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('category_image')) {
                $uploadPath = public_path('/uploads/categories/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['category_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/categories/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['category_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['category_image']->move($uploadPath, $fileName);
                $requestData['category_image'] = $fileName;
            }

            $requestData['category_alias'] = str_slug($requestData['category_name'], '-');
            if ($requestData['category_id'] == '') {

                Categories::create($requestData);

                $mes = 'Banner added successfully!';
            } else {

                if ($categories->category_image != '' && $fileName != '') {

                    File::delete('uploads/categories/' . $categories->category_image);
                    File::delete('uploads/categories/thumbs/' . $categories->category_image);
                }

                $categories->update($requestData);
                $mes = 'Category updated successfully!';
            }
            return redirect()->route('categories')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['category_id'] = '';
            $data['category'] = '';
            if ($id) {
                $data['category_id'] = $id;
                $data['category'] = Categories::findOrFail($id);
            }
            $data['active_menu'] = 'categories';
            $data['sub_active_menu'] = 'manage-categories';
            $data['title'] = 'Manage categories';
            return view('backend.categories.add', $data);
        }
    }

    public function deleteCategoryimage(Request $request)
    {
        $image = Categories::findOrFail($request['image']);

        File::delete('uploads/categories/' . $image->category_image);
        File::delete('uploads/categories/thumbs/' . $image->category_image);

        $update_data = array();
        $update_data['category_image'] = '';
        $image->update($update_data);
        exit();
    }
}
