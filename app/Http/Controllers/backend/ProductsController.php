<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProductImages;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ProductsController extends Controller
{
    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $productimages = ProductImages::where('pi_product_id', $requestData['p_id'])->get();

            foreach ($productimages as $image) {

                File::delete('uploads/products/' . $image->pi_image_name);
                File::delete('uploads/products/thumbs/' . $image->pi_image_name);
                ProductImages::destroy($image->pi_id);
            }

            $file = Products::findOrFail($requestData['p_id']);


            if ($file->product_cover_image != '') {
                File::delete('uploads/products/' . $file->product_cover_image);
                File::delete('uploads/products/thumbs/' . $file->product_cover_image);
            }

            Products::destroy($requestData['p_id']);

            return redirect()->route('products')->with('flash_message', 'Product deleted successfully!');
        } else {
            $products = Products::orderBy('p_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-list';
        $data['title'] = 'Products';
        $data['products'] = $products;
        return view('backend.products.list', $data);
    }

    public function addNewProducts(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {

            $tableInfo = new Products();

            $requestData = $request->all();


//            if ($requestData['product_id'] != '') {
//                $products = Products::findOrFail($requestData['product_id']);
//                $imageRule = empty($products->product_cover_image) ? 'required' : '';
//            } else {
//                $imageRule = 'required';
//            }





            $result = Products::createProduct($request);

            return redirect()->back()->with('flash_message', $result['msg_text']);


//            dd($result);



        } else {
            $data = array();
            $data['p_id'] = $id;
            $data['product'] = '';
            $data['product_images'] = array();
            if ($id) {

                $data['product'] = Products::findOrFail($id);
                $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();
            }
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'manage-products';
            $data['title'] = 'Manage products';
            return view('backend.products.add', $data);
        }
    }




    public function deleteproductcoverimage(Request $request)
    {
        $image = Products::findOrFail($request['image']);

        File::delete('uploads/products/' . $image->product_cover_image);
        File::delete('uploads/products/thumbs/' . $image->product_cover_image);

        $update_data = array();
        $update_data['product_cover_image'] = '';
        $image->update($update_data);
        exit();
    }

    public function productInfo($id)
    {
        $data = array();
        $data['product_id'] = $id;
        $data['product'] = Products::findOrFail($id);
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'manage-products';
        $data['title'] = 'Manage products';
        return view('backend.products.view', $data);
    }
}
