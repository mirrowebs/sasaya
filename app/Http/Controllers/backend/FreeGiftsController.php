<?php

namespace App\Http\Controllers\Backend;

use App\Models\FreeGifts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class FreeGiftsController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = FreeGifts::findOrFail($requestData['free_gift_id']);

            if ($file->free_gift_cover_image != '') {
                File::delete('uploads/products/' . $file->free_gift_cover_image);
                File::delete('uploads/products/thumbs/' . $file->free_gift_cover_image);
            }

            FreeGifts::destroy($requestData['free_gift_id']);
            return redirect()->route('freegifts')->with('flash_message', 'Free Gift deleted successfully!');
        } else {
            $freegifts = FreeGifts::orderBy('free_gift_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'freegifts';
        $data['sub_active_menu'] = 'freegifts-list';
        $data['title'] = 'Free gifts';
        $data['freegifts'] = $freegifts;
        return view('backend.freegifts.list', $data);
    }

    public function addNewFreeGifts(Request $request, $id = null)
    {
        $tableInfo = new FreeGifts();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['free_gift_id'] != '') {
                $freegifts = FreeGifts::findOrFail($requestData['free_gift_id']);
                $imageRule = empty($freegifts->free_gift_cover_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }


            $this->validate(request(), [
                'free_gift_name' => 'required',
                'free_gift_price' => 'required',
                'free_gift_cover_image' => $imageRule,
                'free_gift_status' => 'required'
            ], [
                'free_gift_name.required' => 'Please enter name',
                'free_gift_price.required' => 'Please enter price',
                'free_gift_cover_image.required' => 'select image to upload',
                'free_gift_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('free_gift_cover_image')) {
                $uploadPath = public_path('/uploads/products/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['free_gift_cover_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/products/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['free_gift_cover_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['free_gift_cover_image']->move($uploadPath, $fileName);
                $requestData['free_gift_cover_image'] = $fileName;
            }


            if ($requestData['free_gift_id'] == '') {

                $frid = FreeGifts::create($requestData)->free_gift_id;

                $freegifts = FreeGifts::findOrFail($frid);

                $updateData = array();
                $updateData['free_gift_uniqid'] = $frid . time();


                $freegifts->update($updateData);


                $mes = 'Free Gift added successfully!';
            } else {

                if ($freegifts->free_gift_cover_image != '' && $fileName != '') {

                    File::delete('uploads/products/' . $freegifts->free_gift_cover_image);
                    File::delete('uploads/products/thumbs/' . $freegifts->free_gift_cover_image);
                }

                $freegifts->update($requestData);
                $mes = 'Free Gift updated successfully!';
            }
            return redirect()->route('freegifts')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['free_gift_id'] = '';
            $data['freegift'] = '';
            if ($id) {
                $data['free_gift_id'] = $id;
                $data['freegift'] = FreeGifts::findOrFail($id);
            }
            $data['active_menu'] = 'freegifts';
            $data['sub_active_menu'] = 'manage-freegifts';
            $data['title'] = 'Manage freegifts';
            return view('backend.freegifts.add', $data);
        }
    }

    public function deletefreecoverimage(Request $request)
    {
        $image = FreeGifts::findOrFail($request['image']);

        File::delete('uploads/products/' . $image->free_gift_cover_image);
        File::delete('uploads/products/thumbs/' . $image->free_gift_cover_image);

        $update_data = array();
        $update_data['free_gift_cover_image'] = '';
        $image->update($update_data);
        exit();
    }


}
