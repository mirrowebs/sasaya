<?php

namespace App\Http\Controllers\Backend;

use App\Models\Countries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    public $pagelimit;
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            Countries::destroy($requestData['country_id']);
            return redirect()->route('countries')->with('flash_message', 'Country deleted successfully!');
        } else {
            $countries = Countries::orderBy('country_id', 'desc')
                ->paginate($this->pagelimit);
        }
        $data = array();
        $data['active_menu'] = 'countries';
        $data['sub_active_menu'] = 'countries-list';
        $data['title'] = 'Countries';
        $data['countries'] = $countries;
        return view('backend.countries.list', $data);
    }

    public function addNewCountries(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $this->validate(request(), [
                'country_name' => 'required',
                'country_code' => 'required',
                'country_phone_prefix' => 'required',
                'country_currency' => 'required',
                'country_language' => 'required',
                'country_status' => 'required'
            ], [
                'country_name.required' => 'Please enter name',
                'country_code.required' => 'Please enter code',
                'country_phone_prefix.required' => 'Please enter prefix',
                'country_currency.required' => 'Please enter currency',
                'country_language.required' => 'Please enter language',
                'country_status.required' => 'Select status',
            ]);

            if ($requestData['country_id'] == '') {
                Countries::create($requestData);
                $mes = 'Country added successfully!';
            } else {
                $countries = Countries::findOrFail($requestData['country_id']);
                $countries->update($requestData);
                $mes = 'Country updated successfully!';
            }
            return redirect()->route('countries')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['country_id'] = '';
            $data['country'] = '';
            if ($id) {
                $data['country_id'] = $id;
                $data['country'] = Countries::findOrFail($id);
            }
            $data['active_menu'] = 'countries';
            $data['sub_active_menu'] = 'manage-countries';
            $data['title'] = 'Manage Countries';
            return view('backend.countries.add', $data);
        }
    }
}
