<?php

namespace App\Http\Controllers\Frontend;

use App\Models\FreeGifts;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Payments;
use App\Models\Reviews;
use App\Models\UserAddress;
use App\Models\Wishlist;
use App\Models\User;
use Indipay;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
//        exit();
            $requestData = $request->all();

            $tableInfo = new User();
            $this->validate(request(), [
                'name' => 'required',
                'email' => ['required', Rule::unique($tableInfo->getTable())->ignore(Auth::id(), 'id')],
                'mobile' => 'required',
                'gender' => 'required',
                'dob' => 'required',
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email',
                'mobile.required' => 'Enter mobile',
                'gender.required' => 'Select Gender',
                'dob.required' => 'Enter DOB',
            ]);
            $fileName = '';
            if ($request->hasFile('image')) {
                $uploadPath = public_path('/uploads/users/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/users/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['image']->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
            }

            $requestData['dob'] = Carbon::createFromFormat('d-m-Y', $requestData['dob'])->toDateString();
            $users = User::findOrFail(Auth::id());
            $users->update($requestData);
            $mes = 'User updated successfully!';
            return redirect()->route('userprofile')->with('flash_message', $mes);
        } else {
            $data = array();
            $data['active_menu'] = 'profile';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'User Profile';
            $data['user'] = User::where('id', Auth::id())->first();
            return view('frontend.user.profile', $data);
        }
    }

    public function addToWishList(Request $request)
    {
        $requestData = $request->all();

        $wishlist = array();
        $wishlist['wishlist_product_id'] = $requestData['id'];
        $wishlist['wishlist_user_id'] = Auth::id();

        $checkExisting = Wishlist::where('wishlist_product_id', $requestData['id'])->where('wishlist_user_id', Auth::id())->first();
        if (!empty($checkExisting)) {
            Wishlist::destroy($checkExisting->wishlist_id);
            echo 2;
        } else {
            $id = Wishlist::create($wishlist)->wishlist_id;
            if ($id) {
                echo 1;
            } else {
                echo 0;
            }
        }

    }

    public function wishList(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            Wishlist::destroy($requestData['wishlist_id']);
            return redirect()->route('wishList')->with('flash_message', 'Wishlist Product deleted successfully!');
        }
        $data = array();
        $data['active_menu'] = 'wishList';
        $data['sub_active_menu'] = 'wishList';
        $data['title'] = 'Wish List';
        $data['wishlistProducts'] = Wishlist::with('getProduct')->where('wishlist_user_id', Auth::id())->get();
        return view('frontend.user.mywishlist', $data);
    }

    public function deleteUserimage()
    {
        $image = User::findOrFail(Auth::id());

        File::delete('uploads/users/' . $image->image);
        File::delete('uploads/users/thumbs/' . $image->image);

        $update_data = array();
        $update_data['image'] = '';
        $image->update($update_data);
        exit();
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            g_print($requestData);
//            exit();

            $this->validate(request(), [
                'password' => 'required',
                'new_password' => 'required|string|min:6|max:12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@^&*()~]).*$/',
                'confirm_password' => 'required|same:new_password'
            ], [
                'password.required' => 'Please enter old password',
                'new_password.required' => 'Please enter new password',
                'new_password.regex' => 'Passwords must have at least 6 characters and contains atleast one uppercase letter, lowercase letter, number, and specl symbols',
                'new_password.min' => 'Password number contains minimum 6 digits',
                'new_password.max' => 'Password number contains maximum 12 digits',
                'confirm_password.required' => 'Please confirm password'
            ]);
            $id = Auth::id();
            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('userChangePassword')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('userChangePassword')->with('flash_message_error', $mes);
            }

        } else {
            $data = array();
            $data['active_menu'] = 'changePassword';
            $data['sub_active_menu'] = 'changePassword';
            $data['title'] = 'Change Password';
            return view('frontend.user.changePassword', $data);
        }
    }

    public function userAddressBook(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            UserAddress::destroy($requestData['ua_id']);

            $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->get();
            $userDefaultPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->where('ua_defult', 1)->get();

            if (count($userDefaultPrimaryAddressInfo) == 0 && count($userPrimaryAddressInfo) > 0) {
                if ($userPrimaryAddressInfo[0]->ua_defult == 0) {
                    $updateData = array();
                    $updateData['ua_defult'] = 1;
                    $userAddress = UserAddress::findOrFail($userPrimaryAddressInfo[0]->ua_id);
                    $userAddress->update($updateData);
                }
            }

            if ($requestData['flag']) {
                return redirect()->route('deliveryAddress')->with('flash_message', 'Address deleted successfully!');
            } else {
                return redirect()->route('userAddressBook')->with('flash_message', 'Address deleted successfully!');
            }
        }
        $data = array();
        $data['active_menu'] = 'userAddressBook';
        $data['sub_active_menu'] = 'userAddressBook';
        $data['title'] = 'User Address Book';
        $data['userAddresses'] = UserAddress::where('ua_user_id', Auth::id())->get();
        return view('frontend.user.userAddressBook', $data);
    }

    public function userAddAddressBook(Request $request)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $this->validate(request(), [
                'ua_name' => 'required',
                'ua_address' => 'required',
                'ua_landmark' => 'required',
                'ua_city' => 'required',
                'ua_state' => 'required',
                'ua_email' => 'required',
                'ua_phone' => 'required',
            ], [
                'ua_name.required' => 'Please enter name',
                'ua_address.required' => 'Please enter address',
                'ua_landmark.required' => 'Please enter landmark',
                'ua_city.required' => 'Please enter city',
                'ua_state.required' => 'Please enter state',
                'ua_email.required' => 'Please enter email',
                'ua_phone.required' => 'Please enter phone',
            ]);

            $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->first();

            if (!$requestData['flag']) {
                if (isset($requestData['ua_defult']) && $requestData['ua_defult'] == 1) {
                    UserAddress::where('ua_user_id', Auth::id())->update(['ua_defult' => 0]);
                } else {
                    $requestData['ua_defult'] = 0;
                }
            }

            if (empty($userPrimaryAddressInfo)) {
                $requestData['ua_defult'] = 1;
            }
            if ($requestData['ua_id'] == '') {
                $requestData['ua_user_id'] = Auth::id();
                $requestData['ua_status'] = 'active';

                UserAddress::create($requestData);
                $mes = 'Address added successfully!';
            } else {


                $userAddress = UserAddress::findOrFail($requestData['ua_id']);
                $userAddress->update($requestData);

                $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->get();
                $userDefaultPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->where('ua_defult', 1)->get();

                if (count($userDefaultPrimaryAddressInfo) == 0 && count($userPrimaryAddressInfo) > 0) {
                    if ($userPrimaryAddressInfo[0]->ua_defult == 0) {
                        $updateData = array();
                        $updateData['ua_defult'] = 1;
                        $userAddress = UserAddress::findOrFail($userPrimaryAddressInfo[0]->ua_id);
                        $userAddress->update($updateData);
                    }
                }
                $mes = 'Address updated successfully!';
            }
            if ($requestData['flag']) {
                return redirect()->route('deliveryAddress')->with('flash_message', $mes);
            } else {
                return redirect()->route('userAddressBook')->with('flash_message', $mes);
            }

        }
    }

    public function makeDefaultAddress(Request $request)
    {
        $requestData = $request->all();

        UserAddress::where('ua_user_id', Auth::id())->update(['ua_defult' => 0]);

        $userAddress = UserAddress::findOrFail($requestData['ua_id']);
        $userAddress->update($requestData);
        $mes = 'Address updated successfully!';

        if ($requestData['flag'] == 1) {
            return redirect()->route('deliveryAddress')->with('flash_message', $mes);
        } else {
            return redirect()->route('userAddressBook')->with('flash_message', $mes);
        }

    }

    public function deliveryAddress()
    {
        if (Cart::getContent()->count()) {
            $data = array();
            $data['active_menu'] = 'deliveryAddress';
            $data['sub_active_menu'] = 'deliveryAddress';
            $data['title'] = 'Select Delivery Address';
            $data['userAddresses'] = UserAddress::where('ua_user_id', Auth::id())->get();
            return view('frontend.user.deliveryAddress', $data);
        } else {
            return redirect()->route('home');
        }

    }


    public function addfreeGiftToCart(Request $request)
    {
        $requestData = $request->all();


        $items = Cart::getContent();
        if (Cart::getContent()->count()) {
            foreach ($items as $item) {
                if ($item->price == 0) {
                    Cart::remove($item->id);
                }

            }
        }

        $productInfo = FreeGifts::where('free_gift_uniqid', $requestData['id'])->first();


        Cart::add(array(
            array(
                'id' => $requestData['id'],
                'name' => $productInfo->free_gift_name,
                'price' => 0,
                'quantity' => $requestData['qty'],
                'attributes' => array('description' => $productInfo->free_gift_description)
            ),
        ));

        echo Cart::getContent()->count();
    }


    public function paymentConfirmation(Request $request)
    {
        if (Cart::getContent()->count()) {
            $requestData = $request->all();

            session(['paymentaddressid' => $requestData['address_id']]);
            $products = array();
            if (getPaymentRanges(Cart::getSubTotal())) {
                $products = FreeGifts::where('free_gift_status', 'active')->where('free_gift_price', getPaymentRanges(Cart::getSubTotal()))->get();
            }


            $data = array();
            $data['active_menu'] = 'paymentConfirmation';
            $data['freegifts'] = $products;
            $data['sub_active_menu'] = 'paymentConfirmation ';
            $data['title'] = 'payment Confirmation';
            return view('frontend.user.paymentConfirmation ', $data);
        } else {
            return redirect()->route('home');
        }

    }

    public function saveOrders()
    {


        $userAddress = UserAddress::with('getCountry')->findOrFail(session('paymentaddressid'));

        $userAdd = array();
        $userAdd['ua_name'] = $userAddress->ua_name;
        $userAdd['ua_address'] = $userAddress->ua_address;
        $userAdd['ua_landmark'] = $userAddress->ua_landmark;
        $userAdd['ua_city'] = $userAddress->ua_city;
        $userAdd['ua_state'] = $userAddress->ua_state;
        $userAdd['ua_email'] = $userAddress->ua_email;
        $userAdd['ua_phone'] = $userAddress->ua_phone;
        $userAdd['ua_pincode'] = $userAddress->ua_pincode;
        $userAdd['ua_country'] = $userAddress->getCountry->country_name;
        $orders = array();
        $orders['order_user_id'] = Auth::id();
        $orders['order_delivery_address'] = serialize($userAdd);
        $orders['order_status'] = 'Pending';
        $orders['order_sub_total_price'] = Cart::getSubTotal();
        $orders['order_shipping_price'] = paymentDeliveryCharges(Cart::getSubTotal());
        $orders['order_total_price'] = str_replace(',', '', Cart::getTotal());
        $orders['order_reference_number'] = md5(time() . rand());
        $orders['order_delivery_expected_date'] = Carbon::createFromFormat('d-m-Y', date('d-m-Y'))->addDays('5')->toDateString();
        $order_id = Orders::create($orders)->order_id;
        foreach (Cart::getContent() as $row) {
            $orderItems = array();
            $orderItems['oitem_order_id'] = $order_id;
            $orderItems['oitem_product_id'] = $row->id;

            if ($row->price == 0) {
                $orderItems['oitem_is_freegift'] = 1;
            } else {
                $orderItems['oitem_is_freegift'] = 0;
            }
            $orderItems['oitem_product_name'] = $row->name;
            $orderItems['oitem_qty'] = str_replace(',', '', $row->quantity);
            $orderItems['oitem_product_price'] = str_replace(',', '', $row->price);
            $orderItems['oitem_sub_total'] = str_replace(',', '', $row->getPriceSum());
            OrderItems::create($orderItems);
        }


        $parameters = [

            'tid' => $order_id . '' . Auth::id(),

            'order_id' => $order_id,

            'amount' => $orders['order_total_price'],

        ];

        $order = Indipay::prepare($parameters);

        Cart::clear();
        session()->forget(['paymentaddressid']);

//        dd($order);

        return Indipay::process($order);

        exit();

        $mes = 'Your Order has been successfully submitted !';
        return redirect()->route('paymentSuccessPage')->with('flash_message', $mes);
    }

    public function paymentSuccessPage($order)
    {
        $data = array();
        $data['active_menu'] = 'paymentSuccessPage';
        $data['sub_active_menu'] = 'paymentSuccessPage ';
        $data['title'] = 'Payment Success';
        $data['latest_order'] = Orders::with('orderItems')->where('order_id', $order)->first();
        $data['latest_order_address'] = unserialize($data['latest_order']->order_delivery_address);
        return view('frontend.user.paymentSuccessPage ', $data);
    }

    public function orders()
    {
        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Orders';
        $data['pending_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Pending')->orderBy('order_id', 'desc')->get();
        $data['completed_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Completed')->orderBy('order_id', 'desc')->get();
        $data['cancel_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Cancel')->orderBy('order_id', 'desc')->get();
        $data['all_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->get();
        return view('frontend.user.orders', $data);
    }

    public function orderDetails($order)
    {
        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Order Details';
        $data['orderDetails'] = Orders::with('orderItems.getProduct.productImages')->where('order_reference_number', $order)->first();
        return view('frontend.user.orderDetails', $data);
    }

    public function orderInvoice($order)
    {
        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Order Details';
        $data['orderDetails'] = Orders::with('orderItems.getProduct.productImages')->where('order_reference_number', $order)->first();
        return view('frontend.user.orderInvoice', $data);
    }

    public function savereview(Request $request)
    {
        $return = array();
        $requestData = $request->all();
        $requestData['review_user_id'] = Auth::id();
        $validator = Validator::make($requestData,
            [
                'review_rating' => 'required'
            ],
            [
                'review_rating.required' => 'Please Select rating'
            ]
        );
        if ($validator->fails()) {
            $return['status'] = 2;
            $test = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                array_push($test, $messages[0]);// messages are retrieved (publicly)
            }
            $return['errors'] = implode('<br>', $test);
        } else {
            $cdetails = Reviews::create($requestData);
            if ($cdetails->exists) {
                $return['status'] = 1;
            } else {
                $return['status'] = 2;
                $return['errors'] = "Please enter valid data";
            }
        }
        return $return;
    }

    public function paymentResponse(Request $request)
    {
        $response = Indipay::response($request);

        $lastinvoiceidquery = Payments::where('p_invoice_number', '<>', '')->orderBy('p_id', 'DESC')->first();
        if ($lastinvoiceidquery) {
            $lastinvoiceid = $lastinvoiceidquery->p_invoice_number;
            $invoicenumget = explode("/", $lastinvoiceid);
            $invoicenumval = end($invoicenumget);
            $invoiceidincrementval = $invoicenumval + 1;
        } else {
            $invoiceidincrementval = '1';
        }

        $number = getInvoiceNumber($invoiceidincrementval);

        $requestData = array();
        $requestData['p_order_id'] = $response['order_id'];
        $requestData['p_tracking_id'] = $response['tracking_id'];
        if ($response['order_status'] == 'Success') {
            $requestData['p_invoice_number'] = $number;
        } else {
            $requestData['p_invoice_number'] = "";
        }
        $requestData['p_bank_ref_no'] = $response['bank_ref_no'];
        $requestData['p_order_status'] = $response['order_status'];
        $requestData['p_payment_mode'] = $response['payment_mode'];
        $requestData['p_card_name'] = $response['card_name'];
        $requestData['p_status_code'] = $response['status_code'];
        $requestData['p_status_message'] = $response['status_message'];
        $requestData['p_currency'] = $response['currency'];
        $requestData['p_amount'] = $response['amount'];
        $requestData['p_billing_name'] = $response['billing_name'];
        $requestData['p_billing_address'] = $response['billing_address'];
        $requestData['p_billing_city'] = $response['billing_city'];
        $requestData['p_billing_state'] = $response['billing_state'];
        $requestData['p_billing_zip'] = $response['billing_zip'];
        $requestData['p_billing_country'] = $response['billing_country'];
        $requestData['p_billing_tel'] = $response['billing_tel'];
        $requestData['p_billing_email'] = $response['billing_email'];
        $requestData['p_delivery_name'] = $response['delivery_name'];
        $requestData['p_delivery_address'] = $response['delivery_address'];
        $requestData['p_delivery_city'] = $response['delivery_city'];
        $requestData['p_delivery_state'] = $response['delivery_state'];
        $requestData['p_delivery_zip'] = $response['delivery_zip'];
        $requestData['p_delivery_country'] = $response['delivery_country'];
        $requestData['p_delivery_tel'] = $response['delivery_tel'];
        $requestData['p_country'] = $response['bin_country'];
        $requestData['p_failure_message'] = $response['failure_message'];
        $orderresult = Payments::create($requestData);

        $orderInfo = Orders::findOrFail($response['order_id']);

        $data = array();
        $data['order_status'] = $response['order_status'];
        $orderInfo->update($data);

        return redirect()->route('paymentSuccessPage', ['id' => $orderInfo->order_id]);
    }

}
