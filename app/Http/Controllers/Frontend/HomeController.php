<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Banners;
use App\Models\BlogCategories;
use App\Models\Blogs;
use App\Models\Categories;
use App\Models\ContactUs;
use App\Models\NewsLetters;
use App\Models\Offers;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\Reviews;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public $pagelimit;

    public function __construct()
    {
        $this->pagelimit = 30;
    }

    public function index(Request $request)
    {

        $response = $request;
        $response->header('X-Frame-Options', 'ALLOW FROM https://example.com/');


        $data = array();
        $data['banners'] = Banners::where('banner_status', 'active')->take(5)->get();
        $data['title'] = 'Sasaya Online E-Commerce store';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['wishlistProducts'] = getWishlistProducts();
        $data['offers'] = Offers::where('o_status', 'active')->orderBy('o_id', 'desc')->get();


        $data['products_lists'] = Products::where('p_status', 'active')->orderBy('p_id', 'desc')->get();






        return view('frontend.home', $data);
    }

    public function addToCart(Request $request)
    {
        $requestData = $request->all();

        $productInfo = Products::where('p_id', $requestData['id'])->first();


        Cart::add(array(
            array(
                'id' => $requestData['id'],
                'name' => $productInfo->p_name,
                'price' => $productInfo->p_sel_price,
                'quantity' => $requestData['qty'],
                'attributes' => array('color' => $productInfo->p_color, 'description' => $productInfo->p_description)
            ),
        ));

        $linkedproducts = explode(',', $requestData['linkedproducts']);
        if (count($linkedproducts) > 0 && $requestData['linkedproducts'] != '') {
            foreach ($linkedproducts as $products) {
                $productInfo = Products::where('p_id', $products)->first();


                Cart::add(array(
                    array(
                        'id' => $products,
                        'name' => $productInfo->p_name,
                        'price' => $productInfo->p_sel_price,
                        'quantity' => 1,
                        'attributes' => array('color' => $productInfo->p_color, 'description' => $productInfo->p_description)
                    ),
                ));
            }
        }

        $deliveryCharge = paymentDeliveryCharges(Cart::getSubTotal());

        $condition = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'Shipping Rs.' . $deliveryCharge,
            'type' => 'shipping',
            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => '+' . $deliveryCharge,
            'order' => 1
        ));
        Cart::condition($condition);


        echo Cart::getContent()->count();
    }

    public function updateItemFromCart(Request $request)
    {

        $requestData = $request->all();

        if($requestData['qty'] == 0){
            Cart::remove($requestData['id']);

            Cart::clearCartConditions();
        }else {
            Cart::update($requestData['id'], array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $requestData['qty']
                ),
            ));

        }


    }

    public function removeItemFromCart(Request $request)
    {
        $requestData = $request->all();

        Cart::remove($requestData['id']);

        Cart::clearCartConditions();

    }

    public function cartDetails()
    {
        $data = array();
        $data['title'] = 'Sasaya- cart Details';
        $data['active_menu'] = 'cartDetails';
        $data['sub_active_menu'] = 'cartDetails';
        $data['wishlistProducts'] = getWishlistProducts();
        return view('frontend.cartDetails', $data);
    }

    public function contactUs(Request $request)
    {
        if ($request->isMethod('post')) {
            $return = array();
            $requestData = $request->all();
            $validator = Validator::make($requestData,
                [
                    'name' => 'required',
                    'phone' => 'required',
                    'email' => 'required|email'
                ],
                [
                    'name.required' => 'Please enter email',
                    'phone.required' => 'Please enter phone',
                    'email.required' => 'Please enter email',
                    'email.email' => 'Please enter valid email'
                ]
            );
            if ($validator->fails()) {
                $return['status'] = 2;
                $test = array();
                foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                    array_push($test, $messages[0]);// messages are retrieved (publicly)
                }
                $return['errors'] = implode('<br>', $test);
            } else {
                $requestData['ip'] = $request->getClientIp();
                $cdetails = ContactUs::create($requestData);
                if ($cdetails->exists) {
                    $return['status'] = 1;
                } else {
                    $return['status'] = 2;
                    $return['errors'] = "Please enter valid data";
                }
            }
            return $return;
        } else {
            $data = array();
            $data['title'] = 'sasaya- Support';
            $data['active_menu'] = 'support';
            $data['sub_active_menu'] = 'support';
            return view('frontend.contact', $data);
        }

    }

    public function productslist(Request $request, $category = null, $subcategory = null)
    {

        $pricelow = '';
        $fresharraivals = '';
        $offerproducts = '';
        $popular = 'desc';
        $search = '';
        if ($request->isMethod('post')) {
            $popular = '';
            $requestData = $request->all();
            if ($requestData['fliters'] == 'price_low_to_high') {
                $pricelow = 'asc';
            } elseif ($requestData['fliters'] == 'price_high_to_low') {
                $pricelow = 'desc';
            } elseif ($requestData['fliters'] == 'fresh_arrivals') {
                $fresharraivals = 'desc';
            } elseif ($requestData['fliters'] == 'offer_products') {
                $offerproducts = 'desc';
            } elseif ($requestData['fliters'] == 'popular') {
                $popular = 'desc';
            }
            $search = $requestData['fliters'];
        }
        $data = array();
        $data['title'] = 'SASAYA- Products';
        $data['search'] = $search;
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products';
        $data['products_category'] = $category;
        $data['product_sub_category'] = $subcategory;
        $data['wishlistProducts'] = getWishlistProducts();
        $data['categoryInfo'] = Categories::where('category_alias', $category)->first();
        if ($subcategory == '') {
            $data['brandInfo'] = array();
            $data['products'] = Products::
            join('categories', 'categories.category_id', '=', 'products.p_category')
                ->when($pricelow, function ($query) use ($pricelow) {
                    return $query->orderBy('p_price', $pricelow);
                })
                ->when($fresharraivals, function ($query) use ($fresharraivals) {
                    return $query->orderBy('p_id', $fresharraivals);
                })
                ->when($popular, function ($query) use ($popular) {
                    return $query->orderBy('p_id', $popular);
                })
                ->where('p_category', $data['categoryInfo']->category_id)
                ->where('p_status', 'active')
                ->get();
        } else {
            $data['subcategoryinfo'] = Categories::where('category_alias', $subcategory)->first();
            $data['products'] = Products::
            join('categories', 'categories.category_id', '=', 'products.p_category')
                ->when($pricelow, function ($query) use ($pricelow) {
                    return $query->orderBy('p_price', $pricelow);
                })
                ->when($fresharraivals, function ($query) use ($fresharraivals) {
                    return $query->orderBy('p_id', $fresharraivals);
                })
                ->when($popular, function ($query) use ($popular) {
                    return $query->orderBy('p_id', $popular);
                })
                ->where('p_category', $data['categoryInfo']->category_id)
                ->where('p_sub_category', $data['subcategoryinfo']->category_id)
                ->where('p_status', 'active')
                ->paginate(1);
        }
        return view('frontend.productslist', $data);
    }

    public function productDetails($product_alias)
    {
        $data = array();
        $data['title'] = 'Sasaya- product Details';
        $data['active_menu'] = 'productDetails';
        $data['sub_active_menu'] = 'productDetails';
        $data['wishlistProducts'] = getWishlistProducts();
        $data['product'] = Products::where('p_alias', $product_alias)->first();
        $data['overall_rating'] = Reviews::where('review_status', 'active')->get();
        $data['ratings'] = Reviews::groupBy('review_rating')
            ->select(DB::raw('review_rating , COUNT(*) as rating_count'))
            ->get();
        $data['related_products'] = Products::where('p_category', $data['product']->p_category)->whereNotIn('p_id', [$data['product']->p_id])->take(4)->inRandomOrder()->get();
        $data['productImages'] = ProductImages::where('pi_product_id', $data['product']->p_id)->orderBy('pi_id', 'desc')->get();
        return view('frontend.productDetails', $data);
    }

    public function aboutUs()
    {
        $data = array();
        $data['title'] = 'Sasaya- ABOUTUS';
        $data['active_menu'] = 'aboutus';
        $data['sub_active_menu'] = 'aboutus';
        return view('frontend.aboutus', $data);
    }

    public function blog($blog_category = null, $blog_alias = null)
    {
        if ($blog_alias) {
            $data = array();
            $data['title'] = 'Sasaya- blog';
            $data['active_menu'] = 'blog';
            $data['sub_active_menu'] = 'blog';
            $data['blog'] = Blogs::where('blog_alias', $blog_alias)->first();
            return view('frontend.blogDetails', $data);
        } else {
            $data = array();
            $data['title'] = 'Sasaya- blog';
            $data['active_menu'] = 'blog';
            $data['sub_active_menu'] = 'blog';
            if ($blog_category) {
                $blogcat = BlogCategories::where('bc_alias', $blog_category)->first();
                $data['blogs'] = Blogs::where('blog_bc_id', $blogcat->bc_id)->where('blog_status', 'active')->paginate($this->pagelimit);
            } else {
                $data['blogs'] = Blogs::where('blog_status', 'active')->paginate($this->pagelimit);
            }
            return view('frontend.blog', $data);
        }

    }

    public function faqs()
    {
        $data = array();
        $data['title'] = 'Sasaya- faqs';
        $data['active_menu'] = 'faqs';
        $data['sub_active_menu'] = 'faqs';
        return view('frontend.faqs', $data);
    }

    public function termsAndConditions()
    {
        $data = array();
        $data['title'] = 'Sasaya- Terms And Conditions';
        $data['active_menu'] = 'termsAndConditions';
        $data['sub_active_menu'] = 'termsAndConditions';
        return view('frontend.termsAndConditions', $data);
    }

    public function privacyPolicy()
    {
        $data = array();
        $data['title'] = 'Sasaya- Privacy olicy';
        $data['active_menu'] = 'privacyPolicy';
        $data['sub_active_menu'] = 'privacyPolicy';
        return view('frontend.privacyPolicy', $data);
    }

    public function returnPolicy()
    {
        $data = array();
        $data['title'] = 'Sasaya- Return Policy';
        $data['active_menu'] = 'returnPolicy';
        $data['sub_active_menu'] = 'returnPolicy';
        return view('frontend.returnPolicy', $data);
    }

    public function buyProduct(Request $request)
    {
        $requestData = $request->all();



        $productInfo = Products::where('p_id', $requestData['id'])->first();


        Cart::add(array(
            array(
                'id' => $requestData['id'],
                'name' => $productInfo->p_name,
                'price' => $productInfo->p_sel_price,
                'quantity' => $requestData['qty'],
                'attributes' => array('color' => $productInfo->p_color, 'description' => $productInfo->p_description)
            ),
        ));

        $linkedproducts = explode(',', $requestData['linkedproducts']);
        if (count($linkedproducts) > 0 && $requestData['linkedproducts'] != '') {
            foreach ($linkedproducts as $products) {
                $productInfo = Products::where('p_id', $products)->first();


                Cart::add(array(
                    array(
                        'id' => $products,
                        'name' => $productInfo->p_name,
                        'price' => $productInfo->p_sel_price,
                        'quantity' => 1,
                        'attributes' => array('color' => $productInfo->p_color, 'description' => $productInfo->p_description)
                    ),
                ));
            }
        }

        $deliveryCharge = paymentDeliveryCharges(Cart::getSubTotal());

        $condition = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'Shipping Rs.' . $deliveryCharge,
            'type' => 'shipping',
            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => '+' . $deliveryCharge,
            'order' => 1
        ));
        Cart::condition($condition);


        echo Cart::getContent()->count();
        return redirect()->route('cartDetails');
    }

    public function saveSubscribers(Request $request)
    {
        $return = array();
        $requestData = $request->all();
        $validator = Validator::make($requestData,
            [
                'nl_email' => 'required|email'
            ],
            [
                'nl_email.required' => 'Please enter email',
                'nl_email.email' => 'Please enter valid email'
            ]
        );
        if ($validator->fails()) {
            $return['status'] = 2;
            $test = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                array_push($test, $messages[0]);// messages are retrieved (publicly)
            }
            $return['errors'] = implode('<br>', $test);
        } else {
            $cdetails = NewsLetters::create($requestData);
            if ($cdetails->exists) {
                $return['status'] = 1;
            } else {
                $return['status'] = 2;
                $return['errors'] = "Please enter valid data";
            }
        }
        return $return;

    }


}
