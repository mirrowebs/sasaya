<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getsubcategories(Request $request)
    {
        $category = $request['category'];
        $return = array();
        $return['categories'] = Categories::where('category_parent', $category)->where('category_status', 'active')->pluck('category_name', 'category_id');
        $return['length'] = Categories::where('category_parent', $category)->where('category_status', 'active')->count();
        echo json_encode($return);
        exit();
    }
}
