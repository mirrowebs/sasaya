<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $routname = request()->segment(1);

        if ($routname == 'admin') {
            $url = route('dashboard');
            return $url;
        } else {
            $url = route('home');
            return $url;
        }

    }

    protected function guard()
    {
        $routname = request()->segment(1);

        if ($routname == 'admin') {
            return Auth::guard('admin');
        } else {
            return Auth::guard();
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $routname = request()->segment(1);

        if ($routname == 'admin') {
            $this->middleware('guest:admin')->except('logout');
        } else {
            $this->middleware('guest')->except('logout');

        }
    }

    public function showLoginForm($location = null)
    {
//        exit();

//        App::setLocale($location);

        $routname = request()->segment(1);

        if ($routname == 'admin') {
            $data = array();
            $data['active_menu'] = 'login';
            $data['sub_active_menu'] = 'login';
            $data['title'] = 'Login';
            return view('auth.login', $data);
        } else {
            $data = array();
            $data['active_menu'] = 'login';
            $data['sub_active_menu'] = 'login';
            $data['title'] = 'Login';
            return view('frontend.login', $data);
        }
    }

    protected function credentials(Request $request)
    {

        $routname = request()->segment(1);
        $credentials = $request->only($this->username(), 'password');
        $credentials['status'] = 'active';
        if ($routname == 'admin') {
            $credentials['role'] = 404;
        } else {
            $credentials['role'] = 1;
        }
        return $credentials;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        // Load user from database
        $user = User::where($this->username(), $request->{$this->username()})->first();
        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.

        if (!$user || $errors) {
            $errors = [$this->username() => 'Invalid credentials'];
        }

        if ($user && Hash::check($request->password, $user->password) && $user->status != 'active') {
            $errors = [$this->username() => 'Your account is not in active.'];
        }


        $routname = request()->segment(1);

        if ($routname == 'admin') {
            if ($user && $user->role != 404) {
                $errors = [$this->username() => 'Your do not have admin account access.'];
            }
        } else {
            if ($user && $user->role != 1) {
                $errors = [$this->username() => 'Your do not have user access.'];
            }
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function logout(Request $request)
    {
        $routname = request()->segment(1);
        if ($routname == 'admin') {
            $this->guard('admin')->logout();
            $route = 'login';
        } else {
            $this->guard()->logout();
            $route = 'home';
        }
        return redirect()->route($route);
    }
}
