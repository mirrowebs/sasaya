<?php

namespace App\Http\Controllers;


use App\Models\ProductImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AjaxController extends Controller
{

    public function __construct()
    {

    }


    public function deleteProductimage(Request $request)
    {


        ProductImages::removeImage($request['image']);



        exit();
    }



}
